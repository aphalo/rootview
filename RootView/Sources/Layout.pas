unit Layout;

interface

uses Windows, SysUtils, Classes, Graphics, Forms, Controls, StdCtrls, 
  Buttons, ExtCtrls, OkCancl2, ComCtrls;

type
  TLayoutDlg = class(TOKRightDlg)
    HelpBtn: TButton;
    ListBox1: TListBox;
    Button1: TButton;
    Button2: TButton;
    Button3: TButton;
    Button4: TButton;
    ProgressBar1: TProgressBar;
    procedure HelpBtnClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure ListBox1Click(Sender: TObject);
    procedure ListBox1DblClick(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure OKBtnClick(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure Button3Click(Sender: TObject);
    procedure Button4Click(Sender: TObject);
    procedure FormActivate(Sender: TObject);
  private
    { Private declarations }
    xTake,xPlot,xTube,xFrame,Mode: integer;
    procedure SetupPage( fast: boolean=true );
  public
    { Public declarations }
    function Execute(level: integer): Boolean;
  end;

var
  LayoutDlg: TLayoutDlg;

implementation

uses Data,Roots,Prefer;

{$R *.DFM}

procedure TLayoutDlg.HelpBtnClick(Sender: TObject);
begin
  if Length(HlpFile)>0 then begin
    WinHelp(RootViewer.Handle, @HlpFile[1], Help_Key,
      Integer(PChar('Dialog: Data Browser')));
  end else begin
    Application.HelpContext(HelpContext);
  end;
end;

function TLayoutDlg.Execute( level: integer ): Boolean;
begin
  { execute; set result based on how closed }
  LayoutDlg.Mode:=level;
  Result:=LayoutDlg.ShowModal=IDOK;
end;

procedure TLayoutDlg.SetupPage( fast: boolean=true );
  function isFast(total: integer): boolean;
  begin
    Result:=Fast and (total>Pref.MaxScan);
  end;
  procedure SetupTakes;
  var cc,total,i: integer;
      dir: String;
    function GetTotal: integer;
    var j,k: integer;
    begin
      cc:=0;
      Total:=0;
      for k:=0 to Experiment.NSites-1 do begin
        for j:=0 to Experiment.Sites[k].NTubes-1 do begin
          Inc(Total, Experiment.Sites[k].Tubes[j].Frames);
        end;
      end;
      Total:=Total*Experiment.NTakes;
      Result:=Total;
    end;
    function Exists(n: integer): String;
    var zdir,ydir,xdir,s: String;
        i,j,k,a,b,c: integer;
    begin
      a:=0;
      b:=0;
      c:=0;
      zdir:=dir+Experiment.Takes[n].Name+'\';
      for k:=0 to Experiment.NSites-1 do begin
        ydir:=zdir+Experiment.Sites[k].Name+'\';
        for j:=0 to Experiment.Sites[k].NTubes-1 do begin
          xdir:=ydir+Experiment.Sites[k].Tubes[j].Name+'\';
          for i:=0 to Experiment.Sites[k].Tubes[j].Frames-1 do begin
            Inc(c);
            if not isFast(total) and
              not isCancelled and ((cc mod 100<>0) or not isCancel) then begin
              if (total<1000) or (cc mod 100=0) then begin
                ProgressBar1.Position:=Round(100*(cc/total));
              end;
              Inc(cc);
              s:=xdir+GetFrameName(k,j,i);
              if FileExists(s+'.jpg') then Inc(a);
              if FileExists(s+'.rvd') then Inc(b);
            end;
          end;
        end;
      end;
      Result:='    '+IntToStr(c);
      if not isFast(total) and not isCancelled and not isCancel then begin
        Result:=Result+':  '+IntToStr(b)+' / '+IntToStr(a);
      end;
    end;
  begin
    GetTotal;
    if not isFast(total) then begin
      StartWorking('Scanning Data...');
    end;
    dir:=GetFilePath(Experiment.Name)+'\';
    with Experiment do begin
      for i:=0 to NTakes-1 do begin
        ListBox1.Items.Add(Takes[i].Name+Exists(i));
        ListBox1.Update;
      end;
      ListBox1.ItemIndex:=xTake;
    end;
    if not isFast(total) then begin
      StopWorking;
    end;
  end;
  procedure SetupPlots;
  var cc,total,i: integer;
      dir: String;
    function GetTotal: integer;
    var j,k: integer;
    begin
      cc:=0;
      Total:=0;
      for k:=0 to Experiment.NSites-1 do begin
        for j:=0 to Experiment.Sites[k].NTubes-1 do begin
          Inc(Total, Experiment.Sites[k].Tubes[j].Frames);
        end;
      end;
      Result:=Total;
    end;
    function Exists(n: integer): String;
    var zdir,xdir,s: String;
        i,j,a,b,c: integer;
    begin
      a:=0;
      b:=0;
      c:=0;
      zdir:=dir+Experiment.Sites[n].Name+'\';
      for j:=0 to Experiment.Sites[n].NTubes-1 do begin
        xdir:=zdir+Experiment.Sites[n].Tubes[j].Name+'\';
        for i:=0 to Experiment.Sites[n].Tubes[j].Frames-1 do begin
          Inc(c);
          if not isFast(total) and not isCancelled and not isCancel then begin
            Inc(cc);
            ProgressBar1.Position:=Round(100*(cc/total));
            s:=xdir+GetFrameName(n,j,i);
            if FileExists(s+'.jpg') then Inc(a);
            if FileExists(s+'.rvd') then Inc(b);
          end;
        end;
      end;
      Result:='    '+IntToStr(c);
      if not isFast(total) and not isCancelled and not isCancel then begin
        Result:=Result+':  '+IntToStr(b)+' / '+IntToStr(a);
      end;
    end;
  begin
    GetTotal;
    if not isFast(total) then begin
      StartWorking('Scanning Data...');
    end;
    dir:=GetFilePath(Experiment.Name)+'\'+
      Experiment.Takes[xTake].Name+'\';
    with Experiment do begin
      for i:=0 to NSites-1 do begin
        ListBox1.Items.Add(Sites[i].Name+Exists(i));
        ListBox1.Update;
      end;
      ListBox1.ItemIndex:=xPlot;
    end;
    if not isFast(total) then begin
      StopWorking;
    end;
  end;
  procedure SetupTubes;
  var cc,total,i: integer;
      dir: String;
    function GetTotal: integer;
    var j: integer;
    begin
      cc:=0;
      Total:=0;
      for j:=0 to Experiment.Sites[xPlot].NTubes-1 do begin
        Inc(Total, Experiment.Sites[xPlot].Tubes[j].Frames);
      end;
      Result:=Total;
    end;
    function Exists(n: integer): String;
    var xdir,s: String;
        i,a,b,c: integer;
    begin
      a:=0;
      b:=0;
      c:=0;
      xdir:=dir+Experiment.Sites[xPlot].Tubes[n].Name+'\';
      for i:=0 to Experiment.Sites[xPlot].Tubes[n].Frames-1 do begin
        Inc(c);
        if not isFast(total) and not isCancelled and not isCancel then begin
          Inc(cc);
          ProgressBar1.Position:=Round(100*(cc/total));
          s:=xdir+GetFrameName(xPlot,n,i);
          if FileExists(s+'.jpg') then Inc(a);
          if FileExists(s+'.rvd') then Inc(b);
        end;
      end;
      Result:='    '+IntToStr(c);
      if not isFast(total) and not isCancelled and not isCancel then begin
        Result:=Result+':  '+IntToStr(b)+' / '+IntToStr(a);
      end;
    end;
  begin
    GetTotal;
    if not isFast(total) then begin
      StartWorking('Scanning Data...');
    end;
    dir:=GetFilePath(Experiment.Name)+'\'+
      Experiment.Takes[xTake].Name+'\'+
      Experiment.Sites[xPlot].Name+'\';
    with Experiment.Sites[xPlot] do begin
      ListBox1.Update;
      for i:=0 to NTubes-1 do begin
        ListBox1.Items.Add(Tubes[i].Name+Exists(i));
        ListBox1.Update;
      end;
      ListBox1.ItemIndex:=xTube;
    end;
    if not isFast(total) then begin
      StopWorking;
    end;
  end;
  procedure SetupFrames;
  var i: integer;
      dir: String;
    function Exists(n: integer): String;
    var x,s: String;
    begin
      s:=dir+GetFrameName(xPlot,xTube,n);
      if not FileExists(s+'.jpg') then x:='?';
      if not FileExists(s+'.rvd') then x:=x+'!';
      Result:='       '+x;
    end;
  begin
    dir:=GetFilePath(Experiment.Name)+'\'+
      Experiment.Takes[xTake].Name+'\'+
      Experiment.Sites[xPlot].Name+'\'+
      Experiment.Sites[xPlot].Tubes[xTube].Name+'\';
    with Experiment.Sites[xPlot].Tubes[xTube] do begin
      for i:=0 to Frames-1 do begin
        ListBox1.Items.Add(GetFrameName(xPlot,xTube,i)+Exists(i));
        ListBox1.Update;
      end;
      ListBox1.ItemIndex:=xFrame;
    end;
  end;
begin
  ProgressBar1.Position:=0;
  Caption:='...\'+
    Experiment.Takes[xTake].Name+'\'+
    Experiment.Sites[xPlot].Name+'\'+
    Experiment.Sites[xPlot].Tubes[xTube].Name+'\'+
        GetFrameName(xPlot,xTube,xFrame);
  Button1.Enabled:=mode>0;
  Button2.Enabled:=mode<3;
  Button4.Enabled:=mode<3;
  ListBox1.Clear;
  ListBox1.Update;
  case mode of
    0: SetupTakes;
    1: SetupPlots;
    2: SetupTubes;
    3: SetupFrames;
    else {Oh, well}
  end;
  ProgressBar1.Position:=0;
end;

procedure TLayoutDlg.FormShow(Sender: TObject);
  function NeedsFolders: boolean;
  var i,j,k,c: integer;
      s,s1,s2: String;
  begin
    c:=0;
    with Experiment do begin
      for i:=0 to NTakes-1 do begin
        s:=GetFilePath(Name);
        with Takes[i] do begin
          s:=s+'\'+Name;
          if not FolderExists(s) then begin
            Inc(c);
          end;
          for j:=0 to NSites-1 do begin
            s1:=s+'\'+Sites[j].Name;
            if not FolderExists(s1) then begin
              Inc(c);
            end;
            for k:=0 to Sites[j].NTubes-1 do begin
              s2:=s1+'\'+Sites[j].Tubes[k].Name;
              if not FolderExists(s2) then begin
                Inc(c);
              end;
            end;
          end;
        end;
      end;
    end;
    Result:=c>0;
  end;
begin
  inherited;
  Button3.Enabled:=NeedsFolders;
  with Experiment do begin
    xTake:=TakeIndex;
    xPlot:=PlotIndex;
    xTube:=TubeIndex;
    xFrame:=FrameIndex;
  end;
end;

procedure TLayoutDlg.ListBox1Click(Sender: TObject);
begin
  inherited;
  case mode of
    0: xTake:=ListBox1.ItemIndex;
    1: xPlot:=ListBox1.ItemIndex;
    2: xTube:=ListBox1.ItemIndex;
    3: xFrame:=ListBox1.ItemIndex;
    else {Oh, well}
  end;
  Caption:='...\'+
    Experiment.Takes[xTake].Name+'\'+
    Experiment.Sites[xPlot].Name+'\'+
    Experiment.Sites[xPlot].Tubes[xTube].Name+'\'+
        GetFrameName(xPlot,xTube,xFrame);
end;

procedure TLayoutDlg.ListBox1DblClick(Sender: TObject);
begin
  inherited;
  if Mode<3 then begin
    Inc(Mode);
    SetupPage;
  end else begin
    OKBtn.Click;
  end;
end;

procedure TLayoutDlg.Button1Click(Sender: TObject);
begin
  inherited;
  Dec(Mode);
  SetupPage;
end;

procedure TLayoutDlg.OKBtnClick(Sender: TObject);
begin
  inherited;
  RootViewer.SetFrame(xTake,xPlot,xTube,xFrame);
end;

procedure TLayoutDlg.Button2Click(Sender: TObject);
begin
  inherited;
  Inc(Mode);
  SetupPage;
end;

procedure TLayoutDlg.Button3Click(Sender: TObject);
var n: integer;
begin
  inherited;
  n:=Experiment.CreateFolders;
  InfoMsg(IntToStr(n) + ' New Folders Created.');
  Button3.Enabled:=false;
end;

procedure TLayoutDlg.Button4Click(Sender: TObject);
begin
  inherited;
  SetupPage(false);
end;

procedure TLayoutDlg.FormActivate(Sender: TObject);
begin
  inherited;
  UpdateWindow(Handle);
  Wait(1);
  SetupPage;
end;

end.

