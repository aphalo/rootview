unit ObjDlg;

interface

uses Windows, SysUtils, Classes, Graphics, Forms, Controls, StdCtrls, 
  Buttons, ExtCtrls, OkCancl1, ComCtrls;

type
  TSelDlg = class(TOKBottomDlg)
    HelpBtn: TButton;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    Edit1: TEdit;
    ComboBox1: TComboBox;
    GroupBox1: TGroupBox;
    Edit2: TEdit;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Edit3: TEdit;
    Label4: TLabel;
    ComboBox2: TComboBox;
    GroupBox2: TGroupBox;
    Edit4: TEdit;
    procedure HelpBtnClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    function Execute(Name1,Name2: String): Boolean;
  end;

var
  SelDlg: TSelDlg;

implementation

uses Roots,Data;

{$R *.DFM}

procedure TSelDlg.HelpBtnClick(Sender: TObject);
begin
  if Length(HlpFile)>0 then begin
    WinHelp(RootViewer.Handle, @HlpFile[1], Help_Key,
      Integer(PChar('Dialog: New Root or Tip')));
  end else begin
    Application.HelpContext(HelpContext);
  end;
end;

function TSelDlg.Execute( Name1,Name2: String ): Boolean;
begin
  Edit1.Text:=Name1;
  Edit3.Text:=Name2;
  Result:=SelDlg.ShowModal=IDOK;
end;

end.

