program RootView;

uses
  Forms,
  OkCancl1 in 'C:\PROGRAM FILES\BORLAND\DELPHI5\OBJREPOS\OKCANCL1.pas' {OKBottomDlg},
  OkCancl2 in 'C:\PROGRAM FILES\BORLAND\DELPHI5\OBJREPOS\OKCANCL2.pas' {OKRightDlg},
  Roots in 'Roots.pas' {RootViewer},
  About in 'About.pas' {AboutDlg},
  Data in 'Data.pas' {DataDlg},
  Prefer in 'Prefer.pas' {PrefDlg},
  ObjDlg in 'ObjDlg.pas' {SelDlg},
  RootStat in 'RootStat.pas' {RootStatDlg},
  TipStat in 'TipStat.pas' {TipStatDlg},
  Layout in 'Layout.pas' {LayoutDlg},
  Frame in 'Frame.pas' {FrameDlg},
  OutData in 'OutData.pas' {OutDataDlg},
  CatDel in 'CatDel.pas' {CatDelDlg},
  CatSub in 'CatSub.pas' {CatSubDlg},
  Tools in 'Tools.pas' {MenuDlg},
  Exec in 'Exec.pas' {ExecDlg};

{$R *.RES}

begin
  Application.Initialize;
  Application.HelpFile := 'RootView.hlp';
  Application.Title := 'Root View';
  Application.CreateForm(TRootViewer, RootViewer);
  Application.CreateForm(TSelDlg, SelDlg);
  Application.CreateForm(TRootStatDlg, RootStatDlg);
  Application.CreateForm(TTipStatDlg, TipStatDlg);
  Application.CreateForm(TLayoutDlg, LayoutDlg);
  Application.CreateForm(TFrameDlg, FrameDlg);
  Application.CreateForm(TAboutDlg, AboutDlg);
  Application.CreateForm(TOutDataDlg, OutDataDlg);
  Application.CreateForm(TCatDelDlg, CatDelDlg);
  Application.CreateForm(TCatSubDlg, CatSubDlg);
  Application.Run;
end.
