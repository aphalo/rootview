
{*******************************************************}
{                                                       }
{ Root View - Delphi 5 (Pro)                            }
{                                                       }
{ Copyright (c) 1998-2000 by Aleksander Simonic.        }
{             All Rights Reserved.                      }
{                                                       }
{     e-mail: alex@winedt.com                           }
{     www:    http://www.winedt.com                     }
{                                                       }
{*******************************************************}

unit Roots;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Menus, ExtCtrls, Buttons, ToolWin, ComCtrls, JPEG, ShellAPI, Data,
  ImgList, StdCtrls, DdeMan, MPlayer;

type
  TRootViewer = class(TForm)
    MainMenu1: TMainMenu;
    File1: TMenuItem;
    N1: TMenuItem;
    Exit1: TMenuItem;
    Help1: TMenuItem;
    ToolBar1: TToolBar;
    StatusBar1: TStatusBar;
    ScrollBox1: TScrollBox;
    Image1: TImage;
    OpenDialog1: TOpenDialog;
    Preferences1: TMenuItem;
    Categories1: TMenuItem;
    Edit1: TMenuItem;
    Delete1: TMenuItem;
    Back1: TMenuItem;
    Deselect1: TMenuItem;
    Undo1: TMenuItem;
    AltUndo1: TMenuItem;
    Open1: TMenuItem;
    Save1: TMenuItem;
    N2: TMenuItem;
    N3: TMenuItem;
    Restore1: TMenuItem;
    Contents1: TMenuItem;
    Index1: TMenuItem;
    N4: TMenuItem;
    About1: TMenuItem;
    Clear1: TMenuItem;
    ImageList1: TImageList;
    FirstTakeBtn: TToolButton;
    PrevTakeBtn: TToolButton;
    NextTakeBtn: TToolButton;
    LastTakeBtn: TToolButton;
    FirstFrameBtn: TToolButton;
    PrevFrameBtn: TToolButton;
    NextFrameBtn: TToolButton;
    LastFrameBtn: TToolButton;
    ToolButton5: TToolButton;
    ToolButton1: TToolButton;
    RealSizeBtn: TToolButton;
    StretchBtn: TToolButton;
    ZoomInBtn: TToolButton;
    ZoomOutBtn: TToolButton;
    RegButton: TToolButton;
    ToolButton3: TToolButton;
    ImportBtn: TToolButton;
    KillBtn: TToolButton;
    DelBtn: TToolButton;
    BackBtn: TToolButton;
    UndoBtn: TToolButton;
    ToolButton9: TToolButton;
    N5: TMenuItem;
    Kill1: TMenuItem;
    ToolButton4: TToolButton;
    LabelsBtn: TToolButton;
    WidthsBtn: TToolButton;
    RootsBtn: TToolButton;
    TipsBtn: TToolButton;
    ToolButton6: TToolButton;
    ToolButton7: TToolButton;
    ToolButton8: TToolButton;
    ToolButton10: TToolButton;
    N6: TMenuItem;
    ImportFrame1: TMenuItem;
    ToggleEditMode1: TMenuItem;
    ColorsBtn: TToolButton;
    ToolButton11: TToolButton;
    GhostBtn: TToolButton;
    ToolButton2: TToolButton;
    OutputBtn: TToolButton;
    Data1: TMenuItem;
    View1: TMenuItem;
    SelectPlot1: TMenuItem;
    CreateOutput1: TMenuItem;
    N11: TMenuItem;
    FirstMeasurement1: TMenuItem;
    LastMeasurement1: TMenuItem;
    Nextmeasurement1: TMenuItem;
    PreviousMeasurement1: TMenuItem;
    N7: TMenuItem;
    FirstFrame1: TMenuItem;
    LastFrame1: TMenuItem;
    PreviousFrame1: TMenuItem;
    NextFrame1: TMenuItem;
    RealSize1: TMenuItem;
    FitImagetoClient1: TMenuItem;
    ZoomIn1: TMenuItem;
    ZoomOut1: TMenuItem;
    Close1: TMenuItem;
    N8: TMenuItem;
    StartNextFrame1: TMenuItem;
    N9: TMenuItem;
    ImportNewerFrame1: TMenuItem;
    Tools1: TMenuItem;
    Setup1: TMenuItem;
    procedure Preferences1Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure Categories1Click(Sender: TObject);
    procedure Exit1Click(Sender: TObject);
    procedure Image1MouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure Image1MouseMove(Sender: TObject; Shift: TShiftState; X,
      Y: Integer);
    procedure Image1MouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure DelBtnClick(Sender: TObject);
    procedure BackBtnClick(Sender: TObject);
    procedure Image1DblClick(Sender: TObject);
    procedure Delete1Click(Sender: TObject);
    procedure Back1Click(Sender: TObject);
    procedure Deselect1Click(Sender: TObject);
    procedure RegButtonClick(Sender: TObject);
    procedure RealSizeBtnClick(Sender: TObject);
    procedure StretchBtnClick(Sender: TObject);
    procedure ZoomInBtnClick(Sender: TObject);
    procedure ZoomOutBtnClick(Sender: TObject);
    procedure LabelsBtnClick(Sender: TObject);
    procedure RootsBtnClick(Sender: TObject);
    procedure TipsBtnClick(Sender: TObject);
    procedure WidthsBtnClick(Sender: TObject);
    procedure UndoBtnClick(Sender: TObject);
    procedure Undo1Click(Sender: TObject);
    procedure AltUndo1Click(Sender: TObject);
    procedure Open1Click(Sender: TObject);
    procedure StatusBar1MouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure ImportBtnClick(Sender: TObject);
    procedure Save1Click(Sender: TObject);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure Restore1Click(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure FormKeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure ScrollBox1Enter(Sender: TObject);
    procedure Contents1Click(Sender: TObject);
    procedure Index1Click(Sender: TObject);
    procedure KillBtnClick(Sender: TObject);
    procedure Clear1Click(Sender: TObject);
    procedure ToolButton1Click(Sender: TObject);
    procedure PrevFrameBtnClick(Sender: TObject);
    procedure NextTakeBtnClick(Sender: TObject);
    procedure LastTakeBtnClick(Sender: TObject);
    procedure ToolButton3Click(Sender: TObject);
    procedure FirstFrameBtnClick(Sender: TObject);
    procedure FirstTakeBtnClick(Sender: TObject);
    procedure PrevTakeBtnClick(Sender: TObject);
    procedure LastFrameBtnClick(Sender: TObject);
    procedure ImportFrame1Click(Sender: TObject);
    procedure ToggleEditMode1Click(Sender: TObject);
    procedure ColorsBtnClick(Sender: TObject);
    procedure GhostBtnClick(Sender: TObject);
    procedure OutputBtnClick(Sender: TObject);
    procedure FirstMeasurement1Click(Sender: TObject);
    procedure LastMeasurement1Click(Sender: TObject);
    procedure PreviousMeasurement1Click(Sender: TObject);
    procedure Nextmeasurement1Click(Sender: TObject);
    procedure FirstFrame1Click(Sender: TObject);
    procedure LastFrame1Click(Sender: TObject);
    procedure PreviousFrame1Click(Sender: TObject);
    procedure NextFrameBtnClick(Sender: TObject);
    procedure NextFrame1Click(Sender: TObject);
    procedure RealSize1Click(Sender: TObject);
    procedure FitImagetoClient1Click(Sender: TObject);
    procedure ZoomIn1Click(Sender: TObject);
    procedure ZoomOut1Click(Sender: TObject);
    procedure SelectPlot1Click(Sender: TObject);
    procedure CreateOutput1Click(Sender: TObject);
    procedure Close1Click(Sender: TObject);
    procedure About1Click(Sender: TObject);
    procedure Kill1Click(Sender: TObject);
    procedure StartNextFrame1Click(Sender: TObject);
    procedure ImportNewerFrame1Click(Sender: TObject);
    procedure Setup1Click(Sender: TObject);
    procedure FormResize(Sender: TObject);
  protected
    { Windows Messages}
    procedure wmDropFiles(var Msg: TMessage ); message wm_DropFiles;
  private
    { Private declarations }
    Mark: TFPoint;
    BkpDX,BkpDY: float;
    procedure FixImages;
    procedure DrawFrame;
    procedure Backspace;
    procedure Delete;
    procedure Undo;
    procedure Kill;
    procedure Clear;
    procedure Import(DeltaInd: integer=1);
    procedure CopyFrame(Src: TFrame; var Dest: TFrame);
    function areFramesEqual(A,B: TFrame): boolean;
    procedure MoveFrame( DX,DY: float );
    procedure OpenProject(FN: String);
    procedure SetCursor( newCursor: TCursor );
    function FixFrameDelta: boolean;
  public
    { Public declarations }
    Bmp1: TBitmap;
    JPEG1: TJPegImage;
    Zoom: float;
    JPEGOK,JPEGStretch: boolean;
    OldFrame,BkpFrame,Frame: TFrame;
    function UpdateFrame(doUpdate: boolean=true): boolean;
    function SetFrame(NewTake,NewPlot,NewTube,NewFrame: integer): boolean;
    procedure GetFrameDefaults( TakeIndex, PlotIndex, TubeIndex, FrameIndex: integer );
    procedure FixCategories;
    procedure UpdateStatus;
    procedure WriteOutput;
    procedure SelectPlot;
    procedure Execute(Sender: TObject);
  end;

var
  RootViewer: TRootViewer;

implementation

uses Prefer, ObjDlg, RootStat, TipStat, Layout, Frame, About, OutData,
  Tools, Exec;

{$R *.DFM}

procedure TRootViewer.Execute(Sender: TObject);
var Comm,Dir: String;
  function StrToPChar(var s: String): PChar;
  begin
    if (Length(s)=0) or (s[1]=#0) then begin
      Result:=nil;
    end else begin
      Result:=@s[1];
    end;
  end;
  function Translate(s: String): String;
  var x: String;
      i: integer;
      isShort,isUnix: boolean;
    function FixFile(fn: String): String;
    var i: integer;
    begin
      if Length(fn)=0 then begin
        Result:='';
        Exit;
      end;
      if isShort then begin
        fn:=GetShortFileName(fn);
      end;
      if isUNIX then begin
        for i:=1 to Length(fn) do begin
          if fn[i]='\' then fn[i]:='/';
        end;
      end;
      Result:=fn;
    end;
    function GetRVDir: String;
    var fn: String;
    begin
      fn:=HlpFile;
      fn:=GetFilePath(FN);
      fn:=FixFile(FN);
      Result:=fn;
    end;
    function GetPrjDir: String;
    var fn: String;
    begin
      Result:='';
      with Experiment do begin
        if (TakeIndex>=0) and (FrameIndex>=0) then begin
          fn:=Experiment.Name;
          fn:=GetFilePath(FN);
          fn:=FixFile(FN);
          Result:=fn;
        end;
      end;
    end;
    function GetPrjName: String;
    var fn: String;
    begin
      Result:='';
      with Experiment do begin
        if (TakeIndex>=0) and (FrameIndex>=0) then begin
          fn:=Experiment.Name;
          fn:=GetFileName(FN);
          Result:=fn;
        end;
      end;
    end;
    function GetDataDir: String;
    var fn: String;
    begin
      Result:='';
      with Experiment do begin
        if (TakeIndex>=0) and (FrameIndex>=0) then begin
          fn:=GetDataFileName(TakeIndex, PlotIndex, TubeIndex);
          fn:=FixFile(fn);
          Result:=fn;
        end;
      end;
    end;
    function GetFrameNo: String;
    var fn: String;
    begin
      Result:='';
      with Experiment do begin
        if (TakeIndex>=0) and (FrameIndex>=0) then begin
          fn:=GetFrameName(PlotIndex,TubeIndex,FrameIndex);
          Result:=fn;
        end;
      end;
    end;
    function GetROutName(Short: boolean): String;
    var fn: String;
    begin
      Result:='';
      with Experiment do begin
        if (TakeIndex>=0) and (FrameIndex>=0) then begin
          fn:=Pref.DatRoot;
          if Short then begin
            fn:=GetFileName(fn);
          end;
          Result:=fn;
        end;
      end;
    end;
    function GetTOutName(Short: boolean): String;
    var fn: String;
    begin
      Result:='';
      with Experiment do begin
        if (TakeIndex>=0) and (FrameIndex>=0) then begin
          fn:=Pref.DatTip;
          if Short then begin
            fn:=GetFileName(fn);
          end;
          Result:=fn;
        end;
      end;
    end;
  begin
    x:='';
    i:=1;
    while i<=Length(s) do begin
      if s[i]='%' then begin
        Inc(i);
        isUnix:=false;
        isShort:=false;
        while s[i] in ['~','/'] do begin
          if s[i]='/' then isUnix:=true;
          if s[i]='~' then isShort:=true;
          Inc(i);
        end;
        case s[i] of
          'B': x:=x+GetRVDir;
          'P': x:=x+GetPrjDir;
          'F': x:=x+GetPrjName;
          'p': x:=x+GetDataDir;
          'f': x:=x+GetFrameNo;
          'R': x:=x+GetROutName(false);
          'r': x:=x+GetROutName(true);
          'T': x:=x+GetTOutName(false);
          't': x:=x+GetTOutName(true);
          else begin
            x:=x+s[i];
          end;
        end;
      end else begin
        x:=x+s[i];
      end;
      Inc(i);
    end;
    Result:=x;
  end;
  function isOK(C,T,D: String; Confirm: boolean): boolean;
  var Title: String;
  begin
    Result:=false;
    Comm:=Translate(C);
    Dir:=Translate(D);
    Title:=Translate(T);
    if Length(Title)>0 then begin
      if SetExistAppl(StrToPChar(Title))<>0 then begin
        Exit;
      end;
    end;
    if Confirm then begin
      if not ExecDlg.Execute(Comm, Dir, Title) then begin
        Exit;
      end;
    end;
    Result:=Length(Comm)>0;
  end;
begin
  with MTools[TMenuItem(Sender).Tag] do begin
    if isOK(Command, Caption, Folder, Confirm) then begin
      WindExec(StrToPChar(Comm), StrToPChar(Dir));
    end;
  end;
end;

function TRootViewer.FixFrameDelta: boolean;
begin
  Result:=false;
  if Abs(Frame.DX-BkpFrame.DX)<eps then begin
    Result:=true;
    Frame.DX:=BkpFrame.DX;
  end;
  if Abs(Frame.DY-BkpFrame.DY)<eps then begin
    Result:=true;
    Frame.DY:=BkpFrame.DY;
  end;
  if Abs(Frame.Mark.x-BkpFrame.Mark.x)<eps then begin
    Result:=true;
    Frame.Mark.x:=BkpFrame.Mark.x;
  end;
  if Abs(Frame.Mark.y-BkpFrame.Mark.y)<eps then begin
    Result:=true;
    Frame.Mark.y:=BkpFrame.Mark.y;
  end;
end;

procedure TRootViewer.SetCursor( newCursor: TCursor );
var Pos: TPoint;
begin
  if Image1.Cursor<>newCursor then begin
    GetCursorPos(Pos);
    ShowCursor(false);
    Image1.Cursor:=newCursor;
    SetCursorPos(Pos.x,Pos.Y);
    ShowCursor(true);
  end;
  if NewCursor=crDefault then begin
    ReleaseCapture;
  end else begin
    SetCapture(Image1.Picture.Bitmap.Handle);
  end;
end;

procedure TRootViewer.DrawFrame;
var Bmp: TBitmap;
begin
  UpdateStatus;
  Bmp:=TBitmap.Create;
  Bmp.Assign(Bmp1);
  Frame.DrawItems(Bmp);
  Image1.Picture.Bitmap.Assign(Bmp);
  Bmp.Free;
  Image1.Update;
end;

procedure TRootViewer.FixImages;
var Rect: TRect;
    oldWorking: boolean;
begin
  if not JPEGOK then begin
    Bmp1.Width:=0;
    Bmp1.Height:=0;
    Image1.Picture.Bitmap.Width:=0;
    Image1.Picture.Bitmap.Height:=0;
    Image1.Width:=0;
    Image1.Height:=0;
    UpdateStatus;
    Exit;
  end;
  oldWorking:=isWorking;
  if Abs(Zoom-Round(Zoom))<eps then begin
    Zoom:=Round(Zoom);
  end;
  JPEG1.GrayScale:=not ColorsBtn.Down;
  if JPEGStretch then begin
    if not OldWorking then begin
      StartWorking('Stretching Image...');
    end;
    Rect.Left:=0;
    Rect.Top:=0;
    ScrollBox1.Update;
    ScrollBox1.HorzScrollbar.Visible:=false;
    ScrollBox1.VertScrollbar.Visible:=false;
    Rect.Right:=Round(ScrollBox1.ClientWidth*Zoom);
    Rect.Bottom:=Round(ScrollBox1.ClientHeight*Zoom);
    ScrollBox1.HorzScrollbar.Visible:=true;
    ScrollBox1.VertScrollbar.Visible:=true;
    ScrollBox1.Update;
    Bmp1.Width:=Rect.Right;
    Bmp1.Height:=Rect.Bottom;
    Bmp1.Canvas.StretchDraw(Rect, JPEG1);
  end else begin
    if Abs(Zoom-1)<eps then begin
      if not OldWorking then begin
        StartWorking('Copying Image...');
      end;
      if ColorsBtn.Down then begin
        Bmp1.Assign(JPEG1);
      end else begin
        Bmp1.Width:=JPEG1.Width;
        Bmp1.Height:=JPEG1.Height;
        Bmp1.Canvas.Draw(0,0,JPEG1);
      end;
    end else begin
      if not OldWorking then begin
        if Zoom>1 then begin
          StartWorking('Zomming In...');
        end else begin
          StartWorking('Zomming Out...');
        end;
      end;
      Rect.Left:=0;
      Rect.Top:=0;
      Rect.Right:=Round(JPEG1.Width*Zoom);
      Rect.Bottom:=Round(JPEG1.Height*Zoom);
      Bmp1.Width:=Rect.Right;
      Bmp1.Height:=Rect.Bottom;
      Bmp1.Canvas.StretchDraw(Rect, JPEG1);
    end;
  end;
  if not OldWorking and isWorking then begin
    StopWorking;
  end;
  DrawFrame;
  UpdateStatus;
end;

procedure TRootViewer.FixCategories;
var i: integer;
begin
  SelDlg.ComboBox1.Clear;
  SelDlg.ComboBox2.Clear;
  for i:=0 to Experiment.NRootCat-1 do begin
    SelDlg.ComboBox1.Items.Add(Experiment.RootCat[i].Name);
  end;
  for i:=0 to Experiment.NTipCat-1 do begin
    SelDlg.ComboBox2.Items.Add(Experiment.TipCat[i].Name);
  end;
  SelDlg.ComboBox1.ItemIndex:=0;
  SelDlg.ComboBox2.ItemIndex:=0;
  RootStatDlg.ComboBox1.Clear;
  for i:=0 to Experiment.NRootCat-1 do begin
    RootStatDlg.ComboBox1.Items.Add(Experiment.RootCat[i].Name);
  end;
  RootStatDlg.ComboBox1.ItemIndex:=0;
  TipStatDlg.ComboBox1.Clear;
  for i:=0 to Experiment.NTipCat-1 do begin
    TipStatDlg.ComboBox1.Items.Add(Experiment.TipCat[i].Name);
  end;
  TipStatDlg.ComboBox1.ItemIndex:=0;
  UpdateStatus;
end;

procedure TRootViewer.Backspace;
begin
  if (Frame.SelRoot>=0) and (Frame.SelRoot<Frame.NRoots) then begin
    if Frame.Roots[Frame.SelRoot].NNodes>1 then begin
      CopyFrame(Frame, BkpFrame);
      Frame.Roots[Frame.SelRoot].DelNode(Frame.Roots[Frame.SelRoot].NNodes-1);
      Frame.Roots[Frame.SelRoot].FixLabel;
      DrawFrame;
    end;
  end;
end;

procedure TRootViewer.Delete;
begin
  if Frame.SelRoot>=0 then begin
    if (Frame.SelNode>=0) and
      (Frame.Roots[Frame.SelRoot].NNodes>1) then begin
      CopyFrame(Frame, BkpFrame);
      Frame.Roots[Frame.SelRoot].DelNode(Frame.SelNode);
      Frame.SelNode:=-1;
      Frame.Roots[Frame.SelRoot].FixLabel;
    end else
    if (Frame.SelNode<-1) then begin
      CopyFrame(Frame, BkpFrame);
      with Frame.Roots[Frame.SelRoot] do begin
        if Frame.SelNode=-2 then begin
          wm2.x:=wm1.x;
          wm2.y:=wm1.y;
        end else begin
          wm1.x:=wm2.x;
          wm1.y:=wm2.y;
        end;
        AveWidth:=Sqrt(Sqr(Wm1.x-Wm2.x)+Sqr(Wm1.y-Wm2.y));
      end;
    end else begin
      CopyFrame(Frame, BkpFrame);
      Frame.DelRoot(Frame.SelRoot);
      Frame.SelRoot:=-1;
      Frame.SelNode:=-1;
    end;
    DrawFrame;
  end else
  if Frame.SelTip>=0 then begin
    CopyFrame(Frame, BkpFrame);
    Frame.DelTip(Frame.SelTip);
    Frame.SelTip:=-1;
    DrawFrame;
  end;
end;

procedure TRootViewer.Kill;
begin
  if Frame.SelRoot>=0 then begin
    CopyFrame(Frame, BkpFrame);
    Inc(Frame.Roots[Frame.SelRoot].Attribute);
    Frame.SelNode:=-1;
    if Frame.Roots[Frame.SelRoot].Attribute>1 then begin
      Frame.SelRoot:=-1;
      Frame.SelTip:=-1;
    end;
    DrawFrame;
  end else
  if Frame.SelTip>=0 then begin
    CopyFrame(Frame, BkpFrame);
    Inc(Frame.Tips[Frame.SelTip].Attribute);
    Frame.SelNode:=-1;
    if Frame.Tips[Frame.SelTip].Attribute>1 then begin
      Frame.SelRoot:=-1;
      Frame.SelTip:=-1;
    end;
    DrawFrame;
  end;
end;

procedure TRootViewer.Clear;
begin
  CopyFrame(Frame, BkpFrame);
  with Frame do begin
    SelRoot:=-1;
    SelNode:=-1;
    SelTip:=-1;
    NRoots:=0;
    NTips:=0;
    SetLength(Roots, NRoots);
    SetLength(Tips, NTips);
  end;
  DrawFrame;
end;

procedure TRootViewer.Import(DeltaInd: integer=1);
var FN: String;
begin
  CopyFrame(Frame, BkpFrame);
  with Experiment do begin
    FN:=GetDataFileName(TakeIndex-DeltaInd,PlotIndex,TubeIndex)
      +'\'+GetFrameName(PlotIndex,TubeIndex,FrameIndex)+'.rvd';
  end;
  if FileExists(FN) then begin
    Frame.LoadFromFile(FN);
  end;
  if BkpFrame.Comment<>'' then begin
    Frame.Comment:=BkpFrame.Comment;
  end;
  Frame.SelRoot:=-1;
  Frame.SelNode:=-1;
  Frame.SelTip:=-1;
  Frame.isLabelDrag:=false;
  if RegButton.Down then begin
    CopyFrame(Frame, BkpFrame);
  end;
  DrawFrame;
end;

procedure TRootViewer.Undo;
var tmpFrame: TFrame;
begin
  if Frame.isMarkVisible then begin
    CopyFrame(BkpFrame, Frame);
  end else begin
    CopyFrame(Frame, tmpFrame);
    CopyFrame(BkpFrame, Frame);
    CopyFrame(tmpFrame, BkpFrame);
  end;
  DrawFrame;
end;

procedure TRootViewer.CopyFrame(Src: TFrame; var Dest: TFrame);
var i: integer;
begin
  Dest.CopyFrom(Src);
  Dest.isLabelDrag:=false;
  SetLength(Dest.Roots, Dest.NRoots);
  SetLength(Dest.Tips, Dest.NTips);
  for i:=0 to Dest.NRoots-1 do begin
    with Dest.Roots[i] do begin
      SetLength(Nodes, NNodes);
    end;
  end;
end;

function TRootViewer.areFramesEqual(A,B: TFrame): boolean;
var i: integer;
  function AreNodesEQ( a,b: array of TFPoint ): boolean;
  var i: integer;
  begin
    Result:=Length(a)=Length(b);
    if Result then begin
      for i:=0 to Length(a)-1 do begin
        Result:=Result and
          (a[i].x=b[i].x) and
          (a[i].y=b[i].y);
      end;
    end;
  end;
begin
  Result:=
    (A.Comment=B.Comment) and
    (A.W=B.W) and
    (A.H=B.H) and
    (A.Mark.x=B.Mark.x) and
    (A.Mark.y=B.Mark.y) and
    (A.DX=B.DX) and
    (A.DY=B.DY) and
    (A.NRoots=B.NRoots) and
    (A.NTips=B.NTips);
  if Result then begin
    for i:=0 to A.NTips-1 do begin
      Result:=Result and
        (A.Tips[i].Attribute=B.Tips[i].Attribute) and
        (A.Tips[i].Name=B.Tips[i].Name) and
        (A.Tips[i].Comment=B.Tips[i].Comment) and
        (A.Tips[i].Category=B.Tips[i].Category) and
        (A.Tips[i].Tag.x=B.Tips[i].Tag.x) and
        (A.Tips[i].Tag.y=B.Tips[i].Tag.y) and
        (A.Tips[i].isTagFixed=B.Tips[i].isTagFixed) and
        (A.Tips[i].Point.x=B.Tips[i].Point.x) and
        (A.Tips[i].Point.y=B.Tips[i].Point.y);
    end;
  end;
  if Result then begin
    for i:=0 to A.NRoots-1 do begin
      Result:=Result and
        (A.Roots[i].Attribute=B.Roots[i].Attribute) and
        (A.Roots[i].Name=B.Roots[i].Name) and
        (A.Roots[i].Comment=B.Roots[i].Comment) and
        (A.Roots[i].Category=B.Roots[i].Category) and
        (A.Roots[i].Tag.x=B.Roots[i].Tag.x) and
        (A.Roots[i].Tag.y=B.Roots[i].Tag.y) and
        (A.Roots[i].isTagFixed=B.Roots[i].isTagFixed) and
        (A.Roots[i].WM1.x=B.Roots[i].WM1.x) and
        (A.Roots[i].WM1.y=B.Roots[i].WM1.y) and
        (A.Roots[i].WM2.x=B.Roots[i].WM2.x) and
        (A.Roots[i].WM2.y=B.Roots[i].WM2.y) and
        (A.Roots[i].NNodes=B.Roots[i].NNodes) and
        AreNodesEQ(A.Roots[i].Nodes, B.Roots[i].Nodes);
    end;
  end;
end;

procedure TRootViewer.MoveFrame( DX,DY: float );
var i,j: integer;
begin
  for i:=0 to Frame.NTips-1 do begin
    Frame.Tips[i].Tag.x:=BkpFrame.Tips[i].Tag.x+DX;
    Frame.Tips[i].Tag.y:=BkpFrame.Tips[i].Tag.y+DY;
    Frame.Tips[i].Point.x:=BkpFrame.Tips[i].Point.x+DX;
    Frame.Tips[i].Point.y:=BkpFrame.Tips[i].Point.y+DY;
  end;
  for i:=0 to Frame.NRoots-1 do begin
    Frame.Roots[i].Tag.x:=BkpFrame.Roots[i].Tag.x+DX;
    Frame.Roots[i].Tag.y:=BkpFrame.Roots[i].Tag.y+DY;
    Frame.Roots[i].WM1.x:=BkpFrame.Roots[i].WM1.x+DX;
    Frame.Roots[i].WM1.y:=BkpFrame.Roots[i].WM1.y+DY;
    Frame.Roots[i].WM2.x:=BkpFrame.Roots[i].WM2.x+DX;
    Frame.Roots[i].WM2.y:=BkpFrame.Roots[i].WM2.y+DY;
    for j:=0 to Frame.Roots[i].NNodes-1 do begin
      Frame.Roots[i].Nodes[j].x:=BkpFrame.Roots[i].Nodes[j].x+DX;
      Frame.Roots[i].Nodes[j].y:=BkpFrame.Roots[i].Nodes[j].y+DY;
    end;
  end;
end;

procedure TRootViewer.GetFrameDefaults( TakeIndex, PlotIndex, TubeIndex, FrameIndex: integer );
begin
  if (TakeIndex>=0) and (TakeIndex<Experiment.NTakes) then begin
    with Experiment.Takes[TakeIndex] do begin
      if (PlotIndex>=0) and (PlotIndex<Experiment.NSites) then begin
        with Experiment.Sites[PlotIndex] do begin
          if (TubeIndex>=0) and (TubeIndex<NTubes) then begin
            with Experiment.Sites[PlotIndex].Tubes[TubeIndex] do begin
              Frame.W:=Width;
              Frame.H:=Height;
            end;
          end;
        end;
      end;
    end;
  end;
end;

function TRootViewer.UpdateFrame(doUpdate: boolean=true): boolean;
var FN: String;
begin
  Result:=false;
  with Experiment do begin
    if JPEGOK then begin
      if (TakeIndex>=0) and (TakeIndex<NTakes) then begin
        if not areFramesEqual(Frame, OldFrame) then begin
          Result:=true;
          if doUpdate then begin
            FN:=GetDataFileName(TakeIndex, PlotIndex, TubeIndex)+'\'+
              GetCurrFrameName+'.rvd';
            Frame.SaveToFile(FN);
            CopyFrame(Frame,OldFrame);
          end;
        end;
      end;
    end;
  end;
end;

function TRootViewer.SetFrame(NewTake,NewPlot,NewTube,NewFrame: integer): boolean;
var FN: String;
    jOK: boolean;
begin
  Result:=false;
  UpdateFrame;
  with Experiment do begin
    JOK:=JPEGOK and
      (TakeIndex=NewTake) and
      (PlotIndex=NewPlot) and
      (TubeIndex=NewTube) and
      (FrameIndex=NewFrame);
    TakeIndex:=NewTake;
    PlotIndex:=NewPlot;
    TubeIndex:=NewTube;
    FrameIndex:=NewFrame;
    FN:=GetFilePath(Name);
    JPEGOK:=false;
    Frame.Init;
    BkpFrame.Init;
    if (TakeIndex>=0) and (TakeIndex<NTakes) then begin
      GetFrameDefaults( TakeIndex, PlotIndex, TubeIndex, FrameIndex);
      FN:=GetDataFileName(TakeIndex,PlotIndex,TubeIndex)+'\'+
        GetCurrFrameName+'.rvd';
      if FileExists(FN) then begin
        Result:=Frame.LoadFromFile(FN);
      end;
      CopyFrame(Frame, BkpFrame);
      CopyFrame(Frame, OldFrame);
      FN:=GetDataFileName(TakeIndex,PlotIndex,TubeIndex)+'\'+
       GetCurrFrameName+'.jpg';
      if FileExists(FN) then begin
        if not JOK then begin
          StartWorking('Loading Frame...');
          try
            JPEG1.LoadFromFile(FN);
          except
            StopWorking;
            FixImages;
            UpdateStatus;
          end;
        end;
        JPEGOK:=true;
        FixImages;
        DrawFrame;
        Result:=true;
        StopWorking;
      end;
    end;
  end;
  if not Result then begin
    FixImages;
    UpdateStatus;
  end;
end;

{ ------------------------------------------------------- }
{ *** Mouse Interface ----------------------------------- }
{ ------------------------------------------------------- }

procedure TRootViewer.Image1DblClick(Sender: TObject);
var tmpFrame: TFrame;
begin
  if RegButton.Down then begin
    Exit;
  end;
  Screen.Cursor:=crDefault;
  SetCursor(crDefault);
  if (GetAsyncKeyState(vk_Menu) and $8000<>0) or
     (GetAsyncKeyState(vk_Control) and $8000<>0) or
     (GetAsyncKeyState(vk_Shift) and $8000<>0) then begin
    Exit;
  end;
  if Frame.SelRoot>=0 then begin
    with Frame.Roots[Frame.SelRoot] do begin
      CopyFrame(Frame, tmpFrame);
      if RootStatDlg.Execute(Name, Category, Comment, Attribute, AveWidth, NNodes, Nodes) then begin
        CopyFrame(tmpFrame, bkpFrame);
        DrawFrame;
      end;
    end;
  end else
  if Frame.SelTip>=0 then begin
    with Frame.Tips[Frame.SelTip] do begin
      CopyFrame(Frame, tmpFrame);
      if TipStatDlg.Execute(Name, Category, Comment, Attribute) then begin
        CopyFrame(tmpFrame, BkpFrame);
        DrawFrame;
      end;
    end;
  end else begin
    CopyFrame(Frame, tmpFrame);
    if FrameDlg.Execute then begin
      CopyFrame(tmpFrame, BkpFrame);
      DrawFrame;
    end;
  end;
end;

procedure TRootViewer.Image1MouseDown(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
var OldRoot: integer;
begin
  if Button<>mbLeft then begin
    Exit;
  end;
  if not isLButtonDown then begin
    if not RegButton.Down then begin
      Screen.Cursor:=crDefault;
      SetCursor(crDefault);
    end;
    Exit;
  end;
  if RegButton.Down then begin
    if (ssCtrl in Shift) then begin
      Frame.Mark.x:=ScrXToReal(x,Bmp1.Width,Frame.W);
      Frame.Mark.y:=ScrYToReal(y,Bmp1.Height,Frame.H);
    end else begin
      Screen.Cursor:=crSizeAll;
    end;
    Frame.fLDX:=ScrXToReal(x,Bmp1.Width,Frame.W);
    Frame.fLDY:=ScrYToReal(y,Bmp1.Height,Frame.H);
    Mark:=Frame.Mark;
    BkpDX:=Frame.DX;
    BkpDY:=Frame.DY;
    FixFrameDelta;
    DrawFrame;
    Exit;
  end;
  Frame.SelNode:=-1;
  Frame.SelTip:=-1;
  Frame.isLabelDrag:=false;
  if ssCtrl in Shift then begin
    if (GetAsyncKeyState(vk_Control) and $8000<>0) then begin
      if (GetAsyncKeyState(vk_Shift) and $8000<>0) then begin
        Screen.Cursor:=crDrag;
      end else begin
        Screen.Cursor:=crMultiDrag;
      end;
    end;
    Frame.SelRoot:=-1;
    DrawFrame;
    Exit;
  end;
  if (ssAlt in Shift) and (Image1.Cursor=crCross) then begin
    if (Frame.SelRoot>=0) and
      Frame.areRootsVisible and
      Frame.areWidthsVisible and
      Frame.SelWidthMark(Image1.Picture.Bitmap, X,Y) then begin
      with Frame.Roots[Frame.SelRoot] do begin
        CopyFrame(Frame, BkpFrame);
        WM1.x:=ScrXToReal(x,Bmp1.Width,Frame.W);
        WM1.y:=ScrYToReal(y,Bmp1.Height,Frame.H);
        WM2.x:=ScrXToReal(x,Bmp1.Width,Frame.W);
        WM2.y:=ScrYToReal(y,Bmp1.Height,Frame.H);
        if Frame.Roots[Frame.SelRoot].AveWidth>0 then begin
          Frame.Roots[Frame.SelRoot].AveWidth:=
            Sqrt(Sqr(Wm1.x-Wm2.x)+Sqr(Wm1.y-Wm2.y))
        end;
      end;
      DrawFrame;
    end else begin
      SetCursor(crDefault);
    end;
  end else
  if (Frame.SelRoot<0) or not (ssShift in Shift) then begin
    if Frame.GetRootLabelClick(Bmp1, x,y) then begin
      Frame.SelTip:=-1;
      Frame.isLabelDrag:=true;
      Screen.Cursor:=crHandPoint;
    end else
    if Frame.GetTipLabelClick(Bmp1, x,y) then begin
      Frame.SelRoot:=-1;
      Frame.isLabelDrag:=true;
      Screen.Cursor:=crHandPoint;
    end else begin
      OldRoot:=Frame.SelRoot;
      if Frame.SelRoot>=0 then begin
        Frame.SelNode:=Frame.Roots[Frame.SelRoot].SelectNode(
          ScrXToReal(x,Bmp1.Width,Frame.W),
          ScrYToReal(y,Bmp1.Height,Frame.H),
          Bmp1.Width,Bmp1.Height, Frame.areWidthsVisible);
      end;
      if Frame.SelNode=-1 then begin
        Frame.SelObject(Image1.Picture.Bitmap, X,Y);
        if (Frame.SelRoot>=0) and (Frame.SelRoot=OldRoot) then begin
          Frame.SelNode:=Frame.Roots[Frame.SelRoot].SelectNode(
            ScrXToReal(x,Bmp1.Width,Frame.W),
            ScrYToReal(y,Bmp1.Height,Frame.H),
            Bmp1.Width,Bmp1.Height, Frame.areWidthsVisible);
        end;
      end;
      if Frame.SelNode<>-1 then begin
        Screen.Cursor:=crCross;
        CopyFrame(Frame, BkpFrame);
      end else
      if not Frame.isLabelDrag and (Frame.SelTip>=0) then begin
        Screen.Cursor:=crCross;
        CopyFrame(Frame, BkpFrame);
      end;
    end;
  end else begin
    Screen.Cursor:=crCross;
    CopyFrame(Frame, BkpFrame);
    Frame.SelNode:=Frame.Roots[Frame.SelRoot].InsertNode(
      ScrXToReal(x,Bmp1.Width,Frame.W),
      ScrYToReal(y,Bmp1.Height,Frame.H),
      Bmp1.Width,Bmp1.Height);
    if Frame.SelNode<0 then begin
      Frame.NewNode(Bmp1, X,Y);
      Frame.SelNode:=Frame.Roots[Frame.SelRoot].NNodes-1;
    end;
  end;
  DrawFrame;
end;

procedure TRootViewer.Image1MouseUp(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
var oldCursor,oSC: TCursor;
  function isCatShortcut: boolean;
  var i: integer;
  begin
    Result:=false;
    for i:=0 to Experiment.NRootCat-1 do begin
      if not Result then begin
        if Experiment.RootCat[i].Key>0 then begin
          if GetAsyncKeyState(Experiment.RootCat[i].Key) and $8000<>0 then begin
            Result:=true;
            SelDlg.ComboBox1.Text:=Experiment.RootCat[i].Name;
            SelDlg.PageControl1.ActivePage:=SelDlg.TabSheet1;
          end;
        end;
      end;
    end;
    for i:=0 to Experiment.NTipCat-1 do begin
      if not Result then begin
        if Experiment.TipCat[i].Key>0 then begin
          if GetAsyncKeyState(Experiment.TipCat[i].Key) and $8000<>0 then begin
            Result:=true;
            SelDlg.ComboBox2.Text:=Experiment.TipCat[i].Name;
            SelDlg.PageControl1.ActivePage:=SelDlg.TabSheet2;
          end;
        end;
      end;
    end;
  end;
begin
  oldCursor:=Image1.Cursor;
  oSC:=Screen.Cursor;
  Screen.Cursor:=crDefault;
  SetCursor(crDefault);
  if RegButton.Down then begin
    if (Button=mbLeft) then begin
      if (ssCtrl in Shift) and (oSC=crDefault) then begin
        Frame.Mark.x:=ScrXToReal(x,Bmp1.Width,Frame.W);
        Frame.Mark.y:=ScrYToReal(y,Bmp1.Height,Frame.H);
        FixFrameDelta;
      end else
      if not (ssCtrl in Shift) and (OsC=crSizeAll) then begin
        Frame.DX:=BkpDX+
          ScrXToReal(x,Bmp1.Width,Frame.W)-
          Frame.fLDX;
        Frame.DY:=BkpDY+
          ScrYToReal(y,Bmp1.Height,Frame.H)-
          Frame.fLDY;
        Frame.Mark.x:=Mark.x+
          ScrXToReal(x,Bmp1.Width,Frame.W)-
          Frame.fLDX;
        Frame.Mark.y:=Mark.y+
          ScrYToReal(y,Bmp1.Height,Frame.H)-
          Frame.fLDY;
        FixFrameDelta;
        MoveFrame(Frame.DX-BkpFrame.DX,
                  Frame.DY-BkpFrame.DY);
      end;
      DrawFrame;
    end;
    Exit;
  end;
  if Button=mbRight then begin
    if (Frame.SelRoot>=0) or (Frame.SelTip>=0) then begin
      Frame.SelNode:=-1;
      Frame.SelRoot:=-1;
      Frame.SelTip:=-1;
      DrawFrame;
    end;
  end else
  if (Button=mbLeft) and ((ssCtrl in Shift) or isCatShortcut) then begin
    Frame.isLabelDrag:=false;
    Frame.SelNode:=-1;
    Frame.SelTip:=-1;
    Frame.SelRoot:=-1;
    SelDlg.Edit1.Text:=Frame.GetNextRootName;
    SelDlg.Edit3.Text:=Frame.GetNextTipName;
    if (ssShift in Shift) or isCatShortcut or
      SelDlg.Execute(Frame.GetNextRootName,Frame.GetNextTipName) then begin
      if SelDlg.PageControl1.ActivePage.TabIndex=0 then begin
        CopyFrame(Frame, BkpFrame);
        Frame.NewRoot;
        with Frame.Roots[Frame.SelRoot] do begin
          Name:=SelDlg.Edit1.Text;
          Comment:=SelDlg.Edit2.Text;
          Category:=SelDlg.ComboBox1.Text;
        end;
        Frame.NewNode(Bmp1, X,Y);
        Frame.SelNode:=Frame.Roots[Frame.SelRoot].NNodes-1;
      end else begin
        CopyFrame(Frame, BkpFrame);
        Frame.NewTip;
        with Frame.Tips[Frame.SelTip] do begin
          Name:=SelDlg.Edit3.Text;
          Comment:=SelDlg.Edit4.Text;
          Category:=SelDlg.ComboBox2.Text;
          Point.x:=ScrXToReal(x,Bmp1.Width,Frame.W);
          Point.y:=ScrYToReal(y,Bmp1.Height,Frame.H);
          Tag.x:=Point.x;
          Tag.y:=Point.y;
        end;
      end;
    end;
    DrawFrame;
  end else
  if (Button=mbLeft) and (Frame.SelRoot>=0) and
    (ssAlt in Shift) and (oldCursor=crCross) then begin
    if (Frame.SelRoot>=0) and
      Frame.areRootsVisible and
      Frame.areWidthsVisible and
      Frame.SelWidthMark(Image1.Picture.Bitmap, X,Y) then begin
      with Frame.Roots[Frame.SelRoot] do begin
        WM2.x:=ScrXToReal(x,Bmp1.Width,Frame.W);
        WM2.y:=ScrYToReal(y,Bmp1.Height,Frame.H);
        Frame.Roots[Frame.SelRoot].AveWidth:=
          Sqrt(Sqr(Wm1.x-Wm2.x)+Sqr(Wm1.y-Wm2.y));
      end;
      Frame.Roots[Frame.SelRoot].FixWidthMark;
      DrawFrame;
    end;
  end;
  if (Frame.SelRoot>=0) and
    Frame.areRootsVisible and
    Frame.areWidthsVisible then begin
    if Frame.Roots[Frame.SelRoot].FixWidthMark then begin
      DrawFrame;
    end;
  end;
end;

procedure TRootViewer.Image1MouseMove(Sender: TObject; Shift: TShiftState;
  X, Y: Integer);
begin
  StatusBar1.Panels.Items[5].Text:=
    IntToStr(x)+', '+IntToStr(y);
  StatusBar1.Panels.Items[6].Text:=
    WrFloatStr(ScrXToReal(x,Bmp1.Width,Frame.W),2)+', '+
    WrFloatStr(ScrYToReal(y,Bmp1.Height,Frame.H),2)+' '+Pref.Units;
  if RegButton.Down then begin
    if (ssLeft in Shift) then begin
      if (ssCtrl in Shift) and (Image1.Cursor<>crSizeAll) then begin
        Frame.Mark.x:=ScrXToReal(x,Bmp1.Width,Frame.W);
        Frame.Mark.y:=ScrYToReal(y,Bmp1.Height,Frame.H);
        FixFrameDelta;
      end else
      if not (ssCtrl in Shift) and (Screen.Cursor=crSizeAll) then begin
        Frame.DX:=BkpDX+
          ScrXToReal(x,Bmp1.Width,Frame.W)-
          Frame.fLDX;
        Frame.DY:=BkpDY+
          ScrYToReal(y,Bmp1.Height,Frame.H)-
          Frame.fLDY;
        Frame.Mark.x:=Mark.x+
          ScrXToReal(x,Bmp1.Width,Frame.W)-
          Frame.fLDX;
        Frame.Mark.y:=Mark.y+
          ScrYToReal(y,Bmp1.Height,Frame.H)-
          Frame.fLDY;
        FixFrameDelta;
        MoveFrame(Frame.DX-BkpFrame.DX,
                  Frame.DY-BkpFrame.DY);
      end;
      DrawFrame;
    end;
    Exit;
  end;
  if (ssLeft in Shift) then begin
    {Dragging}
    if Frame.SelRoot>=0 then begin
      if Frame.isLabelDrag then begin
        Frame.Roots[Frame.SelRoot].Tag.x:=
          ScrXToReal(Frame.LDX+x,Bmp1.Width,Frame.W);
        Frame.Roots[Frame.SelRoot].Tag.y:=
          ScrYToReal(Frame.LDY+y,Bmp1.Height,Frame.H);
        Frame.Roots[Frame.SelRoot].isTagFixed:=true;
        DrawFrame;
      end else
      if Frame.SelNode>=0 then begin
        Frame.Roots[Frame.SelRoot].Nodes[Frame.SelNode].x:=
          ScrXToReal(x,Bmp1.Width,Frame.W);
        Frame.Roots[Frame.SelRoot].Nodes[Frame.SelNode].y:=
          ScrYToReal(y,Bmp1.Height,Frame.H);
        Frame.Roots[Frame.SelRoot].FixLabel;
        DrawFrame;
      end else
      if Frame.SelNode<-1 then begin
        if (Frame.SelRoot>=0) and Frame.areRootsVisible and Frame.areWidthsVisible then begin
          with Frame.Roots[Frame.SelRoot] do begin
            if Frame.SelNode=-2 then begin
              WM1.x:=ScrXToReal(x,Bmp1.Width,Frame.W);
              WM1.y:=ScrYToReal(y,Bmp1.Height,Frame.H);
            end else begin
              WM2.x:=ScrXToReal(x,Bmp1.Width,Frame.W);
              WM2.y:=ScrYToReal(y,Bmp1.Height,Frame.H);
            end;
            Frame.Roots[Frame.SelRoot].AveWidth:=
              Sqrt(Sqr(Wm1.x-Wm2.x)+Sqr(Wm1.y-Wm2.y));
          end;
          DrawFrame;
        end;
      end else
      if (ssAlt in Shift) and (Image1.Cursor=crCross) then begin
        if (Frame.SelRoot>=0) and
          Frame.areRootsVisible and
          Frame.areWidthsVisible and
          Frame.SelWidthMark(Image1.Picture.Bitmap, X,Y) then begin
          with Frame.Roots[Frame.SelRoot] do begin
            WM2.x:=ScrXToReal(x,Bmp1.Width,Frame.W);
            WM2.y:=ScrYToReal(y,Bmp1.Height,Frame.H);
            Frame.Roots[Frame.SelRoot].AveWidth:=
              Sqrt(Sqr(Wm1.x-Wm2.x)+Sqr(Wm1.y-Wm2.y));
          end;
          DrawFrame;
        end;
      end;
    end else
    if Frame.SelTip>=0 then begin
      if Frame.isLabelDrag then begin
        Frame.Tips[Frame.SelTip].Tag.x:=
          ScrXToReal(Frame.LDX+x,Bmp1.Width,Frame.W);
        Frame.Tips[Frame.SelTip].Tag.y:=
          ScrYToReal(Frame.LDY+y,Bmp1.Height,Frame.H);
        Frame.Tips[Frame.SelTip].isTagFixed:=true;
        DrawFrame;
      end else begin
        Frame.Tips[Frame.SelTip].Point.x:=
          ScrXToReal(x,Bmp1.Width,Frame.W);
        Frame.Tips[Frame.SelTip].Point.y:=
          ScrYToReal(y,Bmp1.Height,Frame.H);
        DrawFrame;
      end;
    end;
  end;
end;

{ ------------------------------------------------------- }
{ *** Interface  ---------------------------------------- }
{ ------------------------------------------------------- }

procedure TRootViewer.wmDropFiles(var Msg: TMessage );
const maxlen = 256*16;
var FN: PChar;
    num: integer;
  function OpenPrj(FN: String): boolean;
  var s: String;
  begin
    Result:=false;
    FN:=GetLongFileName(FN);
    s:=GetFileType(FN);
    if (Length(s)>0) and
       (StrIComp(@s[1],'.rvp')=0) then begin
      OpenProject(FN);
      Result:=true;
    end else begin
      WarningMsg('"'+FN+'" is not a valid RootView Project File');
    end;
  end;
begin
  inherited;
  GetMem(FN,maxlen);
  num:=DragQueryFile(Msg.wParam,$FFFFFFFF,FN,maxlen-1);
  if (num>0) then begin
    Application.BringToFront;
    Application.Restore;
    if DragQueryFile(Msg.wParam,0,FN,maxlen-1)>0 then begin
      OpenPrj(FN);
    end;
  end;
  FreeMem(FN,maxlen);
  DragFinish(Msg.wParam);
  Msg.Result:=0;
end;

procedure TRootViewer.FormShow(Sender: TObject);
var FN: String;
  function GetFN: String;
  var s: String;
      i: integer;
  begin
    s:=ParamStr(1);
    for i:=2 to ParamCount do begin
      s:=s+' '+ParamStr(i);
    end;
    Result:=s;
  end;
begin
  DragAcceptFiles(Handle, true);
  ImportFrame1.Shortcut:=Shortcut(vk_Right,[ssCtrl]);
  ImportNewerFrame1.Shortcut:=Shortcut(vk_Left,[ssCtrl]);
  Exit1.Shortcut:=Shortcut(vk_F4,[ssAlt]);
  Back1.Shortcut:=vk_Back;
  Deselect1.Shortcut:=vk_Escape;
  FirstMeasurement1.Shortcut:=vk_Home;
  LastMeasurement1.Shortcut:=vk_End;
  Nextmeasurement1.Shortcut:=vk_Right;
  PreviousMeasurement1.Shortcut:=vk_Left;
  FirstFrame1.Shortcut:=vk_Prior;
  LastFrame1.Shortcut:=vk_Next;
  PreviousFrame1.Shortcut:=vk_Up;
  NextFrame1.Shortcut:=vk_Down;
  StartNextFrame1.Shortcut:=vk_Return;
  JPEGOK:=false;
  Pref.Init;
  Experiment.Init;
  Bmp1:=TBitmap.Create;
  JPEG1:=TJPEGImage.Create;
  FN:=ParamStr(0);
  FN:=GetLongFileName(FN);
  HlpFile:=GetFilePath(FN)+'\'+GetFileName(FN)+'.hlp';
  Zoom:=1.00;
  JPEGStretch:=Pref.DefStretch;
  LabelsBtn.Down:=Pref.ShowLabels;
  LabelsBtn.Down:=Pref.ShowLabels;
  RootsBtn.Down:=Pref.ShowRoots;
  TipsBtn.Down:=Pref.ShowTips;
  WidthsBtn.Down:=Pref.ShowWidths;
  ColorsBtn.Down:=Pref.UseColors;
  GhostBtn.Down:=Pref.ShowGhosts;
  Image1.Width:=0;
  Image1.Height:=0;
  FixImages;
  if ParamCount>0 then begin
    FN:=GetFileType(GetFN);
    if (Length(FN)>0) and
       (StrIComp(@FN[1],'.rvp')=0) then begin
      FN:=GetLongFileName(GetFN);
      OpenProject(FN);
    end;
  end;
  UpdateStatus;
  SetupToolsMenu(Tools1);
end;

procedure TRootViewer.FormDestroy(Sender: TObject);
begin
  JPEGOK:=false;
  Bmp1.Free;
  JPEG1.Free;
end;

procedure TRootViewer.Preferences1Click(Sender: TObject);
begin
  if PrefDlg.Execute then begin
    Experiment.WrPref;
    DrawFrame;
  end;
end;

procedure TRootViewer.Categories1Click(Sender: TObject);
begin
  if DataDlg.Execute then begin
    Experiment.WrCats;
    FixCategories;
  end;
  DrawFrame;
end;

procedure TRootViewer.Exit1Click(Sender: TObject);
begin
  Close;
end;

procedure TRootViewer.DelBtnClick(Sender: TObject);
begin
  Delete;
end;

procedure TRootViewer.BackBtnClick(Sender: TObject);
begin
  Backspace;
end;

procedure TRootViewer.Delete1Click(Sender: TObject);
begin
  Delete;
end;

procedure TRootViewer.Back1Click(Sender: TObject);
begin
  Backspace;
end;

procedure TRootViewer.Deselect1Click(Sender: TObject);
begin
  if (Frame.SelRoot>=0) or (Frame.SelTip>=0) then begin
    Frame.SelNode:=-1;
    Frame.SelRoot:=-1;
    Frame.SelTip:=-1;
    DrawFrame;
  end;
end;

procedure TRootViewer.RegButtonClick(Sender: TObject);
begin
  Frame.SelRoot:=-1;
  Frame.SelNode:=-1;
  Frame.SelTip:=-1;
  Frame.isLabelDrag:=false;
  if RegButton.Down then begin
    CopyFrame(Frame, BkpFrame);
  end;
  DrawFrame;
end;

procedure TRootViewer.RealSizeBtnClick(Sender: TObject);
begin
  JPEGStretch:=false;
  Zoom:=1;
  FixImages;
  UpdateStatus;
end;

procedure TRootViewer.StretchBtnClick(Sender: TObject);
begin
  JPEGStretch:=true;
  Zoom:=1;
  FixImages;
  UpdateStatus;
end;

procedure TRootViewer.ZoomInBtnClick(Sender: TObject);
begin
  Zoom:=Zoom*Pref.ZoomFactor;
  FixImages;
  UpdateStatus;
end;

procedure TRootViewer.ZoomOutBtnClick(Sender: TObject);
begin
  Zoom:=Zoom/Pref.ZoomFactor;
  FixImages;
  UpdateStatus;
end;

procedure TRootViewer.LabelsBtnClick(Sender: TObject);
begin
  if not LabelsBtn.Down then begin
    Frame.isLabelDrag:=false;
  end;
  DrawFrame;
end;

procedure TRootViewer.RootsBtnClick(Sender: TObject);
begin
  if not RootsBtn.Down then begin
    Frame.SelRoot:=-1;
    Frame.SelNode:=-1;
  end;
  DrawFrame;
end;

procedure TRootViewer.TipsBtnClick(Sender: TObject);
begin
  if not TipsBtn.Down then begin
    Frame.SelTip:=-1;
  end;
  DrawFrame;
end;

procedure TRootViewer.WidthsBtnClick(Sender: TObject);
begin
  DrawFrame;
end;

procedure TRootViewer.UndoBtnClick(Sender: TObject);
begin
  Undo;
end;

procedure TRootViewer.Undo1Click(Sender: TObject);
begin
  Undo;
end;

procedure TRootViewer.AltUndo1Click(Sender: TObject);
begin
  Undo;
end;

procedure TRootViewer.WriteOutput;
begin
  UpdateFrame;
  OutDataDlg.Execute;
end;

procedure TRootViewer.SelectPlot;
begin
  LayoutDlg.Execute(1);
end;

procedure TRootViewer.UpdateStatus;
{Update Status Line, Menu and Tool Bar}
var i: integer;
begin
  with Experiment do begin
    ZoomInBtn.Enabled:=Zoom<Pref.MaxZoom;
    ZoomOutBtn.Enabled:=Zoom>Pref.MinZoom;
    RealSizeBtn.Enabled:=(Abs(Zoom-1)>eps) or JPEGStretch;
    StretchBtn.Enabled:=(Abs(Zoom-1)>eps) or not JPEGStretch;
    Close1.Enabled:=TakeIndex>=0;
    OutputBtn.Enabled:=TakeIndex>=0;
    SelectPlot1.Enabled:=OutputBtn.Enabled;
    CreateOutput1.Enabled:=OutputBtn.Enabled;
    Save1.Enabled:=(TakeIndex>=0) and UpdateFrame(false);
    NextFrameBtn.Enabled:=(TakeIndex>=0) and
      (FrameIndex<Sites[PlotIndex].Tubes[TubeIndex].Frames-1);
    LastFrameBtn.Enabled:=NextFrameBtn.Enabled;
    PrevFrameBtn.Enabled:=FrameIndex>0;
    FirstFrameBtn.Enabled:=PrevFrameBtn.Enabled;
    NextTakeBtn.Enabled:=TakeIndex<NTakes-1;
    LastTakeBtn.Enabled:=NextTakeBtn.Enabled;
    PrevTakeBtn.Enabled:=TakeIndex>0;
    FirstTakeBtn.Enabled:=PrevTakeBtn.Enabled;
    ImportBtn.Enabled:=JPEGOK and (FrameIndex>=0) and (TakeIndex>0);
    if ImportBtn.Enabled then begin
      ImportBtn.Enabled:=
        FileExists(GetDataFileName( TakeIndex-1, PlotIndex, TubeIndex)
          +'\'+GetCurrFrameName+'.rvd');
    end;
    ImportFrame1.Enabled:=ImportBtn.Enabled;
    ImportNewerFrame1.Enabled:=JPEGOK and (FrameIndex>=0) and (TakeIndex<NTakes-1);
    if ImportNewerFrame1.Enabled then begin
      ImportNewerFrame1.Enabled:=
        FileExists(GetDataFileName( TakeIndex+1, PlotIndex, TubeIndex)
          +'\'+GetCurrFrameName+'.rvd');
    end;
    Clear1.Enabled:=(Frame.NRoots>0) or (Frame.NTips>0);
  end;
  if not JPEGOK then begin
    for i:=1 to StatusBar1.Panels.Count-1 do begin
      StatusBar1.Panels[i].Text:='';
    end;
    Deselect1.Enabled:=false;
    Delete1.Enabled:=false;
    Back1.Enabled:=false;
    Undo1.Enabled:=false;
    KillBtn.Enabled:=false;
    Kill1.Enabled:=false;
    with Experiment do begin
      if TakeIndex>=0 then begin
        StatusBar1.Panels[1].Text:=GetCurrTakeName;
        StatusBar1.Panels[2].Text:=GetCurrPlotName;
        StatusBar1.Panels[3].Text:=GetCurrTubeName;
        StatusBar1.Panels[4].Text:=GetCurrFrameName;
      end;
    end;
  end else begin
    Deselect1.Enabled:=(Frame.SelRoot>=0) or (Frame.SelTip>=0);
    Delete1.Enabled:=(Frame.SelRoot>=0) or (Frame.SelTip>=0);
    KillBtn.Enabled:=false;
    if Frame.SelRoot>=0 then begin
      KillBtn.Enabled:=Frame.Roots[Frame.SelRoot].Attribute<2;
    end else
    if Frame.SelTip>=0 then begin
      KillBtn.Enabled:=Frame.Tips[Frame.SelTip].Attribute<2;
    end;
    Kill1.Enabled:=KillBtn.Enabled;
    Back1.Enabled:=(Frame.SelRoot>=0);
    Undo1.Enabled:=not areFramesEqual(Frame, BkpFrame);
    StatusBar1.Panels[1].Text:=GetCurrTakeName;
    StatusBar1.Panels[2].Text:=GetCurrPlotName;
    StatusBar1.Panels[3].Text:=GetCurrTubeName;
    StatusBar1.Panels[4].Text:=GetCurrFrameName;
    if Frame.SelRoot>=0 then begin
      StatusBar1.Panels[7].Text:=
        Frame.Roots[Frame.SelRoot].Name+
        '  [L: '+WrFloatStr(Frame.Roots[Frame.SelRoot].GetLen, 2)+','+
        '  W: '+WrFloatStr(Frame.Roots[Frame.SelRoot].AveWidth, 2)+']'+
        '  ('+Frame.Roots[Frame.SelRoot].Category+')';
    end else
    if Frame.SelTip>=0 then begin
      StatusBar1.Panels[7].Text:=
        Frame.Tips[Frame.SelTip].Name+
        '  ('+Frame.Tips[Frame.SelTip].Category+')';
    end else begin
      if RegButton.Down then begin
        StatusBar1.Panels[7].Text:='Reg. Error: '+
          WrFloatStr(Frame.DX,2)+', '+
          WrFloatStr(Frame.DY,2);
      end else begin
        StatusBar1.Panels[7].Text:='Image: '+
          IntToStr(Bmp1.Width)+' x '+IntToStr(Bmp1.Height)+
        '  ('+WrFloatStr(Frame.W,2)+' x '
             +WrFloatStr(Frame.H,2)+' '+Pref.Units+')';
      end;
    end;
  end;
  FirstMeasurement1.Enabled:=FirstTakeBtn.Enabled;
  LastMeasurement1.Enabled:=LastTakeBtn.Enabled;
  Nextmeasurement1.Enabled:=NextTakeBtn.Enabled;
  PreviousMeasurement1.Enabled:=PrevTakeBtn.Enabled;
  FirstFrame1.Enabled:=FirstFrameBtn.Enabled;
  LastFrame1.Enabled:=LastFrameBtn.Enabled;
  PreviousFrame1.Enabled:=PrevFrameBtn.Enabled;
  NextFrame1.Enabled:=NextFrameBtn.Enabled;
  StartNextFrame1.Enabled:=NextFrameBtn.Enabled;
  RealSize1.Enabled:=RealSizeBtn.Enabled;
  FitImagetoClient1.Enabled:=StretchBtn.Enabled;
  ZoomIn1.Enabled:=ZoomInBtn.Enabled;
  ZoomOut1.Enabled:=ZoomOutBtn.Enabled;
  AltUndo1.Enabled:=Undo1.Enabled;
  BackBtn.Enabled:=Back1.Enabled;
  DelBtn.Enabled:=Delete1.Enabled;
  UndoBtn.Enabled:=Undo1.Enabled;
  Restore1.Enabled:=Save1.Enabled;
  if Restore1.Enabled then begin
    with Experiment do begin
      Restore1.Enabled:=
        FileExists(GetDataFileName( TakeIndex, PlotIndex, TubeIndex)
          +'\'+GetCurrFrameName+'.rvd');
    end;
  end;
end;

procedure TRootViewer.OpenProject(FN: String);
begin
  UpdateFrame;
  StartWorking('Loading Project...');
  BkpFrame.Init;
  Frame.Init;
  JPEGOK:=false;
  UpdateStatus;
  FixImages;
  Experiment.Init;
  Experiment.Name:=FN;
  Caption:='Root View - ['+FN+']';
  Application.Title:=Caption;
  ChDir(GetFilePath(FN));
  if not Experiment.GetData then begin
    Experiment.TakeIndex:=-1;
  end else begin
    with Experiment do begin
      SetFrame(NTakes-1,0,0,0);
    end;
  end;
  StopWorking;
end;

procedure TRootViewer.Open1Click(Sender: TObject);
var FN: String;
begin
  OpenDialog1.Filter:='RootView Project|*.rvp';
  OpenDialog1.FileName:='';
  if OpenDialog1.Execute then begin
    FN:=OpenDialog1.FileName;
    OpenDialog1.InitialDir:='';
    OpenProject(FN);
    LayoutDlg.Execute(0);
  end;
end;

procedure TRootViewer.StatusBar1MouseUp(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
  function GetPanel(x: integer): integer;
  var i,a,b: integer;
  begin
    Result:=-1;
    a:=0;
    for i:=0 to StatusBar1.Panels.Count-1 do begin
      b:=a+StatusBar1.Panels[i].Width;
      if (x>a) and (x<b) then begin
        Result:=i;
      end;
      a:=b;
    end;
  end;
  procedure DlgExec( ind: integer );
  begin
    if OutputBtn.Enabled then begin
      LayoutDlg.Execute(ind);
    end;
  end;
begin
  if Button=mbLeft then begin
    case GetPanel(x) of
      0: WinHelp(Handle, @HlpFile[1], Help_Key, Integer(PChar('Status Line')));
      1: DlgExec(0);
      2: DlgExec(1);
      3: DlgExec(2);
      4: DlgExec(3);
      else {OK}
    end;
  end;
end;

procedure TRootViewer.ImportBtnClick(Sender: TObject);
begin
  Import;
end;

procedure TRootViewer.Save1Click(Sender: TObject);
begin
  UpdateFrame;
  CopyFrame(Frame, OldFrame);
  UpdateStatus;
end;

procedure TRootViewer.FormCloseQuery(Sender: TObject;
  var CanClose: Boolean);
begin
  CanClose:=false;
  WinHelp(Handle, @HlpFile[1], Help_Quit, 0);
  UpdateFrame;
  CanClose:=true;
end;

procedure TRootViewer.Restore1Click(Sender: TObject);
begin
  CopyFrame(Frame, BkpFrame);
  CopyFrame(OldFrame, Frame);
  DrawFrame;
end;

procedure TRootViewer.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if JPEGOK then begin
    if RegButton.Down then begin
      if (GetAsyncKeyState(vk_Control) and $8000<>0) then begin
        if Screen.Cursor=crDefault then begin
          SetCursor(crCross);
        end;
      end;
    end else begin
      if isLButtonDown then begin
        if (GetAsyncKeyState(vk_Control) and $8000<>0) then begin
          if (GetAsyncKeyState(vk_Shift) and $8000<>0) then begin
            Screen.Cursor:=crDrag;
           end else begin
            Screen.Cursor:=crMultiDrag;
          end;
        end;
      end else
      if (Key=vk_Menu) or (Key=vk_Shift) then begin
        if (Frame.SelRoot>=0) and
          Frame.areRootsVisible and Frame.areWidthsVisible then begin
          SetCursor(crCross);
        end;
      end;
    end;
  end;
end;

procedure TRootViewer.FormKeyUp(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if isLButtonDown then begin
    if RegButton.Down then begin
      if Key=vk_Control then begin
       SetCursor(crDefault);
      end;
    end else
    if (Key=vk_Control) or (Key=vk_Shift) and
      ((Image1.Cursor=crDefault) or (Image1.Cursor=crDrag) or (Image1.Cursor=crMultiDrag)) then begin
      if (GetAsyncKeyState(vk_Control) and $8000<>0) then begin
        if (GetAsyncKeyState(vk_Shift) and $8000<>0) then begin
          Screen.Cursor:=crDrag;
        end else begin
          Screen.Cursor:=crMultiDrag;
        end;
      end else begin
        Screen.Cursor:=crDefault;
        SetCursor(crDefault);
      end;
    end else begin
      Screen.Cursor:=crDefault;
      SetCursor(crDefault);
    end;
  end else begin
    Screen.Cursor:=crDefault;
    SetCursor(crDefault);
  end;
end;

procedure TRootViewer.ScrollBox1Enter(Sender: TObject);
begin
  ActiveControl:=nil;
end;

procedure TRootViewer.Contents1Click(Sender: TObject);
begin
  WinHelp(Handle, @HlpFile[1], Help_Contents, 0);
end;

procedure TRootViewer.Index1Click(Sender: TObject);
begin
  WinHelp(Handle, @HlpFile[1], Help_Key, Integer(PChar('')));
end;

procedure TRootViewer.KillBtnClick(Sender: TObject);
begin
  Kill;
end;

procedure TRootViewer.Clear1Click(Sender: TObject);
begin
  Clear;
end;

procedure TRootViewer.ToolButton1Click(Sender: TObject);
begin
  with Experiment do begin
    SetFrame(0, PlotIndex, TubeIndex, FrameIndex);
  end;
end;

procedure TRootViewer.PrevFrameBtnClick(Sender: TObject);
begin
  with Experiment do begin
    SetFrame(TakeIndex, PlotIndex, TubeIndex, FrameIndex-1);
  end;
end;

procedure TRootViewer.NextTakeBtnClick(Sender: TObject);
begin
  with Experiment do begin
    SetFrame(TakeIndex+1, PlotIndex, TubeIndex, FrameIndex);
  end;
end;

procedure TRootViewer.LastTakeBtnClick(Sender: TObject);
begin
  with Experiment do begin
    SetFrame(NTakes-1, PlotIndex, TubeIndex, FrameIndex);
  end;
end;

procedure TRootViewer.ToolButton3Click(Sender: TObject);
begin
  with Experiment do begin
    SetFrame(TakeIndex, PlotIndex, TubeIndex, FrameIndex+1);
  end;
end;

procedure TRootViewer.FirstFrameBtnClick(Sender: TObject);
begin
  with Experiment do begin
    SetFrame(TakeIndex, PlotIndex, TubeIndex, 0);
  end;
end;

procedure TRootViewer.FirstTakeBtnClick(Sender: TObject);
begin
  with Experiment do begin
    SetFrame(0, PlotIndex, TubeIndex, FrameIndex);
  end;
end;

procedure TRootViewer.PrevTakeBtnClick(Sender: TObject);
begin
  with Experiment do begin
    SetFrame(TakeIndex-1, PlotIndex, TubeIndex, FrameIndex);
  end;
end;

procedure TRootViewer.LastFrameBtnClick(Sender: TObject);
begin
  with Experiment do begin
    SetFrame(TakeIndex, PlotIndex, TubeIndex,
      Sites[PlotIndex].Tubes[TubeIndex].Frames-1);
  end;
end;

procedure TRootViewer.ImportFrame1Click(Sender: TObject);
begin
  Import;
end;

procedure TRootViewer.ToggleEditMode1Click(Sender: TObject);
begin
  RegButton.Down:=not RegButton.Down;
  RegButtonClick(Sender);
end;

procedure TRootViewer.ColorsBtnClick(Sender: TObject);
begin
  FixImages;
end;

procedure TRootViewer.GhostBtnClick(Sender: TObject);
begin
  Frame.SelRoot:=-1;
  Frame.SelNode:=-1;
  Frame.SelTip:=-1;
  DrawFrame;
end;

procedure TRootViewer.OutputBtnClick(Sender: TObject);
begin
  WriteOutput;
end;

procedure TRootViewer.FirstMeasurement1Click(Sender: TObject);
begin
  FirstTakeBtn.Click;
end;

procedure TRootViewer.LastMeasurement1Click(Sender: TObject);
begin
  LastTakeBtn.Click;
end;

procedure TRootViewer.PreviousMeasurement1Click(Sender: TObject);
begin
  PrevTakeBtn.Click;
end;

procedure TRootViewer.Nextmeasurement1Click(Sender: TObject);
begin
  NextTakeBtn.Click;
end;

procedure TRootViewer.FirstFrame1Click(Sender: TObject);
begin
  FirstFrameBtn.Click;
end;

procedure TRootViewer.LastFrame1Click(Sender: TObject);
begin
  LastFrameBtn.Click;
end;

procedure TRootViewer.PreviousFrame1Click(Sender: TObject);
begin
  PrevFrameBtn.Click;
end;

procedure TRootViewer.NextFrameBtnClick(Sender: TObject);
begin
  with Experiment do begin
    SetFrame(TakeIndex, PlotIndex, TubeIndex, FrameIndex+1);
  end;
end;

procedure TRootViewer.NextFrame1Click(Sender: TObject);
begin
  NextFrameBtn.Click;
end;

procedure TRootViewer.RealSize1Click(Sender: TObject);
begin
  RealSizeBtn.Click;
end;

procedure TRootViewer.FitImagetoClient1Click(Sender: TObject);
begin
  StretchBtn.Click;
end;

procedure TRootViewer.ZoomIn1Click(Sender: TObject);
begin
  ZoomInBtn.Click;
end;

procedure TRootViewer.ZoomOut1Click(Sender: TObject);
begin
  ZoomOutBtn.Click;
end;

procedure TRootViewer.SelectPlot1Click(Sender: TObject);
begin
  SelectPlot;
end;

procedure TRootViewer.CreateOutput1Click(Sender: TObject);
begin
  WriteOutput;
end;

procedure TRootViewer.Close1Click(Sender: TObject);
begin
  UpdateFrame;
  BkpFrame.Init;
  Frame.Init;
  JPEGOK:=false;
  Experiment.Init;
  FixImages;
  UpdateStatus;
  Caption:='Root View';
  Application.Title:=Caption;
end;

procedure TRootViewer.About1Click(Sender: TObject);
begin
  AboutDlg.ShowModal;
end;

procedure TRootViewer.Kill1Click(Sender: TObject);
begin
  Kill;
end;

procedure TRootViewer.StartNextFrame1Click(Sender: TObject);
begin
  with Experiment do begin
    SetFrame(0, PlotIndex, TubeIndex, FrameIndex+1);
  end;
end;

procedure TRootViewer.ImportNewerFrame1Click(Sender: TObject);
begin
  Import(-1);
end;

procedure TRootViewer.Setup1Click(Sender: TObject);
begin
  if MenuDlg.Execute then begin
    Experiment.WrPref;
    SetupToolsMenu(Tools1);
  end;
end;

procedure TRootViewer.FormResize(Sender: TObject);
begin
  if JPEGOK and JPEGStretch then begin
    StretchBtn.Enabled:=true;
    StretchBtn.Click;
  end;
end;

end.

