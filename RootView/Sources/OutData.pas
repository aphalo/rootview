unit OutData;

interface

uses Windows, SysUtils, Classes, Graphics, Forms, Controls, StdCtrls,
  Buttons, ExtCtrls, OkCancl2, ComCtrls, CheckLst, Data, Prefer;

type
  TOutDataDlg = class(TOKRightDlg)
    HelpBtn: TButton;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    GroupBox1: TGroupBox;
    ProgressBar1: TProgressBar;
    Button1: TButton;
    TabSheet2: TTabSheet;
    GroupBox2: TGroupBox;
    GroupBox3: TGroupBox;
    CheckListBox1: TCheckListBox;
    CheckListBox2: TCheckListBox;
    SpeedButton1: TSpeedButton;
    SpeedButton2: TSpeedButton;
    Label1: TLabel;
    Label2: TLabel;
    Edit1: TEdit;
    Edit2: TEdit;
    Label3: TLabel;
    Label4: TLabel;
    Edit3: TEdit;
    Edit4: TEdit;
    TabSheet3: TTabSheet;
    GroupBox10: TGroupBox;
    Label12: TLabel;
    Label13: TLabel;
    Label14: TLabel;
    CheckBox9: TCheckBox;
    Edit12: TEdit;
    Edit13: TEdit;
    Edit14: TEdit;
    Label5: TLabel;
    Edit5: TEdit;
    Tags: TTabSheet;
    Edit6: TEdit;
    ListBox1: TListBox;
    procedure HelpBtnClick(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure ListBox1Click(Sender: TObject);
    procedure ListBox1DblClick(Sender: TObject);
    procedure Edit6Change(Sender: TObject);
    procedure PageControl1Change(Sender: TObject);
    procedure PageControl1Changing(Sender: TObject;
      var AllowChange: Boolean);
    procedure CancelBtnClick(Sender: TObject);
    procedure OKBtnClick(Sender: TObject);
  private
    { Private declarations }
    Data: TPref;
    procedure SetupPage;
    function UpdatePage: boolean;
  public
    { Public declarations }
    function Execute: Boolean;
  end;

var
  OutDataDlg: TOutDataDlg;

implementation

{$R *.DFM}

uses Roots;

procedure IndicateProgress( c,t,x,a,b: integer );
begin
  if t<=0 then t:=1;
  with OutDataDlg do begin
    ProgressBar1.Position:=Round(100*c/t);
    Edit1.Text:=IntToStr(c);
    Edit2.Text:=IntToStr(x);
    Edit3.Text:=IntToStr(a);
    Edit4.Text:=IntToStr(b);
    if c mod 100=0 then begin
      UpdateWindow(Handle);
    end;
  end;
end;

function isFrameOK( Take, Plot, Tube, Frame: integer ): boolean;
begin
  Result:=
    OutDataDlg.CheckListBox1.Checked[Plot] and
    OutDataDlg.CheckListBox2.Checked[Take];
end;

function GetFrameCount: integer;
var c,i,j,k,l: integer;
begin
  c:=0;
  k:=0;
  with Experiment do begin
    for i:=0 to NSites-1 do begin
      with Sites[i] do begin
        for j:=0 to NTubes-1 do begin
          with Tubes[j] do begin
            for l:=0 to NTakes-1 do begin
              if isFrameOK(l,i,j,k) then begin
                Inc(c, Frames);
              end;
            end;
          end;
        end;
      end;
    end;
  end;
  Result:=c;
end;

procedure WriteData( doAreas, doRoots, doTips: boolean );
var err_len,err_exist,err_load: integer;
    rlin,tlin: integer;
    x,t,c,i,j,k,l: integer;
    RootDat,TipDat: TextFile;
    s: String;
  function Format(var xs: String; s: String; separator: boolean=true): String;
  begin
    if Length(s)>Length(xs) then begin
      Inc(err_len);
      while Length(xs)<Length(s) do xs:=' '+xs;
    end;
    while Length(s)<Length(xs) do s:=' '+s;
    if separator then begin
      Result:=s+Pref.DatSep;
    end else begin
      Result:=s;
    end;
  end;
  function WriteOut( Take, Plot, Tube, Frame: integer ): boolean;
  var xs,s,FN: String;
      xFrame: TFrame;
      i: integer;
    function GetLeadingString(Plot, Tube, Frame, Take: integer): String;
    begin
      Result:=Format(Pref.Tags[0], Experiment.Sites[Plot].Name)+
         Format(Pref.Tags[1], Experiment.Sites[Plot].Tubes[Tube].Name)+
         Format(Pref.Tags[2], GetFrameName(Plot,Tube,Frame))+
         Format(Pref.Tags[3], Experiment.Takes[Take].Name);
    end;
  begin
    Result:=false;
    if not doRoots and not doTips then begin
      Exit;
    end;
    FN:=GetDataFileName(Take, Plot, Tube)+
      '\'+GetFrameName(Plot,Tube,Frame)+'.rvd';
    if FileExists(FN) then begin
      if xFrame.LoadFromFile(FN) then begin
        Result:=true;
        s:=GetLeadingString(Plot, Tube, Frame, Take);
        if doRoots then begin
          for i:=0 to xFrame.NRoots-1 do begin
            with xFrame.Roots[i] do begin
              xs:=Format(Pref.Tags[9], Name)+
                  Format(Pref.Tags[4], Category)+
                  Format(Pref.Tags[5], IntToStr(Attribute));
              if GetLen>0 then begin
                xs:=xs+Format(Pref.Tags[6], WrFloatStr(GetLen, 2));
              end else begin
                xs:=xs+Format(Pref.Tags[6], Pref.DatUndef);
              end;
              if AveWidth>0 then begin
                xs:=xs+Format(Pref.Tags[7], WrFloatStr(AveWidth, 2), doAreas);
              end else begin
                xs:=xs+Format(Pref.Tags[6], Pref.DatUndef, doAreas);
              end;
              if doAreas then begin
                xs:=xs+Format(Pref.Tags[8], WrFloatStr(xFrame.W*xFrame.H,2), false);
              end;
              writeln(RootDat, s+xs);
              Inc(rlin);
            end;
          end;
        end;
        if doTips then begin
          for i:=0 to xFrame.NTips-1 do begin
            with xFrame.Tips[i] do begin
              xs:=Format(Pref.Tags[10], Name)+
                  Format(Pref.Tags[4],  Category)+
                  Format(Pref.Tags[5], IntToStr(Attribute), doAreas);
              if doAreas then begin
                xs:=xs+Format(Pref.Tags[8], WrFloatStr(xFrame.W*xFrame.H,2), false);
              end;
              writeln(TipDat, s+xs);
              Inc(tlin);
            end;
          end;
        end;
      end else begin
        Inc(err_load);
      end;
    end else begin
      Inc(err_exist);
    end;
  end;
begin
  StartWorking('Generating Data...');
  err_len:=0;
  err_exist:=0;
  err_load:=0;
  rlin:=0;
  tlin:=0;
  if doRoots then begin
    if OpenWrite(RootDat, GetFilePath(Experiment.Name)+'\'+Pref.DatRoot) then begin
      s:='';
      s:=s+Format(Pref.Tags[0],Pref.Tags[0]);
      s:=s+Format(Pref.Tags[1],Pref.Tags[1]);
      s:=s+Format(Pref.Tags[2],Pref.Tags[2]);
      s:=s+Format(Pref.Tags[3],Pref.Tags[3]);
      s:=s+Format(Pref.Tags[9],Pref.Tags[9]);
      s:=s+Format(Pref.Tags[4],Pref.Tags[4]);
      s:=s+Format(Pref.Tags[5],Pref.Tags[5]);
      s:=s+Format(Pref.Tags[6],Pref.Tags[6]);
      s:=s+Format(Pref.Tags[7],Pref.Tags[7], doAreas);
      if doAreas then begin
        s:=s+Format(Pref.Tags[8],Pref.Tags[8], false);
      end;
      writeln(RootDat, s);
    end;
  end;
  if doTips then begin
    if OpenWrite(TipDat, GetFilePath(Experiment.Name)+'\'+Pref.DatTip) then begin
      s:='';
      s:=s+Format(Pref.Tags[0],Pref.Tags[0]);
      s:=s+Format(Pref.Tags[1],Pref.Tags[1]);
      s:=s+Format(Pref.Tags[2],Pref.Tags[2]);
      s:=s+Format(Pref.Tags[3],Pref.Tags[3]);
      s:=s+Format(Pref.Tags[10],Pref.Tags[10]);
      s:=s+Format(Pref.Tags[4],Pref.Tags[4]);
      s:=s+Format(Pref.Tags[5],Pref.Tags[5], doAreas);
      if doAreas then begin
        s:=s+Format(Pref.Tags[8],Pref.Tags[8], false);
      end;
      writeln(TipDat, s);
    end else begin
      if doRoots then begin
        Close(RootDat);
      end;
    end;
  end;
  t:=GetFrameCount;
  c:=0;
  x:=0;
  with Experiment do begin
    for i:=0 to NSites-1 do begin
      with Sites[i] do begin
        for j:=0 to NTubes-1 do begin
          with Tubes[j] do begin
            for k:=0 to Frames-1 do begin
              for l:=0 to NTakes-1 do begin
                if not isCancelled and ((c mod 100<>0) or not isCancel) and
                  isFrameOK(l,i,j,k) then begin
                  Inc(c);
                  if WriteOut(l, i,j,k) then begin
                    Inc(x);
                  end;
                  if (c mod 100=0) or (t<1000) then begin
                    IndicateProgress(c,t,x,rlin,tlin);
                  end;
                end;
              end;
            end;
          end;
        end;
      end;
    end;
  end;
  IndicateProgress(c,t,x,rlin,tlin);
  if doRoots then begin
    if isCancelled then begin
      rewrite(RootDat);
    end;
    Close(RootDat);
  end;
  if doTips then begin
    if isCancelled then begin
      rewrite(TipDat);
    end;
    Close(TipDat);
  end;
  StopWorking;
  if err_len>0 then begin
    WarningMsg('One or more tags had to be enlarged in order to accomodate the data.'+
      'You have to generate data once again to get the fixed column size in output files.');
  end;
end;

procedure TOutDataDlg.HelpBtnClick(Sender: TObject);
begin
  if Length(HlpFile)>0 then begin
    WinHelp(RootViewer.Handle, @HlpFile[1], Help_Key,
      Integer(PChar('Dialog: Output Data Generator')));
  end else begin
    Application.HelpContext(HelpContext);
  end;
end;

function TOutDataDlg.Execute: Boolean;
begin
  Result:=OutDataDlg.ShowModal=IDOK;
end;

procedure TOutDataDlg.SetupPage;
var i: integer;
    ok: boolean;
begin
  UpdateWindow(Handle);
  if PageControl1.ActivePage.PageIndex=0 then begin
    ActiveControl:=nil;
    if Edit1.Text<>IntToStr(GetFrameCount) then begin
      IndicateProgress(0,0,0,0,0);
      Edit1.Text:=IntToStr(GetFrameCount);
    end;
  end else
  if PageControl1.ActivePage.PageIndex=3 then begin
    ok:=ListBox1.Items.Count=10;
    for i:=0 to 10 do begin
      ok:=ok and (ListBox1.Items[i]=Pref.Tags[i]);
    end;
    if not ok then begin
      ListBox1.Clear;
      for i:=0 to 10 do begin
        ListBox1.Items.Add(Pref.Tags[i]);
      end;
      ListBox1.ItemIndex:=0;
      Edit6.Text:=Pref.Tags[0];
    end;
  end;
end;

function TOutDataDlg.UpdatePage: boolean;
  function UpdateOptions: boolean;
  begin
    Result:=true;
    Pref.FrameArea:=CheckBox9.Checked;
    Pref.DatUndef:=Edit12.Text;
    Pref.DatSep:=Edit5.Text;
    Pref.DatRoot:=Edit13.Text;
    Pref.DatTip:=Edit14.Text;
  end;
  function UpdateTags: boolean;
  var i: integer;
  begin
    ActiveControl:=ListBox1;
    Result:=true;
    for i:=0 to 10 do begin
      Pref.Tags[i]:=ListBox1.Items[i];
    end;
  end;
begin
  case PageControl1.ActivePage.PageIndex of
    0: Result:=true;
    1: Result:=true;
    2: Result:=UpdateOptions;
    3: Result:=UpdateTags;
    else Result:=true;
  end;
end;

procedure TOutDataDlg.Button1Click(Sender: TObject);
begin
  inherited;
  IndicateProgress(0,GetFrameCount, 0,0,0);
  Wait(100);
  WriteData(Pref.FrameArea,
        not SpeedButton2.Down,
        not SpeedButton1.Down);
  Wait(100);
  ProgressBar1.Position:=0;
end;

procedure TOutDataDlg.FormShow(Sender: TObject);
var i: integer;
    ok: boolean;
begin
  inherited;
  Data.CopyFrom(Pref);
  PageControl1.ActivePage:=TabSheet1;
  with Experiment do begin
    ok:=CheckListBox1.Items.Count=NSites;
    for i:=0 to NSites-1 do begin
      ok:=ok and (CheckListBox1.Items[i]=Sites[i].Name);
    end;
    if not ok then begin
      CheckListBox1.Clear;
      for i:=0 to NSites-1 do begin
        CheckListBox1.Items.Add(Sites[i].Name);
        CheckListBox1.Checked[i]:=true;
      end;
    end;
    ok:=CheckListBox2.Items.Count=NTakes;
    for i:=0 to NTakes-1 do begin
      ok:=ok and (CheckListBox2.Items[i]=Takes[i].Name);
    end;
    if not ok then begin
      CheckListBox2.Clear;
      for i:=0 to NTakes-1 do begin
        CheckListBox2.Items.Add(Takes[i].Name);
        CheckListBox2.Checked[i]:=true;
      end;
    end;
  end;
  IndicateProgress(0,0,0,0,0);
  Edit1.Text:=IntToStr(GetFrameCount);
  CheckBox9.Checked:=Pref.FrameArea;
  Edit12.Text:=Pref.DatUndef;
  Edit5.Text:=Pref.DatSep;
  Edit13.Text:=Pref.DatRoot;
  Edit14.Text:=Pref.DatTip;
  ListBox1.Clear;
  for i:=0 to 10 do begin
    ListBox1.Items.Add(Pref.Tags[i]);
  end;
  ListBox1.ItemIndex:=0;
  Edit6.Text:=Pref.Tags[0];
end;

procedure TOutDataDlg.ListBox1Click(Sender: TObject);
begin
  inherited;
  Edit6.Text:=ListBox1.Items[ListBox1.ItemIndex];
end;

procedure TOutDataDlg.ListBox1DblClick(Sender: TObject);
begin
  inherited;
  ActiveControl:=Edit6;
end;

procedure TOutDataDlg.Edit6Change(Sender: TObject);
begin
  inherited;
  if Edit6.Text<>ListBox1.Items[ListBox1.ItemIndex] then begin
    ListBox1.Items[ListBox1.ItemIndex]:=Edit6.Text;
  end;
end;

procedure TOutDataDlg.PageControl1Change(Sender: TObject);
begin
  inherited;
  SetupPage;
end;

procedure TOutDataDlg.PageControl1Changing(Sender: TObject;
  var AllowChange: Boolean);
begin
  inherited;
  AllowChange:=false;
  if UpdatePage then begin
    AllowChange:=true;
  end;
end;

procedure TOutDataDlg.CancelBtnClick(Sender: TObject);
begin
  inherited;
  Pref.CopyFrom(Data);
  ListBox1.Clear;
  CheckListBox1.Clear;
  CheckListBox2.Clear;
end;

procedure TOutDataDlg.OKBtnClick(Sender: TObject);
  function Diff(A,B: TPref): boolean;
  var i: integer;
  begin
    Result:=
      (A.FrameArea<>B.FrameArea) or
      (A.DatUndef<>B.DatUndef) or
      (A.DatSep<>B.DatSep) or
      (A.DatRoot<>B.DatRoot) or
      (A.DatTip<>B.DatTip);
    if Result then begin
      for i:=0 to 10 do begin
        Result:=Result or (A.Tags[i]<>B.Tags[i]);
      end;
    end;
  end;
begin
  inherited;
  if UpdatePage then begin
    if Diff(Pref, Data) then begin
      Experiment.WrPref;
    end;
  end;
end;

end.

