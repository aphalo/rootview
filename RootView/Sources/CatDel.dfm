�
 TCATDELDLG 0{  TPF0�
TCatDelDlg	CatDelDlgCaption Scan Data for Deleted CategoriesClientHeight� ClientWidth7OnShowFormShowPixelsPerInch`
TextHeight �TBevelBevel1Left,Top� Width	Height	Visible  �TButtonOKBtnLeft� TopDefaultTabOrder  �TButton	CancelBtnLeft� Top,TabOrder  TButtonHelpBtnLeft� TopPWidthKHeightCaption&HelpTabOrderOnClickHelpBtnClick  	TGroupBox	GroupBox1LeftTopWidth� Height� CaptionDeleted CategoriesTabOrder TListBoxListBox1LeftTopWidth� Heighte
ItemHeightTabOrder   TProgressBarProgressBar1LeftTop� Width� HeightMin MaxdSmooth	StepTabOrder   TButtonButton1Left� ToptWidthKHeightCaption
&Scan DataDefault	TabOrder OnClickButton1Click  TMemoMemo1LeftTop� Width%HeightMLines.Strings4Categories listed above have been deleted. This may <cause invalid entries in the existing data. You are advised 6to scan data for such entries and then decide whether :you still want to save the changes (OK) or else return to the Categories Dialog (Cancel). ReadOnly	
ScrollBars
ssVerticalTabOrder   