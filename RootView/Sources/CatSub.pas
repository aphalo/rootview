unit CatSub;

interface

uses Windows, SysUtils, Classes, Graphics, Forms, Controls, StdCtrls, 
  Buttons, ExtCtrls, OkCancl1, CheckLst, ComCtrls;

type
  TCatSubDlg = class(TOKBottomDlg)
    HelpBtn: TButton;
    GroupBox1: TGroupBox;
    ProgressBar1: TProgressBar;
    Memo1: TMemo;
    Button1: TButton;
    CheckListBox1: TCheckListBox;
    procedure HelpBtnClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure CheckListBox1ClickCheck(Sender: TObject);
  private
    { Private declarations }
    xnr,xnt: integer;
    xRA,xRB: array of String;
    xTA,xTB: array of String;
    xR,xT: array of integer;
    function GetFrameCount: integer;
    function ScanData: integer;
  public
    { Public declarations }
    undef: integer;
    function Execute(nr,nt: integer; const RA,RB, TA,TB: array of String): Boolean;
  end;

var
  CatSubDlg: TCatSubDlg;

implementation

uses Data, Roots;

{$R *.DFM}

procedure TCatSubDlg.HelpBtnClick(Sender: TObject);
begin
  if Length(HlpFile)>0 then begin
    WinHelp(RootViewer.Handle, @HlpFile[1], Help_Key,
      Integer(PChar('Dialog: Renamed Categories')));
  end else begin
    Application.HelpContext(HelpContext);
  end;
end;

function TCatSubDlg.Execute(nr,nt: integer; const RA,RB, TA,TB: array of String): Boolean;
var i: integer;
begin
  undef:=-1;
  CatSubDlg.xnr:=nr;
  CatSubDlg.xnt:=nt;
  SetLength(CatSubDlg.xRA,nr);
  SetLength(CatSubDlg.xRB,nr);
  for i:=0 to nr-1 do begin
    CatSubDlg.xRA[i]:=RA[i];
    CatSubDlg.xRB[i]:=RB[i];
  end;
  SetLength(CatSubDlg.xTA,nt);
  SetLength(CatSubDlg.xTB,nt);
  for i:=0 to nt-1 do begin
    CatSubDlg.xTA[i]:=TA[i];
    CatSubDlg.xTB[i]:=TB[i];
  end;
  Result:=CatSubDlg.ShowModal=IDOK;
end;

procedure TCatSubDlg.FormShow(Sender: TObject);
var i,j: integer;
begin
  inherited;
  SetLength(xR,xnr);
  SetLength(xT,xnt);
  j:=0;
  CheckListBox1.Clear;
  for i:=0 to xnr-1 do begin
    CheckListBox1.Items.Add(xRA[i]+' --> '+xRB[i]);
    CheckListBox1.Checked[j]:=true;
    xR[i]:=j;
    Inc(j);
  end;
  CheckListBox1.Items.Add('');
  CheckListBox1.Checked[j]:=false;
  CheckListBox1.State[j]:=cbGrayed;
  Inc(j);
  for i:=0 to xnt-1 do begin
    CheckListBox1.Items.Add(xTA[i]+' --> '+xTB[i]);
    CheckListBox1.Checked[j]:=true;
    xT[i]:=j;
    Inc(j);
  end;
end;

function TCatSubDlg.GetFrameCount: integer;
var c,i,j: integer;
begin
  c:=0;
  with Experiment do begin
    for i:=0 to NSites-1 do begin
      with Sites[i] do begin
        for j:=0 to NTubes-1 do begin
          with Tubes[j] do begin
            Inc(c, Frames);
          end;
        end;
      end;
    end;
    c:=c*NTakes;
  end;
  Result:=c;
end;

function TCatSubDlg.ScanData: integer;
var mf,sf,x,c,t,i,j,k,l: integer;
    rc,tc: array of integer;
    ok: boolean;
  procedure IndicateProgress( c,t: integer );
  begin
    if t<=0 then t:=1;
    ProgressBar1.Position:=Round(100*c/t);
  end;
  function CheckOut( Take, Plot, Tube, Frame: integer ): boolean;
  var FN: String;
      xFrame: TFrame;
      i: integer;
      isModified: boolean;
    procedure CheckRootCat( var Cat: String );
    var i: integer;
    begin
      for i:=0 to xnr-1 do begin
        if CheckListBox1.Checked[xR[i]] then begin
          if Cat=xra[i] then begin
            Cat:=xrb[i];
            Inc(rc[i]);
            Inc(undef);
            isModified:=true;
            Exit;
          end;
        end;
      end;
    end;
    procedure CheckTipCat( var Cat: String );
    var i: integer;
    begin
      for i:=0 to xnt-1 do begin
        if CheckListBox1.Checked[xT[i]] then begin
          if Cat=xta[i] then begin
            Cat:=xtb[i];
            Inc(tc[i]);
            Inc(undef);
            isModified:=true;
            Exit;
          end;
        end;
      end;
    end;
  begin
    Result:=false;
    if (xnr<=0) and (xnt<=0) then begin
      Exit;
    end;
    isModified:=false;
    FN:=GetDataFileName(Take, Plot, Tube)+
      '\'+GetFrameName(Plot,Tube,Frame)+'.rvd';
    if FileExists(FN) then begin
      if xFrame.LoadFromFile(FN) then begin
        Result:=true;
        if xnr>0 then begin
          for i:=0 to xFrame.NRoots-1 do begin
            with xFrame.Roots[i] do begin
              CheckRootCat(Category);
            end;
          end;
        end;
        if xnt>0 then begin
          for i:=0 to xFrame.NTips-1 do begin
            with xFrame.Tips[i] do begin
              CheckTipCat(Category);
            end;
          end;
        end;
        if isModified then begin
          Inc(mf);
          if xFrame.SaveToFile(FN) then begin
            Inc(sf);
          end;
        end;
      end;
    end;
  end;
begin
  StartWorking('Substituting...');
  undef:=0;
  mf:=0;
  sf:=0;
  t:=GetFrameCount;
  c:=0;
  x:=0;
  SetLength(rc, xnr);
  for i:=0 to xnr-1 do begin
    rc[i]:=0;
  end;
  SetLength(tc, xnt);
  for i:=0 to xnt-1 do begin
    tc[i]:=0;
  end;
  with Experiment do begin
    for l:=0 to NTakes-1 do begin
      for i:=0 to NSites-1 do begin
        with Sites[i] do begin
          for j:=0 to NTubes-1 do begin
            with Tubes[j] do begin
              for k:=0 to Frames-1 do begin
                if not isCancelled and ((c mod 100<>0) or not isCancel) then begin
                  Inc(c);
                  if CheckOut(l, i,j,k) then begin
                    Inc(x);
                  end;
                  if (c mod 100=0) or (t<1000) then begin
                    IndicateProgress(c,t);
                  end;
                end;
              end;
            end;
          end;
        end;
      end;
    end;
  end;
  IndicateProgress(c,t);
  StopWorking;
  Result:=x;
  j:=0;
  for i:=0 to xnr-1 do begin
    ok:=CheckListBox1.Checked[j];
    CheckListBox1.Items[j]:=xRA[i]+' --> '+xRB[i]+'   ('+IntToStr(rc[i])+')';
    CheckListBox1.Checked[j]:=ok;
    Inc(j);
  end;
  CheckListBox1.Items[j]:='';
  CheckListBox1.Checked[j]:=false;
  CheckListBox1.State[j]:=cbGrayed;
  Inc(j);
  for i:=0 to xnt-1 do begin
    ok:=CheckListBox1.Checked[j];
    CheckListBox1.Items[j]:=xTA[i]+' --> '+xTB[i]+'   ('+IntToStr(tc[i])+')';
    CheckListBox1.Checked[j]:=ok;
    Inc(j);
  end;
  Wait(100);
  IndicateProgress(0,1);
  if isCancelled then begin
    undef:=-1;
  end;
  if (mf<>sf) then begin
    WarningMsg('Only '+IntToStr(mf-sf)+' out of '+IntToStr(mf)+
      ' modified frames have been succesfully updated!');
  end;
end;

procedure TCatSubDlg.Button1Click(Sender: TObject);
begin
  inherited;
  ScanData;
end;

procedure TCatSubDlg.CheckListBox1ClickCheck(Sender: TObject);
var ok: boolean;
    i: integer;
begin
  inherited;
  ok:=false;
  for i:=0 to xnr-1 do begin
    ok:=ok or CheckListBox1.Checked[i];
  end;
  for i:=1 to xnt do begin
    ok:=ok or CheckListBox1.Checked[xnr+i];
  end;
  Button1.Enabled:=ok
end;

end.

