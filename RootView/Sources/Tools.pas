unit Tools;

interface

uses Windows, SysUtils, Classes, Graphics, Forms, Controls, StdCtrls,
  Buttons, ExtCtrls, OkCancl1, ComCtrls, Menus;

type
  TTools = record
    Name,Command,Caption,Folder: String;
    Shortcut: Word;
    Confirm: boolean;
  end;
  procedure InitTool( var T: TTools );

var
  NTools: integer=0;
  MTools: array of TTools;

type
  TMenuDlg = class(TOKBottomDlg)
    HelpBtn: TButton;
    ListBox1: TListBox;
    Edit1: TEdit;
    Button1: TButton;
    Button2: TButton;
    Button3: TButton;
    Button4: TButton;
    Label2: TLabel;
    Edit2: TEdit;
    GroupBox1: TGroupBox;
    Edit3: TEdit;
    GroupBox2: TGroupBox;
    Edit4: TEdit;
    GroupBox3: TGroupBox;
    HotKey1: THotKey;
    CheckBox1: TCheckBox;
    procedure HelpBtnClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure OKBtnClick(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure Button3Click(Sender: TObject);
    procedure Button4Click(Sender: TObject);
    procedure ListBox1Click(Sender: TObject);
    procedure ListBox1DblClick(Sender: TObject);
    procedure Edit1Change(Sender: TObject);
  private
    { Private declarations }
    n,ind: integer;
    Items: array of TTools;
    function UpdateItem: boolean;
    procedure SetupItem;
  public
    { Public declarations }
    function Execute: Boolean;
  end;

var
  MenuDlg: TMenuDlg;

function ReadMenu(var f: TextFile): boolean;
function WriteMenu(var f: TextFile): boolean;
procedure SetupToolsMenu(Menu: TMenuItem);

implementation

uses Data, Roots;

{$R *.DFM}

function ReadMenu(var f: TextFile): boolean;
var i,j,Err: integer;
begin
  Err:=IOResult;
  if Err=0 then begin
    readln(f, NTools);
    Err:=Err or IOResult;
    if (Err<>0) or (NTools>999) then NTools:=0;
    SetLength(MTools, NTools);
    for i:=0 to NTools-1 do begin
      if Err=0 then begin
        with MTools[i] do begin
          readln(f,Name);
          readln(f,Command);
          readln(f,Caption);
          readln(f,Folder);
          readln(f,Shortcut);
          readln(f,j);
          Confirm:=j<>0;
          Err:=Err or IOResult;
        end;
      end;
    end;
  end;
  Err:=Err or IOResult;
  Result:=Err=0;
end;

function WriteMenu(var f: TextFile): boolean;
var i,Err: integer;
begin
  writeln(f);
  Err:=IOResult;
  if Err=0 then begin
    writeln(f, NTools);
    for i:=0 to NTools-1 do begin
      if Err=0 then begin
        with MTools[i] do begin
          writeln(f,Name);
          writeln(f,Command);
          writeln(f,Caption);
          writeln(f,Folder);
          writeln(f,Shortcut);
          writeln(f,ord(Confirm));
          Err:=Err or IOResult;
        end;
      end;
    end;
  end;
  Err:=Err or IOResult;
  Result:=Err=0;
end;

procedure SetupToolsMenu(Menu: TMenuItem);
var Item: TMenuItem;
    i: integer;
begin
  for i:=Menu.Count-1 downto 1 do begin
    Menu.Delete(i);
  end;
  for i:=0 to NTools-1 do begin
    Item:=TMenuItem.Create(Menu);
    Item.Caption:=MTools[i].Name;
    Item.ShortCut:=MTools[i].Shortcut;
    Item.OnClick:=RootViewer.Execute;
    Item.Tag:=i;
    Menu.Add(Item);
  end;
end;

procedure InitTool( var T: TTools );
begin
  with T do begin
    Name:='';
    Command:='';
    Caption:='';
    Folder:='';
    Shortcut:=0;
    Confirm:=false;
  end;
end;

procedure TMenuDlg.HelpBtnClick(Sender: TObject);
begin
  if Length(HlpFile)>0 then begin
    WinHelp(RootViewer.Handle, @HlpFile[1], Help_Key,
      Integer(PChar('Dialog: Tools Menu Setup')));
  end else begin
    Application.HelpContext(HelpContext);
  end;
end;

function TMenuDlg.Execute: Boolean;
begin
 { construct the form }
 MenuDlg:=TMenuDlg.Create(Application);
 try
   { execute; set result based on how closed }
   Result:=MenuDlg.ShowModal=IDOK;
 finally
   { dispose of the form }
   MenuDlg.Free;
 end;
end;

procedure TMenuDlg.FormCreate(Sender: TObject);
var i: integer;
begin
  inherited;
  n:=NTools;
  SetLength(Items, n);
  for i:=0 to n-1 do begin
    Items[i]:=MTools[i];
    ListBox1.Items.Add(Items[i].Name);
  end;
  ListBox1.Items.Add('');
  ListBox1.ItemIndex:=0;
  Ind:=0;
  SetupItem;
end;

procedure TMenuDlg.OKBtnClick(Sender: TObject);
var i: integer;
begin
  inherited;
  UpdateItem;
  NTools:=n;
  SetLength(MTools, NTools);
  for i:=0 to NTools-1 do begin
    MTools[i]:=Items[i];
  end;
  SetLength(Items, 0);
end;

function TMenuDlg.UpdateItem: boolean;
begin
  Result:=true;
  if ind<n then begin
    with Items[ind] do begin
      Confirm:=CheckBox1.Checked;
      Name:=Edit1.Text;
      Command:=Edit2.Text;
      Caption:=Edit3.Text;
      Folder:=Edit4.Text;
      Shortcut:=HotKey1.Hotkey;
    end;
  end;
end;

procedure TMenuDlg.SetupItem;
begin
  if ind<n then begin
    CheckBox1.Enabled:=true;
    Edit1.Enabled:=true;
    Edit2.Enabled:=true;
    Edit3.Enabled:=true;
    Edit4.Enabled:=true;
    HotKey1.Enabled:=true;
    with Items[ind] do begin
      CheckBox1.Checked:=Confirm;
      Edit1.Text:=Name;
      Edit2.Text:=Command;
      Edit3.Text:=Caption;
      Edit4.Text:=Folder;
      HotKey1.Hotkey:=Shortcut;
    end;
    Button2.Enabled:=true;
    Button3.Enabled:=ind>0;
    Button4.Enabled:=ind<n-1;
  end else begin
    CheckBox1.Enabled:=false;
    Edit1.Enabled:=false;
    Edit2.Enabled:=false;
    Edit3.Enabled:=false;
    Edit4.Enabled:=false;
    HotKey1.Enabled:=false;
    CheckBox1.Checked:=false;
    Edit1.Text:='';
    Edit2.Text:='';
    Edit3.Text:='';
    Edit4.Text:='';
    HotKey1.Hotkey:=0;
    Button2.Enabled:=false;
    Button3.Enabled:=false;
    Button4.Enabled:=false;
  end;
end;

procedure TMenuDlg.Button1Click(Sender: TObject);
var i: integer;
begin
  inherited;
  UpdateItem;
  Inc(n);
  SetLength(Items, n);
  for i:=n-1 downto ind+1 do Items[i]:=Items[i-1];
  InitTool(Items[ind]);
  ListBox1.Items.Insert(Ind, Items[ind].Name);
  ListBox1.ItemIndex:=Ind;
  SetupItem;
  ActiveControl:=Edit1;
end;

procedure TMenuDlg.Button2Click(Sender: TObject);
var i: integer;
begin
  inherited;
  UpdateItem;
  for i:=ind to n-2 do Items[i]:=Items[i+1];
  Dec(n);
  SetLength(Items, n);
  ListBox1.Items.Delete(ind);
  if ind>ListBox1.Items.Count-1 then begin
    ind:=ListBox1.Items.Count-1;
  end;
  ListBox1.ItemIndex:=Ind;
  SetupItem;
end;

procedure TMenuDlg.Button3Click(Sender: TObject);
var c: TTools;
begin
  inherited;
  UpdateItem;
  c:=Items[Ind];
  Items[Ind]:=Items[Ind-1];
  Items[Ind-1]:=c;
  ListBox1.Items[Ind-1]:=Items[Ind-1].Name;
  ListBox1.Items[Ind]:=Items[Ind].Name;
  Dec(Ind);
  ListBox1.ItemIndex:=Ind;
  SetupItem;
end;

procedure TMenuDlg.Button4Click(Sender: TObject);
var c: TTools;
begin
  inherited;
  UpdateItem;
  c:=Items[Ind];
  Items[Ind]:=Items[Ind+1];
  Items[Ind+1]:=c;
  ListBox1.Items[Ind]:=Items[Ind].Name;
  ListBox1.Items[Ind+1]:=Items[Ind+1].Name;
  Inc(Ind);
  ListBox1.ItemIndex:=Ind;
  SetupItem;
end;

procedure TMenuDlg.ListBox1Click(Sender: TObject);
begin
  inherited;
  try
    UpdateItem;
  finally
    Ind:=ListBox1.ItemIndex;
    SetupItem;
  end;
end;

procedure TMenuDlg.ListBox1DblClick(Sender: TObject);
begin
  inherited;
  if Edit1.Enabled then begin
    ActiveControl:=Edit1;
  end else begin
    Button1.Click;
  end;
end;

procedure TMenuDlg.Edit1Change(Sender: TObject);
begin
  inherited;
  if (ListBox1.ItemIndex>=0) and
     (ListBox1.Items[ListBox1.ItemIndex]<>Edit1.Text) then begin
    ListBox1.Items[ListBox1.ItemIndex]:=Edit1.Text;
  end;
end;

end.

