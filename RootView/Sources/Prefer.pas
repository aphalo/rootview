unit Prefer;

interface

uses Windows, SysUtils, Classes, Graphics, Forms, Controls, StdCtrls,
  Buttons, ExtCtrls, ComCtrls, Dialogs, OkCancl1,
  ColorGrid1, Data;

{ ------------------------------------------------------- }
{ Preferences ------------------------------------------- }
{ ------------------------------------------------------- }

type

  TPref = object
    {Defaults}
    Units,RootPrefix,TipPrefix,FrameStr: String;
    WriteTransparent: boolean;
    LabelBack,LabelFont: integer;
    SelLabelBack,SelLabelFont: integer;
    SelItemColor,SelNodeColor: integer;
    WMarkColor,FMarkColor: integer;
    ClickError,ZoomFactor: float;
    DefStretch: boolean;
    UseColors,ShowGhosts: boolean;
    ShowLabels,ShowRoots,ShowTips,ShowWidths: boolean;
    MaxZoom,MinZoom: float;
    MaxScan: integer;
    FrameArea: boolean;
    DatUndef,DatSep: String;
    DatRoot,DatTip: String;
    Tags: array [0..10] of String;
    {Font}
    FontName: string;
    FontHeight: integer;
    FontSize: integer;
    FontPitch: integer;
    FontCharset: integer;
    FontBold: boolean;
    FontItalic: boolean;
    {Colors}
    CustomColors: TCustomColors;
    {Methods}
    procedure Init;
    procedure CopyFrom( var Src: TPref );
    procedure Delete;
    procedure GetDefaultColors;
    function ReadItems( var f: text ): boolean;
    function WriteItems( var f: text ): boolean;
    function ReadColors( var f: text ): boolean;
    function WriteColors( var f: text ): boolean;
    function ReadFont( var f: text ): boolean;
    function WriteFont( var f: text ): boolean;
  end;

var
  Pref: TPref;

{ ------------------------------------------------------- }
{ Preferences Dialog ------------------------------------ }
{ ------------------------------------------------------- }

type
  TPrefDlg = class(TOKBottomDlg)
    HelpBtn: TButton;
    PageControl: TPageControl;
    TabFont: TTabSheet;
    btChangeFont: TButton;
    TextFontDialog: TFontDialog;
    edFontSample: TEdit;
    ColorDialog1: TColorDialog;
    TabSheet1: TTabSheet;
    GroupBox2: TGroupBox;
    Label1: TLabel;
    Label2: TLabel;
    Edit1: TEdit;
    Edit2: TEdit;
    GroupBox3: TGroupBox;
    ColorGrid11: TColorGrid1;
    ListBox1: TListBox;
    CheckBox1: TCheckBox;
    Label3: TLabel;
    Edit3: TEdit;
    GroupBox1: TGroupBox;
    ColorGrid: TColorGrid1;
    Button1: TButton;
    Button2: TButton;
    TabSheet2: TTabSheet;
    GroupBox4: TGroupBox;
    ColorGrid12: TColorGrid1;
    ListBox2: TListBox;
    GroupBox5: TGroupBox;
    Edit4: TEdit;
    Label4: TLabel;
    Edit5: TEdit;
    Label7: TLabel;
    TabSheet3: TTabSheet;
    GroupBox8: TGroupBox;
    CheckBox3: TCheckBox;
    CheckBox4: TCheckBox;
    CheckBox5: TCheckBox;
    CheckBox6: TCheckBox;
    CheckBox7: TCheckBox;
    CheckBox8: TCheckBox;
    Label9: TLabel;
    Edit9: TEdit;
    Label10: TLabel;
    Edit10: TEdit;
    Label11: TLabel;
    Edit11: TEdit;
    GroupBox7: TGroupBox;
    RadioButton1: TRadioButton;
    RadioButton2: TRadioButton;
    Label5: TLabel;
    Edit6: TEdit;
    TabSheet4: TTabSheet;
    GroupBox6: TGroupBox;
    Edit7: TEdit;
    GroupBox9: TGroupBox;
    Edit8: TEdit;
    GroupBox10: TGroupBox;
    Edit12: TEdit;
    procedure HelpBtnClick(Sender: TObject);
    procedure btChangeFontClick(Sender: TObject);
    procedure PageControlChange(Sender: TObject);
    procedure PageControlChanging(Sender: TObject;
      var AllowChange: Boolean);
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormDestroy(Sender: TObject);
    procedure OKBtnClick(Sender: TObject);
    procedure CancelBtnClick(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure ListBox1Click(Sender: TObject);
    procedure ListBox2Click(Sender: TObject);
  private
    { Private declarations }
    DefAction: TCloseAction;
    Data: TPref;
    xCustomColors: TCustomColors;
    SelList: integer;
    procedure SetupPage;
    function UpdatePage: boolean;
    procedure SetupSampleFont(Size: boolean);
  public
    { Public declarations }
    function Execute: Boolean;
  end;

var
  PrefDlg: TPrefDlg;

{ ------------------------------------------------------- }
{ Do IT! ------------------------------------------------ }
{ ------------------------------------------------------- }

implementation

uses Tools, Roots;

{$R *.DFM}

{ ------------------------------------------------------- }
{ Preferences ------------------------------------------- }
{ ------------------------------------------------------- }

procedure TPref.Init;
begin
  {Font}
  FontName:='MS Sans Serif';
  FontHeight:=10;
  FontSize:=10;
  FontPitch:=0;
  FontCharset:=0;
  FontBold:=true;
  FontItalic:=false;
  {Colors}
  GetDefaultColors;
  {Options}
  Units:='mm';
  RootPrefix:='R';
  TipPrefix:='T';
  FrameStr:='01';
  LabelBack:=15;
  LabelFont:=00;
  SelLabelBack:=00;
  SelLabelFont:=10;
  WriteTransparent:=true;
  SelItemColor:=09;
  SelNodeColor:=12;
  WMarkColor:=12;
  FMarkColor:=12;
  ClickError:=0.25;
  ZoomFactor:=sqrt(2);
  DefStretch:=false;
  ShowLabels:=true;
  ShowRoots:=true;
  ShowTips:=true;
  ShowWidths:=true;
  ShowGhosts:=false;
  UseColors:=true;
  MaxZoom:=1.99;
  MinZoom:=0.51;
  MaxScan:=1000;
  FrameArea:=true;
  DatUndef:='*';
  DatSep:=' ';
  DatRoot:='Roots.dat';
  DatTip:='Tips.dat';
  Tags[00]:='   plot';
  Tags[01]:='   tube';
  Tags[02]:='frame';
  Tags[03]:='   date';
  Tags[04]:='   category';
  Tags[05]:='a';
  Tags[06]:='length';
  Tags[07]:=' width';
  Tags[08]:='  area';
  Tags[09]:='  root';
  Tags[10]:='   tip';
end;

procedure TPref.CopyFrom( var Src: TPref );
begin
  Units:=Src.Units;
  RootPrefix:=Src.RootPrefix;
  TipPrefix:=Src.TipPrefix;
  FrameStr:=Src.FrameStr;
  WriteTransparent:=Src.WriteTransparent;
  LabelBack:=Src.LabelBack;
  LabelFont:=Src.LabelFont;
  SelLabelBack:=Src.SelLabelBack;
  SelLabelFont:=Src.SelLabelFont;
  SelItemColor:=Src.SelItemColor;
  SelNodeColor:=Src.SelNodeColor;
  WMarkColor:=Src.WMarkColor;
  FMarkColor:=Src.FMarkColor;
  ClickError:=Src.ClickError;
  ZoomFactor:=Src.ZoomFactor;
  DefStretch:=Src.DefStretch;
  UseColors:=Src.UseColors;
  ShowGhosts:=Src.ShowGhosts;
  ShowLabels:=Src.ShowLabels;
  ShowRoots:=Src.ShowRoots;
  ShowTips:=Src.ShowTips;
  ShowWidths:=Src.ShowWidths;
  MaxZoom:=Src.MaxZoom;
  MinZoom:=Src.MinZoom;
  MaxScan:=Src.MaxScan;
  FrameArea:=Src.FrameArea;
  DatUndef:=Src.DatUndef;
  DatSep:=Src.DatSep;
  DatRoot:=Src.DatRoot;
  DatTip:=Src.DatTip;
  Tags:=Src.Tags;
  {Font}
  FontName:=Src.FontName;
  FontHeight:=Src.FontHeight;
  FontSize:=Src.FontSize;
  FontPitch:=Src.FontPitch;
  FontCharset:=Src.FontCharset;
  FontBold:=Src.FontBold;
  FontItalic:=Src.FontItalic;
  {Colors}
  CustomColors:=Src.CustomColors;
end;

procedure TPref.Delete;
begin
  {It's OK}
end;

procedure TPref.GetDefaultColors;
var FPaletteEntries: array[0..NumPaletteEntries - 1] of TPaletteEntry;
    i,j: integer;
begin
  GetPaletteEntries(GetStockObject(DEFAULT_PALETTE), 0, NumPaletteEntries,
    FPaletteEntries);
  j:=0;
  for i:=0 to max_CustomColors do begin
    if i=8 then j:=4;
    with FPaletteEntries[i+j] do begin
      CustomColors[i]:=RGB(peRed, peGreen, peBlue);
    end;
  end;
end;

function TPref.ReadItems( var f: text ): boolean;
var i,Err: integer;
  function GetBool: boolean;
  var x: integer;
  begin
    x:=0;
    if Err=0 then begin
      readln(f,x);
      Err:=IOResult;
    end;
    Result:=x<>0;
  end;
begin
  Err:=IOResult;
  if (Err=0) and not eof(f) then begin
    if not ReadFont(f) then Err:=1;
    if not ReadColors(f) then Err:=1;
  end;
  Err:=Err or IOResult;
  if Err=0 then begin
    WriteTransparent:=GetBool;
    readln(f, Units);
    readln(f, RootPrefix);
    readln(f, TipPrefix);
    if DataPatch>0 then begin
      readln(f, FrameStr);
    end;
    readln(f, LabelBack);
    readln(f, LabelFont);
    readln(f, SelLabelBack);
    readln(f, SelLabelFont);
    readln(f, SelItemColor);
    readln(f, SelNodeColor);
    readln(f, WMarkColor);
    readln(f, FMarkColor);
    readln(f, ClickError);
    DefStretch:=GetBool;
    ShowLabels:=GetBool;
    ShowRoots:=GetBool;
    ShowTips:=GetBool;
    ShowWidths:=GetBool;
    ShowGhosts:=GetBool;
    UseColors:=GetBool;
    readln(f, MaxZoom);
    readln(f, MinZoom);
    readln(f, MaxScan);
  end;
  Err:=Err or IOResult;
  if Err=0 then begin
    FrameArea:=GetBool;
    readln(f, DatRoot);
    readln(f, DatTip);
    readln(f, DatUndef);
    readln(f, DatSep);
  end;
  Err:=Err or IOResult;
  if Err=0 then begin
    for i:=0 to 10 do begin
      readln(f, Tags[i]);
    end;
  end;
  Err:=Err or IOResult;
  if not ReadMenu(f) then Err:=1;
  Result:=Err=0;
end;

function TPref.WriteItems( var f: text ): boolean;
var i,Err: integer;
begin
  Err:=IOResult;
  if Err=0 then begin
    if not WriteFont(f) then Err:=1;
  end;
  Err:=Err or IOResult;
  if Err=0 then begin
    if not WriteColors(f) then Err:=1;
  end;
  Err:=Err or IOResult;
  if Err=0 then begin
    writeln(f, ord(WriteTransparent));
    writeln(f, Units);
    writeln(f, RootPrefix);
    writeln(f, TipPrefix);
    writeln(f, FrameStr);
    writeln(f, LabelBack);
    writeln(f, LabelFont);
    writeln(f, SelLabelBack);
    writeln(f, SelLabelFont);
    writeln(f, SelItemColor);
    writeln(f, SelNodeColor);
    writeln(f, WMarkColor);
    writeln(f, FMarkColor);
    writeln(f, ClickError:FXA:FXB);
    writeln(f, ord(DefStretch));
    writeln(f, ord(ShowLabels));
    writeln(f, ord(ShowRoots));
    writeln(f, ord(ShowTips));
    writeln(f, ord(ShowWidths));
    writeln(f, ord(ShowGhosts));
    writeln(f, ord(UseColors));
    writeln(f, MaxZoom:FXA:FXB);
    writeln(f, MinZoom:FXA:FXB);
    writeln(f, MaxScan);
  end;
  Err:=Err or IOResult;
  if Err=0 then begin
    writeln(f, ord(FrameArea));
    writeln(f, DatRoot);
    writeln(f, DatTip);
    writeln(f, DatUndef);
    writeln(f, DatSep);
  end;
  Err:=Err or IOResult;
  if Err=0 then begin
    for i:=0 to 10 do begin
      writeln(f, Tags[i]);
    end;
  end;
  if not WriteMenu(f) then Err:=1;
  Err:=Err or IOResult;
  Result:=Err=0;
end;

function TPref.ReadFont( var f: text ): boolean;
var Err: integer;
  function GetBool: boolean;
  var x: integer;
  begin
    x:=0;
    if Err=0 then begin
      readln(f,x);
      Err:=IOResult;
    end;
    Result:=x<>0;
  end;
begin
  Err:=IOResult;
  if Err=0 then begin
    readln(f, FontName);
    readln(f, FontHeight);
    readln(f, FontSize);
    readln(f, FontPitch);
    readln(f, FontCharset);
    FontBold:=GetBool;
    FontItalic:=GetBool;
  end;
  Err:=Err or IOResult;
  Result:=Err=0;
end;

function TPref.WriteFont( var f: text ): boolean;
var Err: integer;
begin
  Err:=IOResult;
  writeln(f, FontName);
  writeln(f, FontHeight);
  writeln(f, FontSize);
  writeln(f, FontPitch);
  writeln(f, FontCharset);
  writeln(f, ord(FontBold));
  writeln(f, ord(FontItalic));
  Err:=Err or IOResult;
  Result:=Err=0;
end;

function TPref.ReadColors( var f: text ): boolean;
var i,Err: integer;
begin
  Err:=IOResult;
  for i:=0 to max_CustomColors do begin
    readln(f, CustomColors[i]);
  end;
  Err:=Err or IOResult;
  Result:=Err=0;
end;

function TPref.WriteColors( var f: text ): boolean;
var i,Err: integer;
begin
  Err:=IOResult;
  if Err=0 then begin
    for i:=0 to max_CustomColors do begin
      writeln(f, CustomColors[i]);
    end;
  end;
  Err:=Err or IOResult;
  Result:=Err=0;
end;

{ ------------------------------------------------------- }
{ Preferences Dialog ------------------------------------ }
{ ------------------------------------------------------- }

procedure TPrefDlg.HelpBtnClick(Sender: TObject);
begin
  if Length(HlpFile)>0 then begin
    WinHelp(RootViewer.Handle, @HlpFile[1], Help_Key,
      Integer(PChar('Dialog: Preferences')));
  end else begin
    Application.HelpContext(HelpContext);
  end;
end;

function TPrefDlg.Execute: Boolean;
begin
 { construct the form }
 PrefDlg:=TPrefDlg.Create(Application);
 try
   PrefDlg.DefAction:=caHide;
   { execute; set result based on how closed }
   Result:=PrefDlg.ShowModal=IDOK;
 finally
   { dispose of the form }
   PrefDlg.Free;
 end;
end;

procedure TPrefDlg.SetupSampleFont(Size: boolean);
begin
  Data.FontName:=TextFontDialog.Font.Name;
  Data.FontHeight:=TextFontDialog.Font.Height;
  Data.FontSize:=TextFontDialog.Font.Size;
  Data.FontPitch:=ord(TextFontDialog.Font.Pitch);
  Data.FontCharset:=TextFontDialog.Font.Charset;
  with TextFontDialog.Font do begin
    Data.FontBold:=fsBold in Style;
    Data.FontItalic:=fsItalic in Style;
  end;
  edFontSample.Font:=TextFontDialog.Font;
  edFontSample.Text:=TextFontDialog.Font.Name;
end;

procedure TPrefDlg.SetupPage;
  procedure SetupFont;
  begin
    TextFontDialog.Font.Name:=Data.FontName;
    TextFontDialog.Font.Height:=Data.FontHeight;
    TextFontDialog.Font.Size:=Data.FontSize;
    TextFontDialog.Font.Pitch:=TFontPitch(Data.FontPitch);
    TextFontDialog.Font.Charset:=Data.FontCharset;
    TextFontDialog.Font.Style:=[];
    with TextFontDialog.Font do begin
      if Data.FontBold then Style:=Style+[fsBold];
      if Data.FontItalic then Style:=Style+[fsItalic];
    end;
    SetupSampleFont(false);
  end;
  procedure SetupLabels;
  begin
    Edit1.Text:=Data.RootPrefix;
    Edit2.Text:=Data.TipPrefix;
    Edit3.Text:=Data.Units;
    Edit6.Text:=Data.FrameStr;
    CheckBox1.Checked:=Data.WriteTransparent;
    if ListBox1.ItemIndex=0 then begin
      ColorGrid11.BackgroundIndex:=Data.SelLabelBack;
      ColorGrid11.ForegroundIndex:=Data.SelLabelFont;
    end else begin
      ColorGrid11.BackgroundIndex:=Data.LabelBack;
      ColorGrid11.ForegroundIndex:=Data.LabelFont;
    end;
    SelList:=ListBox1.ItemIndex;
  end;
  procedure SetupEditor;
  begin
    Edit4.Text:=WrFloatStr(Data.ClickError,6);
    Edit5.Text:=WrFloatStr(Data.ZoomFactor,6);
    Edit9.Text:=WrFloatStr(Data.MaxZoom,2);
    Edit10.Text:=WrFloatStr(Data.MinZoom,2);
    Edit11.Text:=IntToStr(Data.MaxScan);
    if ListBox2.ItemIndex=0 then begin
      ColorGrid12.ForegroundIndex:=Data.SelItemColor;
    end else
    if ListBox2.ItemIndex=1 then begin
      ColorGrid12.ForegroundIndex:=Data.SelNodeColor;
    end else
    if ListBox2.ItemIndex=2 then begin
      ColorGrid12.ForegroundIndex:=Data.WMarkColor;
    end else
    if ListBox2.ItemIndex=3 then begin
      ColorGrid12.ForegroundIndex:=Data.FMarkColor;
    end else begin
      ColorGrid12.ForegroundIndex:=-1;
    end;
    SelList:=ListBox2.ItemIndex;
  end;
  procedure SetupDefaults;
  begin
    RadioButton1.Checked:=not Data.DefStretch;
    RadioButton2.Checked:=Data.DefStretch;
    CheckBox3.Checked:=Data.ShowLabels;
    CheckBox4.Checked:=Data.ShowWidths;
    CheckBox5.Checked:=Data.ShowRoots;
    CheckBox6.Checked:=Data.ShowTips;
    CheckBox7.Checked:=Data.ShowGhosts;
    CheckBox8.Checked:=Data.useColors;
  end;
begin
  case PageControl.ActivePage.PageIndex of
    0: SetupFont;
    1: SetupLabels;
    2: SetupEditor;
    3: SetupDefaults;
    else {Oh, well}
  end;
end;

function TPrefDlg.UpdatePage: boolean;
  function UpdateFont: boolean;
  begin
    Result:=true;
    {OK}
  end;
  function UpdateLabels: boolean;
  begin
    Result:=true;
    Data.RootPrefix:=Edit1.Text;
    Data.TipPrefix:=Edit2.Text;
    Data.FrameStr:=Edit6.Text;
    Data.Units:=Edit3.Text;
    Data.WriteTransparent:=CheckBox1.Checked;
    if SelList=0 then begin
      Data.SelLabelBack:=ColorGrid11.BackgroundIndex;
      Data.SelLabelFont:=ColorGrid11.ForegroundIndex;
    end else begin
      Data.LabelBack:=ColorGrid11.BackgroundIndex;
      Data.LabelFont:=ColorGrid11.ForegroundIndex;
    end;
  end;
  function UpdateEditor: boolean;
  begin
    Result:=true;
    if Edit4.Modified then begin
      Data.ClickError:=StrToFloat(Edit4.Text);
    end;
    if Edit5.Modified then begin
      Data.ZoomFactor:=StrToFloat(Edit5.Text);
    end;
    if Edit9.Modified then begin
      Data.MaxZoom:=StrToFloat(Edit9.Text);
    end;
    if Edit10.Modified then begin
      Data.MinZoom:=StrToFloat(Edit10.Text);
    end;
    if Edit11.Modified then begin
      Data.MaxScan:=StrToInt(Edit11.Text);
    end;
    if SelList=0 then begin
      Data.SelItemColor:=ColorGrid12.ForegroundIndex;
    end else
    if SelList=1 then begin
      Data.SelNodeColor:=ColorGrid12.ForegroundIndex;
    end else
    if SelList=2 then begin
      Data.WMarkColor:=ColorGrid12.ForegroundIndex;
    end else
    if SelList=3 then begin
      Data.FMarkColor:=ColorGrid12.ForegroundIndex;
    end else begin
      ColorGrid12.ForegroundIndex:=-1;
    end;
  end;
  function UpdateDefaults: boolean;
  begin
    Result:=true;
    Data.DefStretch:=RadioButton2.Checked;
    Data.ShowLabels:=CheckBox3.Checked;
    Data.ShowWidths:=CheckBox4.Checked;
    Data.ShowRoots:=CheckBox5.Checked;
    Data.ShowTips:=CheckBox6.Checked;
    Data.ShowGhosts:=CheckBox7.Checked;
    Data.useColors:=CheckBox8.Checked;
  end;
begin
  case PageControl.ActivePage.PageIndex of
    0: Result:=UpdateFont;
    1: Result:=UpdateLabels;
    2: Result:=UpdateEditor;
    3: Result:=UpdateDefaults;
    else Result:=true;
  end;
end;

procedure TPrefDlg.btChangeFontClick(Sender: TObject);
begin
  inherited;
  if TextFontDialog.Execute then begin
    SetupSampleFont(true);
  end;
end;

procedure TPrefDlg.PageControlChange(Sender: TObject);
begin
  inherited;
  SetupPage;
end;

procedure TPrefDlg.PageControlChanging(Sender: TObject;
  var AllowChange: Boolean);
begin
  inherited;
  AllowChange:=false;
  if UpdatePage then begin
    AllowChange:=true;
  end;
end;

procedure TPrefDlg.FormCreate(Sender: TObject);
var dir: String;
begin
  inherited;
  xCustomColors:=Pref.CustomColors;
  ColorGrid.SetCustomColors(Pref.CustomColors);
  ColorGrid11.SetCustomColors(Pref.CustomColors);
  ColorGrid12.SetCustomColors(Pref.CustomColors);
  Data.CopyFrom(Pref);
  ListBox1.ItemIndex:=0;
  ListBox2.ItemIndex:=0;
  SetupPage;
  dir:=ParamStr(0);
  dir:=GetLongFileName(dir);
  dir:=GetFilePath(dir);
  Edit7.Text:=Experiment.CfgFile;
  Edit8.Text:=Experiment.CatFile;
  Edit12.Text:=dir;
end;

procedure TPrefDlg.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  inherited;
  Action:=DefAction;
end;

procedure TPrefDlg.FormDestroy(Sender: TObject);
begin
  Data.Delete;
  inherited;
end;

procedure TPrefDlg.OKBtnClick(Sender: TObject);
begin
  DefAction:=caNone;
  if UpdatePage then begin
    xCustomColors:=Pref.CustomColors;
    Pref.Delete;
    Pref.CopyFrom(Data);
    Pref.CustomColors:=xCustomColors;
    DefAction:=caHide;
    inherited;
  end else begin
    DefAction:=caNone;
  end;
end;

procedure TPrefDlg.CancelBtnClick(Sender: TObject);
begin
  inherited;
  Pref.CustomColors:=xCustomColors;
  DefAction:=caHide;
end;

procedure TPrefDlg.Button2Click(Sender: TObject);
begin
  inherited;
  Pref.GetDefaultColors;
  ColorGrid.SetCustomColors(Pref.CustomColors);
  ColorGrid11.SetCustomColors(Pref.CustomColors);
  ColorGrid12.SetCustomColors(Pref.CustomColors);
end;

procedure TPrefDlg.Button1Click(Sender: TObject);
var i: integer;
    s: String;
begin
  inherited;
  ColorDialog1.CustomColors.Clear;
  for i:=0 to max_CustomColors do begin
    s:='Color'+chr(i+ord('A'))+'='+IntToHex(Pref.CustomColors[i],6);
    ColorDialog1.CustomColors.Add(s);
  end;
  if ColorDialog1.Execute then begin
    for i:=0 to max_CustomColors do begin
      s:=ColorDialog1.CustomColors[i];
      if Length(s)>7 then begin
        s[7]:='$';
        Pref.CustomColors[i]:=StrToInt(PChar(@s[7]));
      end;
    end;
    ColorGrid.SetCustomColors(Pref.CustomColors);
    ColorGrid11.SetCustomColors(Pref.CustomColors);
    ColorGrid12.SetCustomColors(Pref.CustomColors);
  end;
end;

procedure TPrefDlg.ListBox1Click(Sender: TObject);
begin
  inherited;
  try
    UpdatePage;
  finally
    SetupPage;
  end;
end;

procedure TPrefDlg.ListBox2Click(Sender: TObject);
begin
  inherited;
  try
    UpdatePage;
  finally
    SetupPage;
  end;
end;

end.

