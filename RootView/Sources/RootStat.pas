unit RootStat;

interface

uses Windows, SysUtils, Classes, Graphics, Forms, Controls, StdCtrls, 
  Buttons, ExtCtrls, OkCancl1, Data;

type
  TRootStatDlg = class(TOKBottomDlg)
    HelpBtn: TButton;
    Edit1: TEdit;
    ComboBox1: TComboBox;
    Edit2: TEdit;
    Edit3: TEdit;
    Edit4: TEdit;
    Edit5: TEdit;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    RadioButton1: TRadioButton;
    RadioButton2: TRadioButton;
    RadioButton3: TRadioButton;
    procedure HelpBtnClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    function Execute(var Name,Cat,Comm: String; var Attr: integer;
      Width: Float; NNodes: integer; Nodes: array of TFPoint): Boolean;
  end;

var
  RootStatDlg: TRootStatDlg;

implementation

uses ObjDlg, Roots;

{$R *.DFM}

procedure TRootStatDlg.HelpBtnClick(Sender: TObject);
begin
  if Length(HlpFile)>0 then begin
    WinHelp(RootViewer.Handle, @HlpFile[1], Help_Key,
      Integer(PChar('Dialog: Root Properties')));
  end else begin
    Application.HelpContext(HelpContext);
  end;
end;

function TRootStatDlg.Execute(var Name,Cat,Comm: String; var Attr: integer;
  Width: Float; NNodes: integer; Nodes: array of TFPoint): Boolean;
var i: integer;
    l: float;
begin
  Edit1.Text:=Name;
  ComboBox1.Text:=Cat;
  Edit2.Text:=Comm;
  Edit3.Text:=IntToStr(NNodes);
  Edit4.Text:=WrFloatStr(Width, 4);
  RadioButton1.Checked:=Attr=0;
  RadioButton2.Checked:=Attr=1;
  RadioButton3.Checked:=Attr=2;
  l:=0;
  for i:=1 to NNodes-1 do begin
    l:=l+Sqrt(Sqr(Nodes[i-1].x-Nodes[i].x)+
              Sqr(Nodes[i-1].y-Nodes[i].y));
  end;
  Edit5.Text:=WrFloatStr(l, 4);
  Result:=RootStatDlg.ShowModal=IDOK;
  if Result then begin
    Name:=Edit1.Text;
    Cat:=ComboBox1.Text;
    Comm:=Edit2.Text;
    if RadioButton1.Checked then Attr:=0;
    if RadioButton2.Checked then Attr:=1;
    if RadioButton3.Checked then Attr:=2;
  end;
end;

end.

