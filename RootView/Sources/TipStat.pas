unit TipStat;

interface

uses Windows, SysUtils, Classes, Graphics, Forms, Controls, StdCtrls, 
  Buttons, ExtCtrls, OkCancl1;

type
  TTipStatDlg = class(TOKBottomDlg)
    HelpBtn: TButton;
    Edit1: TEdit;
    ComboBox1: TComboBox;
    Edit2: TEdit;
    RadioButton1: TRadioButton;
    RadioButton2: TRadioButton;
    RadioButton3: TRadioButton;
    procedure HelpBtnClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    function Execute(var Name,Cat,Comm: String; var Attr: integer): boolean;
  end;

var
  TipStatDlg: TTipStatDlg;

implementation

uses Roots,Data;

{$R *.DFM}

procedure TTipStatDlg.HelpBtnClick(Sender: TObject);
begin
  if Length(HlpFile)>0 then begin
    WinHelp(RootViewer.Handle, @HlpFile[1], Help_Key,
      Integer(PChar('Dialog: Tip Properties')));
  end else begin
    Application.HelpContext(HelpContext);
  end;
end;

function TTipStatDlg.Execute(var Name,Cat,Comm: String; var Attr: integer): boolean;
begin
  Edit1.Text:=Name;
  ComboBox1.Text:=Cat;
  Edit2.Text:=Comm;
  RadioButton1.Checked:=Attr=0;
  RadioButton2.Checked:=Attr=1;
  RadioButton3.Checked:=Attr=2;
  Result:=TipStatDlg.ShowModal=IDOK;
  if Result then begin
    Name:=Edit1.Text;
    Cat:=ComboBox1.Text;
    Comm:=Edit2.Text;
    if RadioButton1.Checked then Attr:=0;
    if RadioButton2.Checked then Attr:=1;
    if RadioButton3.Checked then Attr:=2;
  end;
end;

end.

