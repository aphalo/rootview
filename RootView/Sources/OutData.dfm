�
 TOUTDATADLG 0w  TPF0�TOutDataDlg
OutDataDlgCaptionOutput Data GeneratorClientHeight� ClientWidthOnShowFormShowPixelsPerInch`
TextHeight �TBevelBevel1LeftTop� Width	Height	Visible  �TButtonOKBtnLeft Top� OnClick
OKBtnClick  �TButton	CancelBtnLefttTop� OnClickCancelBtnClick  TButtonHelpBtnLeft� Top� WidthKHeightCaption&HelpTabOrderOnClickHelpBtnClick  TPageControlPageControl1LeftTopWidthHeight� 
ActivePage	TabSheet1TabOrderOnChangePageControl1Change
OnChangingPageControl1Changing 	TTabSheet	TabSheet1CaptionData TSpeedButtonSpeedButton1LefttToplWidthAHeight
AllowAllUp	
GroupIndexCaption&Roots Only  TSpeedButtonSpeedButton2Left� ToplWidth=Height
AllowAllUp	
GroupIndexCaption
&Tips Only  	TGroupBox	GroupBox1LeftTopWidth� HeightaCaptionProgressTabOrder  TLabelLabel1LeftTopWidth%HeightCaptionImages:  TLabelLabel2LeftTop0Width5HeightCaption
Processed:  TLabelLabel3Left� TopWidthHeightCaptionRoots:  TLabelLabel4Left� Top0WidthHeightCaptionTips:  TProgressBarProgressBar1LeftTopIWidth� HeightMin MaxdSmooth	StepTabOrder   TEditEdit1LeftHTopWidth5HeightEnabledTabOrder  TEditEdit2LeftHTop,Width5HeightEnabledTabOrder  TEditEdit3Left� TopWidth=HeightEnabledTabOrder  TEditEdit4Left� Top,Width=HeightEnabledTabOrder   TButtonButton1LeftToplWidthSHeightCaption&Generate DataTabOrderOnClickButton1Click   	TTabSheet	TabSheet2Caption	Selection
ImageIndex 	TGroupBox	GroupBox2LeftTopWidthuHeight}CaptionPlotsTabOrder  TCheckListBoxCheckListBox1LeftTopWidtheHeighta
ItemHeightTabOrder    	TGroupBox	GroupBox3Left� TopWidthuHeight}CaptionMeasurementsTabOrder TCheckListBoxCheckListBox2LeftTopWidtheHeighta
ItemHeightTabOrder     	TTabSheet	TabSheet3CaptionOptions
ImageIndex 	TGroupBox
GroupBox10LeftTopWidth� Height}CaptionOutputTabOrder  TLabelLabel12LeftTop,WidthIHeightCaptionMissing Values:  TLabelLabel13Left� Top,Width6HeightCaptionSeparators:  TLabelLabel14LeftTopHWidthBHeightCaptionRoots Output:  TLabelLabel5LeftTopdWidth:HeightCaptionTips Output:  	TCheckBox	CheckBox9Left|TopWidthqHeightCaptionInclude Frame &AreaTabOrder   TEditEdit12LeftXTop(Width%HeightTabOrder  TEditEdit13LeftXTopDWidth� HeightTabOrder  TEditEdit14LeftXTop`Width� HeightTabOrder  TEditEdit5Left� Top(Width%HeightTabOrder    	TTabSheetTagsCaptionTags
ImageIndex TEditEdit6LeftTopWidth� HeightFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameCourier New
Font.Style 
ParentFontTabOrderOnChangeEdit6Change  TListBoxListBox1LeftTop$Width� Height]Font.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameCourier New
Font.Style 
ItemHeight
ParentFontTabOrder OnClickListBox1Click
OnDblClickListBox1DblClick     