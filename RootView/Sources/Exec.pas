unit Exec;

interface

uses Windows, SysUtils, Classes, Graphics, Forms, Controls, StdCtrls, 
  Buttons, ExtCtrls, OkCancl1;

type
  TExecDlg = class(TOKBottomDlg)
    HelpBtn: TButton;
    Edit1: TEdit;
    Edit2: TEdit;
    Edit3: TEdit;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    procedure HelpBtnClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    function Execute(var C,D,T: String): Boolean;
  end;

var
  ExecDlg: TExecDlg;

implementation

uses Roots,Data;

{$R *.DFM}

procedure TExecDlg.HelpBtnClick(Sender: TObject);
begin
  if Length(HlpFile)>0 then begin
    WinHelp(RootViewer.Handle, @HlpFile[1], Help_Key,
      Integer(PChar('Dialog: Execute Command Line')));
  end else begin
    Application.HelpContext(HelpContext);
  end;
end;

function TExecDlg.Execute(var C,D,T: String): Boolean;
begin
 { construct the form }
 ExecDlg:=TExecDlg.Create(Application);
 try
   { execute; set result based on how closed }
   ExecDlg.Edit1.Text:=C;
   ExecDlg.Edit2.Text:=D;
   ExecDlg.Edit3.Text:=T;
   Result:=ExecDlg.ShowModal=IDOK;
   if Result then begin
     C:=ExecDlg.Edit1.Text;
     D:=ExecDlg.Edit2.Text;
     C:=ExecDlg.Edit3.Text;
   end;
 finally
   { dispose of the form }
   ExecDlg.Free;
 end;
end;

end.

