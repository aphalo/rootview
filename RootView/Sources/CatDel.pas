unit CatDel;

interface

uses Windows, SysUtils, Classes, Graphics, Forms, Controls, StdCtrls, 
  Buttons, ExtCtrls, OkCancl1, ComCtrls;

type
  TCatDelDlg = class(TOKBottomDlg)
    HelpBtn: TButton;
    GroupBox1: TGroupBox;
    Button1: TButton;
    ListBox1: TListBox;
    ProgressBar1: TProgressBar;
    Memo1: TMemo;
    procedure HelpBtnClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure Button1Click(Sender: TObject);
  private
    { Private declarations }
    xnr,xnt: integer;
    xR,xT: array of String;
    function GetFrameCount: integer;
    function ScanData: integer;
  public
    { Public declarations }
    undef: integer;
    function Execute(nr,nt: integer; const R,T: array of String): Boolean;
  end;

var
  CatDelDlg: TCatDelDlg;

implementation

{$R *.DFM}

uses Data, Roots;

procedure TCatDelDlg.HelpBtnClick(Sender: TObject);
begin
  if Length(HlpFile)>0 then begin
    WinHelp(RootViewer.Handle, @HlpFile[1], Help_Key,
      Integer(PChar('Dialog: Deleted Categories')));
  end else begin
    Application.HelpContext(HelpContext);
  end;
end;

function TCatDelDlg.Execute(nr,nt: integer; const R,T: array of String): Boolean;
var i: integer;
begin
  undef:=-1;
  CatDelDlg.xnr:=nr;
  CatDelDlg.xnt:=nt;
  SetLength(CatDelDlg.xR,nr);
  for i:=0 to nr-1 do begin
    CatDelDlg.xR[i]:=R[i];
  end;
  SetLength(CatDelDlg.xT,nt);
  for i:=0 to nt-1 do begin
    CatDelDlg.xT[i]:=T[i];
  end;
  Result:=CatDelDlg.ShowModal=IDOK;
end;

procedure TCatDelDlg.FormShow(Sender: TObject);
var i: integer;
begin
  inherited;
  ListBox1.Clear;
  for i:=0 to xnr-1 do begin
    ListBox1.Items.Add(xR[i]);
  end;
  ListBox1.Items.Add('');
  for i:=0 to xnt-1 do begin
    ListBox1.Items.Add(xT[i]);
  end;
end;

function TCatDelDlg.GetFrameCount: integer;
var c,i,j: integer;
begin
  c:=0;
  with Experiment do begin
    for i:=0 to NSites-1 do begin
      with Sites[i] do begin
        for j:=0 to NTubes-1 do begin
          with Tubes[j] do begin
            Inc(c, Frames);
          end;
        end;
      end;
    end;
    c:=c*NTakes;
  end;
  Result:=c;
end;

function TCatDelDlg.ScanData: integer;
var x,c,t,i,j,k,l: integer;
    rc,tc: array of integer;
  procedure IndicateProgress( c,t: integer );
  begin
    if t<=0 then t:=1;
    ProgressBar1.Position:=Round(100*c/t);
  end;
  function CheckOut( Take, Plot, Tube, Frame: integer ): boolean;
  var FN: String;
      xFrame: TFrame;
      i: integer;
    procedure CheckRootCat( Cat: String );
    var i: integer;
    begin
      for i:=0 to xnr-1 do begin
        if Cat=xr[i] then begin
          Inc(rc[i]);
          Inc(undef);
        end;
      end;
    end;
    procedure CheckTipCat( Cat: String );
    var i: integer;
    begin
      for i:=0 to xnt-1 do begin
        if Cat=xt[i] then begin
          Inc(tc[i]);
          Inc(undef);
        end;
      end;
    end;
  begin
    Result:=false;
    if (xnr<=0) and (xnt<=0) then begin
      Exit;
    end;
    FN:=GetDataFileName(Take, Plot, Tube)+
      '\'+GetFrameName(Plot,Tube,Frame)+'.rvd';
    if FileExists(FN) then begin
      if xFrame.LoadFromFile(FN) then begin
        Result:=true;
        if xnr>0 then begin
          for i:=0 to xFrame.NRoots-1 do begin
            with xFrame.Roots[i] do begin
              CheckRootCat(Category);
            end;
          end;
        end;
        if xnt>0 then begin
          for i:=0 to xFrame.NTips-1 do begin
            with xFrame.Tips[i] do begin
              CheckTipCat(Category);
            end;
          end;
        end;
      end;
    end;
  end;
begin
  StartWorking('Scanning Data...');
  undef:=0;
  t:=GetFrameCount;
  c:=0;
  x:=0;
  SetLength(rc, xnr);
  for i:=0 to xnr-1 do begin
    rc[i]:=0;
  end;
  SetLength(tc, xnt);
  for i:=0 to xnt-1 do begin
    tc[i]:=0;
  end;
  with Experiment do begin
    for l:=0 to NTakes-1 do begin
      for i:=0 to NSites-1 do begin
        with Sites[i] do begin
          for j:=0 to NTubes-1 do begin
            with Tubes[j] do begin
              for k:=0 to Frames-1 do begin
                if not isCancelled and ((c mod 100<>0) or not isCancel) then begin
                  Inc(c);
                  if CheckOut(l, i,j,k) then begin
                    Inc(x);
                  end;
                  if (c mod 100=0) or (t<1000) then begin
                    IndicateProgress(c,t);
                  end;
                end;
              end;
            end;
          end;
        end;
      end;
    end;
  end;
  IndicateProgress(c,t);
  if isCancelled then undef:=-1;
  StopWorking;
  Result:=x;
  ListBox1.Clear;
  for i:=0 to xnr-1 do begin
    ListBox1.Items.Add(xR[i]+'   ('+IntToStr(rc[i])+')');
  end;
  ListBox1.Items.Add('');
  for i:=0 to xnt-1 do begin
    ListBox1.Items.Add(xT[i]+'   ('+IntToStr(tc[i])+')');
  end;
  Wait(100);
  IndicateProgress(0,1);
end;

procedure TCatDelDlg.Button1Click(Sender: TObject);
begin
  inherited;
  ScanData;
end;

end.

