unit Data;

interface

uses Windows, SysUtils, Classes, Graphics, Forms, Controls, StdCtrls,
  Buttons, ExtCtrls, Dialogs, OkCancl1, ColorGrid1, ComCtrls, CheckLst;

{ ------------------------------------------------------- }
{ Data -------------------------------------------------- }
{ ------------------------------------------------------- }

const
  FXA   = 12; {Real Write Format :FXA:FXB}
  FXB   =  4;
  Patch =  1;
  eps   = 0.0001;

type
  Float = Double;
  TFPoint= record
    X,Y: Float;
  end;

  TCategory = record
    Name: String;
    Comment: String;
    Ind,Key: Word;
    Color: integer;
    Width: integer;
  end;

  TRoot = object
    Name: String;
    Comment: String;
    Category: String;
    Tag: TFPoint;
    isTagFixed: boolean;
    Attribute: integer;
    NNodes: integer;
    Nodes: array of TFPoint;
    WM1,WM2: TFPoint;
    AveWidth: float;
    procedure Init;
    procedure CopyFrom( var Src: TRoot );
    function ReadItems( var f: TextFile ): boolean;
    function WriteItems( var f: TextFile ): boolean;
    function SelectNode(X,Y: float; BmpWidth,BmpHeight: integer; Marks: boolean): integer;
    function InsertNode(X,Y: float; BmpWidth,BmpHeight: integer): integer;
    procedure DelNode( Ind: integer );
    procedure FixLabel;
    function GetLen: float;
    function FixWidthMark: boolean;
  end;
  TTip = object
    Name: String;
    Comment: String;
    Category: String;
    Tag: TFPoint;
    Attribute: integer;
    isTagFixed: boolean;
    Point: TFPoint;
    procedure Init;
    procedure CopyFrom( var Src: TTip );
    function ReadItems( var f: TextFile ): boolean;
    function WriteItems( var f: TextFile ): boolean;
  end;

  TFrame = object
    Comment: String;
    W,H,DX,DY: float;
    Mark: TFPoint;
    NRoots,NTips: integer;
    SelRoot,SelTip: integer;
    SelNode: integer;
    Roots: array of TRoot;
    Tips: array of TTip;
    LDX,LDY: integer;
    fLDX,fLDY: float;
    isLabelDrag: boolean;
    procedure Init;
    procedure CopyFrom( var Src: TFrame );
    function LoadFromFile( FN: String ): boolean;
    function SaveToFile( FN: String ): boolean;
    function ReadItems( var f: TextFile ): boolean;
    function WriteItems( var f: TextFile ): boolean;
    procedure DrawItems( Bitmap: TBitmap );
    procedure NewTip;
    procedure DelTip( Ind: integer );
    procedure NewRoot;
    procedure DelRoot( Ind: integer );
    procedure NewNode(Bitmap: TBitmap; X,Y: integer);
    procedure SelObject(Bitmap: TBitmap; X,Y: integer);
    function SelWidthMark(Bitmap: TBitmap; X,Y: integer): boolean;
    function GetRootLabelClick(Bitmap: TBitmap; X,Y: integer): boolean;
    function GetTipLabelClick(Bitmap: TBitmap; X,Y: integer): boolean;
    function isMarkVisible: boolean;
    function areLabelsVisible: boolean;
    function areRootsVisible: boolean;
    function areTipsVisible: boolean;
    function areWidthsVisible: boolean;
    function areGoneVisible: boolean;
    function GetFontHeight: integer;
    function GetFontSize: integer;
    function GetNextRootName: String;
    function GetNextTipName: String;
  end;

  TTube = record
    Name: String;
    Offset,Frames: integer;
    Width,Height: Float;
  end;
  TSite = object
    Name: String;
    NTubes: integer;
    Tubes: array of TTube;
    procedure Init;
    function ReadItems( var f: TextFile ): boolean;
  end;

  TTake = object
    Name: String;
    procedure Init;
  end;

{ ------------------------------------------------------- }
{ Experiment -------------------------------------------- }
{ ------------------------------------------------------- }

  TExperiment = object
    Name: String;
    CatFile,CfgFile: String;
    {Categories}
    NRootCat,NTipCat: integer;
    RootCat,TipCat: array of TCategory;
    {Takes}
    NTakes: integer;
    Takes: array of TTake;
    {Sites}
    NSites: integer;
    Sites: array of TSite;
    {Current Frame Identifier}
    TakeIndex,PlotIndex,TubeIndex,FrameIndex: integer;
    {Methods}
    procedure Init;
    function CreateFolders: integer;
    function GetData: boolean;
    function GetPref: boolean;
    function WrPref: boolean;
    function GetCats: boolean;
    function WrCats: boolean;
    function GetCatPenColor(Cat: String): integer;
    function GetCatPenWidth(Cat: String): integer;
    function GetTipCatPenColor(Cat: String): integer;
    function GetTipCatPenWidth(Cat: String): integer;
  end;

var
  Experiment: TExperiment;

  HlpFile: String = 'RootView.hlp';
  DataPatch: integer = 0;

  isWorking: boolean = false;
  isCancelled: boolean = false;

{ ------------------------------------------------------- }
{ Functions --------------------------------------------- }
{ ------------------------------------------------------- }

function GetCurrFrameName: String;
function GetCurrTakeName: String;
function GetCurrPlotName: String;
function GetCurrTubeName: String;
function GetFrameName(siteInd, tubeInd, ind: integer): String;
function GetDataFileName( TakeIndex, PlotIndex, TubeIndex: integer ): String;
function GetFileType(FN: string): string;
function GetFileName(FN: string): string;
function GetFilePath(FN: string): string;
function GetFileDrive(FN: string): string;
function GetShortFileName(FN: string): string;
function GetLongFileName(FN: string): string;
function FolderExists(Dir: string): boolean;
function OpenRead( var f: TextFile; fn: String ): boolean;
function OpenWrite( var f: TextFile; fn: String ): boolean;
function GetPatch( var f: TextFile; fn: String ): boolean;
function PutPatch( var f: TextFile; fn: String ): boolean;

function RealXToScr( x,RW: Float; CW: integer ): integer;
function RealYToScr( y,RH: Float; CH: integer ): integer;
function ScrXToReal( x,CW: integer; RW: Float): float;
function ScrYToReal( y,CH: integer; RH: Float): float;
function DoIntersect(var A1,A2, B1,B2: TFPoint ): boolean;

function WindExec(Comm,Dir: PChar): boolean;
function SetExistAppl( appl: PChar ): HWnd;
function GetExistAppl( appl: PChar): HWnd;

function WrFloatStr( v: float; d: integer): String;

procedure InfoMsg(Msg: String);
procedure WarningMsg(Msg: String);
procedure ErrorMsg(Msg: String);

procedure Wait(msec: integer);
procedure SetInfoText(s: String);
procedure StartWorking(s: String);
procedure StopWorking;
function isCancelKeyDown: boolean;
function isCancel: boolean;
function isLButtonDown: boolean;

{ ------------------------------------------------------- }
{ *** Data Dialog --------------------------------------- }
{ ------------------------------------------------------- }

type
  TDataDlg = class(TOKBottomDlg)
    HelpBtn: TButton;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    ListBox1: TListBox;
    Edit1: TEdit;
    Edit2: TEdit;
    Edit3: TEdit;
    ColorGrid11: TColorGrid1;
    Label1: TLabel;
    Label2: TLabel;
    Button1: TButton;
    Button2: TButton;
    Button3: TButton;
    Button4: TButton;
    Label3: TLabel;
    Label4: TLabel;
    Button5: TButton;
    Button6: TButton;
    Button7: TButton;
    Button8: TButton;
    Edit4: TEdit;
    Label5: TLabel;
    Edit5: TEdit;
    ListBox2: TListBox;
    ColorGrid12: TColorGrid1;
    Edit6: TEdit;
    Label6: TLabel;
    Label7: TLabel;
    Label8: TLabel;
    HotKey1: THotKey;
    HotKey2: THotKey;
    TabSheet3: TTabSheet;
    GroupBox1: TGroupBox;
    ProgressBar1: TProgressBar;
    CheckListBox1: TCheckListBox;
    Button9: TButton;
    Button10: TButton;
    ComboBox1: TComboBox;
    procedure HelpBtnClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure ListBox1Click(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure Button3Click(Sender: TObject);
    procedure Button4Click(Sender: TObject);
    procedure Edit1Change(Sender: TObject);
    procedure CancelBtnClick(Sender: TObject);
    procedure OKBtnClick(Sender: TObject);
    procedure PageControl1Change(Sender: TObject);
    procedure PageControl1Changing(Sender: TObject;
      var AllowChange: Boolean);
    procedure Button5Click(Sender: TObject);
    procedure Button6Click(Sender: TObject);
    procedure Button7Click(Sender: TObject);
    procedure Button8Click(Sender: TObject);
    procedure Edit4Change(Sender: TObject);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure ListBox1DblClick(Sender: TObject);
    procedure ListBox2Click(Sender: TObject);
    procedure ListBox2DblClick(Sender: TObject);
    procedure Button9Click(Sender: TObject);
    procedure Button10Click(Sender: TObject);
    procedure CheckListBox1Click(Sender: TObject);
    procedure CheckListBox1DblClick(Sender: TObject);
    procedure ComboBox1Change(Sender: TObject);
  private
    { Private declarations }
    xnr,xnt: integer;
    xRA,xRB: array of String;
    xTA,xTB: array of String;
    CCItem,RItem,TItem: integer;
    _NRootCat,_NTipCat: integer;
    _RootCat,_TipCat: array of TCategory;
    dlgCancel: boolean;
    procedure SetupItems;
    procedure EnableUpdate;
    function  UpdateItems(index: integer): boolean;
    function GetFrameCount: integer;
    function ScanData: integer;
    function UpdateData: integer;
  public
    { Public declarations }
    function Execute: Boolean;
  end;

var
  DataDlg: TDataDlg;

{ ------------------------------------------------------- }
{ *** Let's Do It --------------------------------------- }
{ ------------------------------------------------------- }

implementation

{$R *.DFM}

uses Roots, Prefer, CatDel, CatSub;

procedure InfoMsg(Msg: String);
begin
  MessageDlg(Msg, mtInformation, [mbOK], 0);
end;

procedure WarningMsg(Msg: String);
begin
  MessageDlg(Msg, mtWarning, [mbOK], 0);
end;

procedure ErrorMsg(Msg: String);
begin
  MessageDlg(Msg, mtError, [mbOK], 0);
end;

function GetCurrFrameName: String;
begin
  Result:='';
  with Experiment do begin
    if (TakeIndex>=0) and (TakeIndex<NTakes) and
      (FrameIndex>=0) then begin
      Result:=GetFrameName(PlotIndex,TubeIndex,FrameIndex);
    end;
  end;
end;

function GetCurrTakeName: String;
begin
  Result:='';
  with Experiment do begin
    if (TakeIndex>=0) and (TakeIndex<NTakes) then begin
      Result:=Takes[TakeIndex].Name;
    end;
  end;
end;

function GetCurrPlotName: String;
begin
  Result:='';
  with Experiment do begin
    if (TakeIndex>=0) and (TakeIndex<NTakes) then begin
      if (PlotIndex>=0) and (PlotIndex<NSites) then begin
        with Experiment.Sites[PlotIndex] do begin
          Result:=Name;
        end;
      end;
    end;
  end;
end;

function GetCurrTubeName: String;
begin
  Result:='';
  with Experiment do begin
    if (TakeIndex>=0) and (TakeIndex<NTakes) then begin
      if (PlotIndex>=0) and (PlotIndex<NSites) then begin
        with Sites[PlotIndex] do begin
          if (TubeIndex>=0) and (TubeIndex<NTubes) then begin
            with Tubes[TubeIndex] do begin
              Result:=Name;
            end;
          end;
        end;
      end;
    end;
  end;
end;

function GetFrameName(siteInd, tubeInd, ind: integer): String;
var i,l: integer;
begin
  Result:='';
  if (siteInd>=0) and (siteInd<Experiment.NSites) then begin
    with Experiment.Sites[siteInd].Tubes[tubeInd] do begin
      Result:=IntToStr(ind+Offset);
      l:=Length(Pref.FrameStr)-Length(Result);
      for i:=1 to l do Result:='0'+Result;
    end;
  end;
end;

function GetDataFileName( TakeIndex, PlotIndex, TubeIndex: integer ): String;
var FN: string;
begin
  FN:=GetFilePath(Experiment.Name);
  if (TakeIndex>=0) and (TakeIndex<Experiment.NTakes) then begin
    with Experiment.Takes[TakeIndex] do begin
      FN:=FN+'\'+Name;
      if (PlotIndex>=0) and (PlotIndex<Experiment.NSites) then begin
        with Experiment.Sites[PlotIndex] do begin
          FN:=FN+'\'+Name;
          if (TubeIndex>=0) and (TubeIndex<NTubes) then begin
            with Experiment.Sites[PlotIndex].Tubes[TubeIndex] do begin
              FN:=FN+'\'+Name;
            end;
          end;
        end;
      end;
    end;
  end;
  Result:=FN;
end;

function GetFileType(FN: string): string;
var i,j,len: integer;
    s: string;
begin
  s:='';
  len:=Length(FN);
  if len>0 then begin
    i:=len;
    while (i>0) and (FN[i]<>'.') and (FN[i]<>'\') do Dec(i);
    if (i>0) and (FN[i]='.') then begin
      for j:=i to len do s:=s+FN[j];
    end;
  end;
  Result:=s;
end;

function GetFileName(FN: string): string;
var i,j,len: integer;
    s: string;
begin
  s:='';
  len:=Length(FN);
  if len>0 then begin
    j:=len;
    while (j>0) and (FN[j]<>'.') and (FN[j]<>'\') do Dec(j);
    if (j=0) or (FN[j]<>'.') then j:=len+1;
    i:=len;
    while (i>0) and (FN[i]<>'\') do Dec(i);
    Inc(i);
    while i<j do begin
      s:=s+FN[i];
      Inc(i);
    end;
  end;
  Result:=s;
end;

function GetFilePath(FN: string): string;
var i,j,len: integer;
    s: string;
begin
  s:='';
  len:=Length(FN);
  if len>0 then begin
    i:=len;
    while (i>0) and (FN[i]<>'\') do Dec(i);
    for j:=1 to i-1 do s:=s+FN[j];
  end;
  Result:=s;
end;

function GetFileDrive(FN: string): string;
var len: integer;
    s: string;
begin
  s:='';
  len:=Length(FN);
  if (len>1) and (FN[2]=':') then s:=FN[1];
  Result:=s;
end;

function GetShortFileName(FN: string): string;
var xfn: PChar;
begin
  if Length(FN)>0 then begin
    GetMem(xfn, 512);
    GetShortPathName(@FN[1], xfn, 512);
    Result:=xfn;
    FreeMem(xfn, 512);
  end else begin
    Result:='';
  end;
end;

function GetLongFileName(FN: string): string;
var xfn,tmp: PChar;
    i,l: integer;
    xdt: Int64;
    xs,s: String;
    FindHandle: integer;
    FindData: TSearchRec;
  function isUNC(FN: string): boolean;
  var i: integer;
  begin
    Result:=false;
    for i:=1 to Length(FN)-1 do begin
      Result:=Result or
        ((FN[i]='\') and
         (FN[i+1]='\'));
    end;
  end;
begin
  xdt:=GetTickCount;
  if isUNC(FN) then begin
    Result:=FN;
    Exit;
  end;
  xs:='';
  if Length(FN)>0 then begin
    GetMem(xfn, 512);
    GetFullPathName(@FN[1], 512, xfn, tmp);
    if GetTickCount-xdt>333 then begin
      Result:=FN;
      FreeMem(xfn, 512);
      Exit;
    end;
    if not FileExists(xfn) then begin
      Result:=FN;
      FreeMem(xfn, 512);
      Exit;
    end;
    if GetTickCount-xdt>333 then begin
      Result:=FN;
      FreeMem(xfn, 512);
      Exit;
    end;
    l:=StrLen(xfn);
    i:=0;
    s:='';
    while (i<l) and (xfn[i]<>'\') do begin
      s:=s+xfn[i];
      Inc(i);
    end;
    Inc(i);
    if (Length(s)>0) then begin
      s:=ANSIUpperCase(s);
    end else begin
      Result:=FN;
      FreeMem(xfn, 512);
      Exit;
    end;
    xs:=s;
    repeat
      s:='';
      while (i<l) and (xfn[i]<>'\') do begin
        s:=s+xfn[i];
        Inc(i);
      end;
      if Length(s)>0 then begin
        s:=xs+'\'+s;
        if i<l then begin
          FindHandle:=FindFirst(s, faDirectory, FindData);
        end else begin
          FindHandle:=FindFirst(s, faAnyFile, FindData);
        end;
        xs:=xs+'\'+FindData.Name;
        FindClose(FindData);
        if (GetTickCount-xdt>333) or
           (FindHandle<>0) then begin
          Result:=FN;
          FreeMem(xfn, 512);
          Exit;
        end;
      end else
      if i<l then begin
        xs:=xs+'\';
      end;
      Inc(i);
    until i>=l;
    FreeMem(xfn, 512);
    if FileExists(xs) then begin
      Result:=xs;
    end else begin
      Result:=FN;
    end;
  end;
end;

function FolderExists(Dir: string): boolean;
var FindHandle: integer;
    FindData: TSearchRec;
begin
  FindHandle:=FindFirst(Dir, faDirectory, FindData);
  Result:=FindHandle=0;
  FindClose(FindData);
end;

function BackupFile(FN: string): boolean;
var NewFN: string;
begin
  Result:=false;
  if FileExists(FN) then begin
    NewFN:=FN+'.bak';
    if FileExists(NewFN) then begin
      DeleteFile(NewFN);
    end;
    Result:=RenameFile(FN, NewFN);
  end;
end;

function OpenRead( var f: TextFile; fn: String ): boolean;
begin
  Assign(f, fn);
  Reset(f);
  Result:=IOResult=0;
  if not Result then begin
    Close(f);
    ErrorMsg('Cannot Open file "'+fn+'" for reading.');
  end;
end;

function OpenWrite( var f: TextFile; fn: String ): boolean;
begin
  BackupFile(fn);
  Assign(f, fn);
  Rewrite(f);
  Result:=IOResult=0;
  if not Result then begin
    Close(f);
    ErrorMsg('Cannot Open file "'+fn+'" for writing.');
  end;
end;

function GetPatch( var f: TextFile; fn: String ): boolean;
begin
  readln(f, DataPatch);
  Result:=IOResult=0;
  if not Result then begin
    Close(f);
    ErrorMsg('Cannot read from "'+fn+'".');
  end;
end;

function PutPatch( var f: TextFile; fn: String ): boolean;
begin
  writeln(f, Patch, ' Patch Number');
  Result:=IOResult=0;
  if not Result then begin
    Close(f);
    ErrorMsg('Cannot write to "'+fn+'".');
  end;
end;

{ ------------------------------------------------------- }
{ *** Execute ------------------------------------------- }
{ ------------------------------------------------------- }

function GetExistAppl( appl: PChar): HWnd;
var HW: HWnd;
    cnt: integer;
    ok: boolean;
    s: PChar;
begin
  HW:=GetTopWindow(0);
  ok:=true;
  cnt:=0;
  GetMem(s, 1000);
  while ok and (cnt<1111) do begin
    HW:=GetNextWindow(HW, gw_HWndNext);
    ok:=(HW<>0);
    Inc(cnt);
    if ok then begin
      GetWindowText(HW, s, 512);
      if (Length(s)>0) and isWindow(HW) then begin
        ok:=StrLIComp(appl,s,StrLen(appl))<>0;
      end;
    end;
  end;
  FreeMem(s, 1000);
  if cnt>=1111 then HW:=0;
  if not isWindow(HW) then HW:=0;
  GetExistAppl:=HW;
end;

function SetExistAppl( appl: PChar ): HWnd;
var HW,xHW: HWnd;
begin
  HW:=GetExistAppl(appl);
  if (HW<>0) and isWindow(HW) then begin
    xHW:=GetTopWindow(HW);
    if (xHW<>HW) and
       (GetWindowTextLength(xHW)>0) and
       isWindow(xHW) and
       isWindowVisible(xHW) and
       isWindowEnabled(xHW) then begin
      SetForegroundWindow(xHW);
    end else begin
      SetForegroundWindow(HW);
    end;
  end else begin
    HW:=0;
  end;
  Result:=HW;
end;

function WindExec(Comm,Dir: PChar): boolean;
var StartInfo: TStartupInfo;
    ProcInfo: TProcessInformation;
    CFl: integer;
    Exec,Param: PChar;
    ok: boolean;
begin
  FillChar(StartInfo, SizeOf(StartInfo), #0);
  FillChar(ProcInfo, SizeOf(ProcInfo), #0);
  CFl:=CREATE_DEFAULT_ERROR_MODE or
       NORMAL_PRIORITY_CLASS;
  with StartInfo do begin
    cb:=SizeOf(StartInfo);
    dwFlags:=STARTF_USESHOWWINDOW;
    wShowWindow:=sw_ShowNormal;
  end;
  Exec:=nil;
  Param:=Comm;
  ok:=CreateProcess(
    Exec,  // pointer to name of executable module
    Param, // pointer to command line string
    nil,   // pointer to process security attributes
    nil,   // pointer to thread security attributes
    false, // handle inheritance flag
    CFl,   // creation flags
    nil,   // pointer to new environment block
    Dir,   // pointer to current directory name
    StartInfo, // pointer to TSTARTUPINFO
    ProcInfo   // pointer to TPROCESSINFORMATION
  );
  Result:=ok;
end;

{ ------------------------------------------------------- }
{ *** Working ------------------------------------------- }
{ ------------------------------------------------------- }

procedure Wait(msec: integer);
var dt: Int64;
begin
  dt:=GetTickCount;
  while not isCancelKeyDown and (GetTickCount-dt<msec) do begin
    {OK}
  end;
end;

function isCancel: boolean;
var dt: Int64;
begin
  Result:=isCancelKeyDown;
  if Result then begin
    SetInfoText('...Cancelled!');
    MessageBeep(MB_ICONEXCLAMATION);
    isCancelled:=true;
    dt:=GetTickCount;
    while isCancelKeyDown or (GetTickCount-dt<2222) do begin
      {OK}
    end;
  end;
end;

function isCancelKeyDown: boolean;
begin
  Result:=false;
  if GetAsyncKeyState(vk_Escape) and $8000<>0 then begin
    Result:=GetAsyncKeyState(vk_Shift) and $8000<>0;
  end;
end;

function isLButtonDown: boolean;
begin
  if GetSystemMetrics(SM_SWAPBUTTON)=0 then begin
    Result:=(GetAsyncKeyState(vk_LButton) and $8000<>0) and
            (GetAsyncKeyState(vk_RButton) and $8000=0) and
            (GetAsyncKeyState(vk_MButton) and $8000=0);
  end else begin
    Result:=(GetAsyncKeyState(vk_RButton) and $8000<>0) and
            (GetAsyncKeyState(vk_LButton) and $8000=0) and
            (GetAsyncKeyState(vk_MButton) and $8000=0);
  end;
end;

procedure SetInfoText(s: String);
begin
  RootViewer.StatusBar1.Panels
    [RootViewer.StatusBar1.Panels.Count-1].Text:=s;
  UpdateWindow(RootViewer.StatusBar1.Handle);
end;

procedure StartWorking(s: String);
begin
  isWorking:=true;
  SetInfoText(s);
  isCancelled:=false;
  if Screen.Cursor<>crHourGlass then begin
    Screen.Cursor:=crHourGlass;
  end;
  UpdateWindow(RootViewer.Handle);
  UpdateWindow(RootViewer.StatusBar1.Handle);
  UpdateWindow(RootViewer.ToolBar1.Handle);
end;

procedure StopWorking;
var Pos: TPoint;
begin
  isWorking:=false;
  if Screen.Cursor<>crDefault then begin
    GetCursorPos(Pos);
    ShowCursor(false);
    Screen.Cursor:=crDefault;
    SetCursorPos(Pos.x,Pos.Y);
    ShowCursor(true);
  end;
  SetInfoText('');
  RootViewer.UpdateStatus;
end;

{ ------------------------------------------------------- }
{ *** Coordinates --------------------------------------- }
{ ------------------------------------------------------- }

function DoIntersect(var A1,A2, B1,B2: TFPoint ): boolean;
var a,b,c,d,x,y,s,u: float;
begin
  Result:=false;
  a:=A1.x-A2.x;
  b:=B2.x-B1.x;
  c:=A1.y-A2.y;
  d:=B2.y-B1.y;
  s:=a*d-b*c;
  if s<>0 then begin
    x:=A1.x-B1.x;
    y:=A1.y-B1.y;
    u:=(d*x-b*y)/s;
    if (u>0) and (u<1) then begin
      u:=(a*y-c*x)/s;
      if (u>0) and (u<1) then begin
        Result:=true;
      end;
    end;
  end;
end;


function GetClickError: float;
begin
  with RootViewer do begin
    Result:=Pref.ClickError*
      sqrt((JPeg1.Height+JPeg1.Width+1)/
           (Bmp1.Height+Bmp1.Width+1));
  end;
end;

function RealXToScr( x,RW: Float; CW: integer ): integer;
begin
  if RW=0 then begin
    Result:=0;
    Exit;
  end;
  Result:=Round(CW*(x/RW));
end;

function RealYToScr( y,RH: Float; CH: integer ): integer;
begin
  if RH=0 then begin
    Result:=0;
    Exit;
  end;
  Result:=Round(CH*(y/RH));
end;

function ScrXToReal( x,CW: integer; RW: Float): float;
begin
  if CW=0 then begin
    Result:=0;
    Exit;
  end;
  Result:=RW*(x/(CW-ord(CW>100)));
end;

function ScrYToReal( y,CH: integer; RH: Float): float;
begin
  if CH=0 then begin
    Result:=0;
    Exit;
  end;
  Result:=RH*(y/(CH-ord(CH>100)));
end;

function WrFloatStr( v: float; d: integer): String;
var i: integer;
    s: String;
begin
  if v<0 then begin
    s:='-';
    v:=Abs(v);
  end;
  s:=s+IntToStr(Trunc(v));
  s:=s+'.';
  for i:=1 to d-1 do begin
    v:=v*10;
    s:=s+IntToStr(Trunc(v) mod 10);
  end;
  v:=v*10;
  s:=s+IntToStr(Round(v) mod 10);
  Result:=s;
end;

{ ------------------------------------------------------- }
{ *** Data Dialog --------------------------------------- }
{ ------------------------------------------------------- }

procedure TDataDlg.HelpBtnClick(Sender: TObject);
begin
  if Length(HlpFile)>0 then begin
    WinHelp(RootViewer.Handle, @HlpFile[1], Help_Key,
      Integer(PChar('Dialog: Categories')));
  end else begin
    Application.HelpContext(HelpContext);
  end;
end;

function TDataDlg.Execute: Boolean;
begin
 { construct the form }
 DataDlg:=TDataDlg.Create(Application);
 try
   { execute; set result based on how closed }
   Result:=DataDlg.ShowModal=IDOK;
 finally
   { dispose of the form }
   DataDlg.Free;
 end;
end;

{ ------------------------------------------------------- }
{ *** Root ---------------------------------------------- }
{ ------------------------------------------------------- }

procedure TRoot.Init;
begin
  NNodes:=0;
  SetLength(Nodes, NNodes);
  Category:='';
  Tag.x:=0;
  Tag.y:=0;
  isTagFixed:=false;
  Attribute:=0;
  AveWidth:=0;
  Wm1.x:=0;
  Wm1.y:=0;
  Wm2.x:=0;
  Wm2.y:=0;
end;

procedure TRoot.CopyFrom( var Src: TRoot );
begin
  Name:=Src.Name;
  Comment:=Src.Comment;
  Category:=Src.Category;
  Tag:=Src.Tag;
  isTagFixed:=Src.isTagFixed;
  Attribute:=Src.Attribute;
  NNodes:=Src.NNodes;
  Nodes:=Src.Nodes;
  WM1:=Src.WM1;
  WM2:=Src.WM2;
  AveWidth:=Src.AveWidth;
end;

function TRoot.ReadItems( var f: TextFile ): boolean;
var i,j,Err: integer;
begin
  Err:=IOResult;
  if Err=0 then begin
    readln(f, Name);
    readln(f, Comment);
    readln(f, Category);
    readln(f, Attribute);
    readln(f, AveWidth);
    readln(f, WM1.x,WM1.y);
    readln(f, WM2.x,WM2.y);
    readln(f, Tag.x, Tag.y, j);
    isTagFixed:=j<>0;
    readln(f, NNodes);
    SetLength(Nodes, NNodes);
    for i:=0 to NNodes-1 do begin
      if Err=0 then begin
        Err:=Err or IOResult;
        readln(f, Nodes[i].x, Nodes[i].y);
      end;
    end;
  end;
  Err:=Err or IOResult;
  Result:=Err=0;
end;

function TRoot.WriteItems( var f: TextFile ): boolean;
var i,Err: integer;
begin
  Err:=IOResult;
  if Err=0 then begin
    writeln(f, Name);
    writeln(f, Comment);
    writeln(f, Category);
    writeln(f, Attribute,' Attribute');
    writeln(f, AveWidth:FXA:FXB, ' Width');
    writeln(f, WM1.x:FXA:FXB,' ',WM1.y:FXA:FXB);
    writeln(f, WM2.x:FXA:FXB,' ',WM2.y:FXA:FXB);
    writeln(f, Tag.x:FXA:FXB,' ',Tag.y:FXA:FXB, ' ', ord(isTagFixed));
    writeln(f, NNodes, ' Nodes');
    for i:=0 to NNodes-1 do begin
      if Err=0 then begin
        Err:=Err or IOResult;
        writeln(f, Nodes[i].x:FXA:FXB,' ',
                   Nodes[i].y:FXA:FXB);
      end;
    end;
  end;
  Err:=Err or IOResult;
  Result:=Err=0;
end;

function TRoot.SelectNode(X,Y: float; BmpWidth,BmpHeight: integer; Marks: boolean): integer;
var ll,xx,yy,d,d1,d2: float;
    xi: integer;
  function Distance( x,y: float; NNodes: integer;
    const Nodes: array of TFPoint): float;
  var d,c,l,a,b,u,v: float;
      i: integer;
  begin
    d:=maxint;
    xx:=x;
    yy:=y;
    xi:=-1;
    ll:=1;
    if NNodes>0 then begin
      d:=Sqr(xx-Nodes[0].x)+Sqr(yy-Nodes[0].y);
      xx:=Nodes[0].x;
      yy:=Nodes[0].y;
      xi:=0;
    end;
    for i:=1 to NNodes-1 do begin
      a:=Nodes[i-1].x;
      b:=Nodes[i-1].y;
      u:=Nodes[i].x-a;
      v:=Nodes[i].y-b;
      l:=(u*(x-a)+v*(y-b))/(Sqr(u)+Sqr(v));
      if l<0 then l:=0 else
      if l>1 then l:=1;
      c:=Sqr(a+l*u-x)+Sqr(b+l*v-y);
      if c<d then begin
        xi:=i;
        d:=c;
        xx:=a+l*u;
        yy:=b+l*v;
        ll:=l;
      end;
    end;
    Result:=d;
  end;
  function MarkOK: boolean;
  begin
    Result:=Marks and
      (Sqr(wm1.x-wm2.x)+Sqr(wm1.y-wm2.y)>0);
  end;
begin
  Result:=-1;
  d:=Distance(x,y, NNodes, Nodes);
  if sqrt(d)<GetClickError then begin
    if (xi>0) and (ll<0.5) then begin
      d:=Sqr(xx-Nodes[xi-1].x)+Sqr(yy-Nodes[xi-1].y);
      if Sqrt(d)<GetClickError then begin
        Result:=xi-1;
      end;
    end else begin
      d:=Sqr(xx-Nodes[xi].x)+Sqr(yy-Nodes[xi].y);
      if Sqrt(d)<GetClickError then begin
        Result:=xi;
      end;
    end;
  end;
  d1:=Sqr(wm1.x-x)+Sqr(wm1.y-y);
  d2:=Sqr(wm2.x-x)+Sqr(wm2.y-y);
  if MarkOK and (d1<=d2) and (d1<=d) then begin
    if Sqrt(d1)<GetClickError then begin
      Result:=-2;
    end else begin
      d:=Sqr((wm1.x+wm2.x)/2-x)+Sqr((wm1.y+wm2.y)/2-y);
      if sqrt(d)<GetClickError then begin
        Result:=-2;
      end;
    end;
  end else
  if MarkOK and (d2<=d) then begin
    if Sqrt(d2)<GetClickError then begin
      Result:=-3;
    end else begin
      d:=Sqr((wm1.x+wm2.x)/2-x)+Sqr((wm1.y+wm2.y)/2-y);
      if sqrt(d)<GetClickError then begin
        Result:=-3;
      end;
    end;
  end;
end;

function TRoot.InsertNode(X,Y: float; BmpWidth,BmpHeight: integer): integer;
var ll,xx,yy,d: float;
    i,xi: integer;
  function Distance( x,y: float; NNodes: integer;
    const Nodes: array of TFPoint): float;
  var d,c,l,a,b,u,v: float;
      i: integer;
  begin
    d:=maxint;
    xx:=x;
    yy:=y;
    xi:=-1;
    ll:=1;
    if NNodes>0 then begin
      d:=Sqr(xx-Nodes[0].x)+Sqr(yy-Nodes[0].y);
      xx:=Nodes[0].x;
      yy:=Nodes[0].y;
      xi:=0;
    end;
    for i:=1 to NNodes-1 do begin
      a:=Nodes[i-1].x;
      b:=Nodes[i-1].y;
      u:=Nodes[i].x-a;
      v:=Nodes[i].y-b;
      l:=(u*(x-a)+v*(y-b))/(Sqr(u)+Sqr(v));
      if l<0 then l:=0 else
      if l>1 then l:=1;
      c:=Sqr(a+l*u-x)+Sqr(b+l*v-y);
      if c<d then begin
        xi:=i;
        d:=c;
        xx:=a+l*u;
        yy:=b+l*v;
        ll:=l;
      end;
    end;
    Result:=d;
  end;
begin
  Result:=-1;
  d:=Distance(x,y, NNodes, Nodes);
  d:=sqrt(d);
  if d<GetClickError then begin
    if (xi>0) and (ll<0.5) then begin
      d:=Sqr(xx-Nodes[xi-1].x)+Sqr(yy-Nodes[xi-1].y);
      if Sqrt(d)<GetClickError then begin
        Result:=xi-1;
      end;
    end else begin
      d:=Sqr(xx-Nodes[xi].x)+Sqr(yy-Nodes[xi].y);
      if Sqrt(d)<GetClickError then begin
        Result:=xi;
      end;
    end;
    if Result<0 then begin
      Result:=xi;
      Inc(NNodes);
      SetLength(Nodes, NNodes);
      for i:=NNodes-1 downto xi+1 do begin
        Nodes[i]:=Nodes[i-1];
      end;
      Nodes[xi].x:=xx;
      Nodes[xi].y:=yy;
    end;
  end;
end;

procedure TRoot.DelNode( Ind: integer );
var i: integer;
begin
  if (Ind>=0) and (Ind<NNodes) then begin
    for i:=Ind to NNodes-2 do begin
      Nodes[i]:=Nodes[i+1];
    end;
    Dec(NNodes);
    SetLength(Nodes, NNodes);
  end;
end;

procedure TRoot.FixLabel;
var i: integer;
begin
  if not isTagFixed then begin
    Tag.x:=0;
    Tag.y:=0;
    for i:=0 to NNodes-1 do begin
      Tag.x:=Tag.x+Nodes[i].x;
      Tag.y:=Tag.y+Nodes[i].y;
    end;
    if NNodes>0 then begin
      Tag.x:=Tag.x/NNodes;
      Tag.y:=Tag.y/NNodes;
    end;
  end;
end;

function TRoot.GetLen: float;
var i: integer;
    l: float;
begin
  l:=0;
  for i:=0 to NNodes-2 do begin
    l:=l+Sqrt(Sqr(Nodes[i].x-Nodes[i+1].x)+
              Sqr(Nodes[i].y-Nodes[i+1].y));
  end;
  Result:=l;
end;

function TRoot.FixWidthMark: boolean;
var ok: boolean;
    i: integer;
begin
  ok:=false;
  for i:=1 to NNodes-1 do begin
    ok:=ok or DoIntersect(wm1,wm2,Nodes[i-1],Nodes[i]);
  end;
  Result:=not ok;
  if not ok then begin
    wm1.x:=0;
    wm1.y:=0;
    wm2.x:=0;
    wm2.y:=0;
  end;
  AveWidth:=
    Sqrt(Sqr(Wm1.x-Wm2.x)+Sqr(Wm1.y-Wm2.y));
end;

{ ------------------------------------------------------- }
{ *** Tip ----------------------------------------------- }
{ ------------------------------------------------------- }

procedure TTip.Init;
begin
  Category:='';
  Tag.x:=0;
  Tag.y:=0;
  Point.x:=0;
  Point.y:=0;
  isTagFixed:=false;
  Attribute:=0;
end;

procedure TTip.CopyFrom( var Src: TTip );
begin
  Name:=Src.Name;
  Comment:=Src.Comment;
  Category:=Src.Category;
  Tag:=Src.Tag;
  Attribute:=Src.Attribute;
  isTagFixed:=Src.isTagFixed;
  Point:=Src.Point;
end;

function TTip.ReadItems( var f: TextFile ): boolean;
var j,Err: integer;
begin
  Err:=IOResult;
  if Err=0 then begin
    readln(f, Name);
    readln(f, Comment);
    readln(f, Category);
    readln(f, Attribute);
    readln(f, Tag.x, Tag.y, j);
    isTagFixed:=j<>0;
    readln(f, Point.x, Point.y);
  end;
  Err:=Err or IOResult;
  Result:=Err=0;
end;

function TTip.WriteItems( var f: TextFile ): boolean;
var Err: integer;
begin
  Err:=IOResult;
  if Err=0 then begin
    writeln(f, Name);
    writeln(f, Comment);
    writeln(f, Category);
    writeln(f, Attribute,' Attribute');
    writeln(f, Tag.x:FXA:FXB,' ',Tag.y:FXA:FXB, ' ', ord(isTagFixed));
    writeln(f, Point.x:FXA:FXB,' ',Point.y:FXA:FXB);
  end;
  Err:=Err or IOResult;
  Result:=Err=0;
end;

{ ------------------------------------------------------- }
{ *** Frame ---------------------------------------------- }
{ ------------------------------------------------------- }

procedure TFrame.Init;
begin
  NRoots:=0;
  SetLength(Roots, NRoots);
  NTips:=0;
  SetLength(Tips, NTips);
  SelNode:=-1;
  SelRoot:=-1;
  SelTip:=-1;
  isLabelDrag:=false;
  Mark.x:=0;
  Mark.y:=0;
  DX:=0;
  DY:=0;
  w:=16.00;
  h:=10.00;
  Comment:='';
end;

procedure TFrame.CopyFrom( var Src: TFrame );
begin
  Comment:=Src.Comment;
  W:=Src.W;
  H:=Src.H;
  DX:=Src.DX;
  DY:=Src.DY;
  Mark:=Src.Mark;
  NRoots:=Src.NRoots;
  NTips:=Src.NTips;
  SelRoot:=Src.SelRoot;
  SelTip:=Src.SelTip;
  SelNode:=Src.SelNode;
  Roots:=Src.Roots;
  Tips:=Src.Tips;
  LDX:=Src.LDX;
  LDY:=Src.LDY;
  fLDX:=Src.fLDX;
  fLDY:=Src.fLDY;
  isLabelDrag:=Src.isLabelDrag;
end;

function TFrame.LoadFromFile( FN: String ): boolean;
var f: text;
begin
  Result:=false;
  if OpenRead(f, FN) then begin
    if not GetPatch(f, FN) then begin
      Exit;
    end;
    Result:=ReadItems(f);
    Close(f);
  end;
end;

function TFrame.SaveToFile( FN: String ): boolean;
var f: text;
begin
  Result:=false;
  if OpenWrite(f, FN) then begin
    if not PutPatch(f, FN) then begin
      Exit;
    end;
    Result:=WriteItems(f);
    Close(f);
  end;
end;

function TFrame.ReadItems( var f: TextFile ): boolean;
var i,Err: integer;
begin
  Err:=IOResult;
  if Err=0 then begin
    readln(f, Comment);
    readln(f, W, H);
    readln(f, Mark.x, Mark.y);
    readln(f, DX, DY);
  end;
  Err:=Err or IOResult;
  if Err=0 then begin
    readln(f, NRoots);
    SetLength(Roots, NRoots);
    for i:=0 to NRoots-1 do begin
      if not Roots[i].ReadItems(f) then Err:=1;
    end;
  end;
  Err:=Err or IOResult;
  if Err=0 then begin
    readln(f, NTips);
    SetLength(Tips, NTips);
    for i:=0 to NTips-1 do begin
      if not Tips[i].ReadItems(f) then Err:=1;
    end;
  end;
  Err:=Err or IOResult;
  Result:=Err=0;
end;

function TFrame.WriteItems( var f: TextFile ): boolean;
var i,Err: integer;
begin
  Err:=IOResult;
  if Err=0 then begin
    writeln(f, Comment);
    writeln(f, W:FXA:FXB,' ',H:FXA:FXB);
    writeln(f, Mark.x:FXA:FXB,' ',Mark.y:FXA:FXB);
    writeln(f, DX:FXA:FXB,' ',DY:FXA:FXB);
  end;
  Err:=Err or IOResult;
  if Err=0 then begin
    writeln(f, NRoots, ' Roots');
    for i:=0 to NRoots-1 do begin
      if not Roots[i].WriteItems(f) then Err:=1;
    end;
  end;
  Err:=Err or IOResult;
  if Err=0 then begin
    writeln(f, NTips, ' Tips');
    for i:=0 to NTips-1 do begin
      if not Tips[i].WriteItems(f) then Err:=1;
    end;
  end;
  Err:=Err or IOResult;
  Result:=Err=0;
end;

procedure TFrame.DrawItems( Bitmap: TBitmap );
var i,j: integer;
  procedure PutLabel(Tag: String; pt: TFPoint; Sel: boolean; Cat: String; Attribute: integer; isTip: boolean=false);
  begin
    Bitmap.Canvas.Font.Name:=Pref.FontName;
    Bitmap.Canvas.Font.Height:=GetFontHeight;
    Bitmap.Canvas.Font.Size:=GetFontSize;
    Bitmap.Canvas.Font.Pitch:=TFontPitch(Pref.FontPitch);
    Bitmap.Canvas.Font.Charset:=Pref.FontCharset;
    Bitmap.Canvas.Font.Style:=[];
    if Pref.FontBold then begin
      Bitmap.Canvas.Font.Style:=Bitmap.Canvas.Font.Style+[fsBold];
    end;
    if Pref.FontItalic then begin
      Bitmap.Canvas.Font.Style:=Bitmap.Canvas.Font.Style+[fsItalic];
    end;
    if Sel then begin
      Bitmap.Canvas.Brush.Color:=Pref.CustomColors[Pref.SelLabelBack];
      Bitmap.Canvas.Font.Color:=Pref.CustomColors[Pref.SelLabelFont];
    end else begin
      Bitmap.Canvas.Brush.Color:=Pref.CustomColors[Pref.LabelBack];
      Bitmap.Canvas.Font.Color:=Pref.CustomColors[Pref.LabelFont];
    end;
    if not Sel and (Pref.WriteTransparent and (Attribute<=1)) then begin
      SetBkMode(Bitmap.Canvas.Handle, TRANSPARENT);
      if isTip then begin
        Bitmap.Canvas.Font.Color:=Experiment.GetTipCatPenColor(Cat);
      end else begin
        Bitmap.Canvas.Font.Color:=Experiment.GetCatPenColor(Cat);
      end;
    end;
    Bitmap.Canvas.TextOut(
      RealXToScr(pt.x,W,Bitmap.Width),
      RealYToScr(pt.y,H,Bitmap.Height),
      ' '+Tag+' ');
    if not Sel and (Pref.WriteTransparent and (Attribute<=1)) then begin
      SetBkMode(Bitmap.Canvas.Handle, OPAQUE);
    end;
  end;
  procedure SelectCategory(Cat: String);
  begin
    Bitmap.Canvas.Pen.Color:=Experiment.GetCatPenColor(Cat);
    Bitmap.Canvas.Pen.Width:=Experiment.GetCatPenWidth(Cat);
  end;
  procedure MarkSelNode( X,Y: integer; Sel,Last: boolean; Cat: String );
  var d: integer;
  begin
    if Sel then begin
      Bitmap.Canvas.Brush.Color:=Pref.CustomColors[Pref.SelNodeColor];
    end else begin
      Bitmap.Canvas.Brush.Color:=Pref.CustomColors[Pref.SelItemColor];
    end;
    Bitmap.Canvas.Pen.Color:=Experiment.GetCatPenColor(Cat);
    Bitmap.Canvas.Pen.Width:=1;
    d:=4;
    if Last then begin
      Bitmap.Canvas.Rectangle(X-d, Y-d, X+d, Y+d);
      Bitmap.Canvas.Rectangle(X-1, Y-1, X+1, Y+1);
    end else begin
      Bitmap.Canvas.Ellipse(X-d, Y-d, X+d, Y+d);
    end;
  end;
  procedure PutMark(AveWidth: float; pt1,pt2: TFPoint; Sel: boolean);
  var x,y: integer;
  begin
    if AveWidth>0 then begin
      Bitmap.Canvas.Pen.Color:=Pref.CustomColors[Pref.WMarkColor];
      Bitmap.Canvas.Pen.Width:=1;
      Bitmap.Canvas.MoveTo(
        RealXToScr(pt1.x,W,Bitmap.Width),
        RealYToScr(pt1.y,H,Bitmap.Height));
      Bitmap.Canvas.LineTo(
        RealXToScr(pt2.x,W,Bitmap.Width),
        RealYToScr(pt2.y,H,Bitmap.Height));
      if Sel and (SelNode<-1) then begin
        Bitmap.Canvas.Pen.Color:=Pref.CustomColors[Pref.SelItemColor];
        if SelNode=-2 then begin
          Bitmap.Canvas.MoveTo(
            RealXToScr(pt1.x,W,Bitmap.Width),
            RealYToScr(pt1.y,H,Bitmap.Height));
        end else begin
          Bitmap.Canvas.MoveTo(
            RealXToScr(pt2.x,W,Bitmap.Width),
            RealYToScr(pt2.y,H,Bitmap.Height));
        end;
        x:=(RealXToScr(pt1.x,W,Bitmap.Width)+RealXToScr(pt2.x,W,Bitmap.Width)) div 2;
        y:=(RealYToScr(pt1.y,H,Bitmap.Height)+RealYToScr(pt2.y,H,Bitmap.Height)) div 2;
        Bitmap.Canvas.LineTo(X,Y);
        Bitmap.Canvas.Brush.Color:=Pref.CustomColors[Pref.WMarkColor];
        Bitmap.Canvas.Ellipse(X-2,Y-2,X+3,Y+3);
      end;
    end;
  end;
  procedure DrawTip(X,Y: integer; Cat: String; Sel: boolean; Attr: integer);
  var w: integer;
      p: array of TPoint;
  begin
    w:=Experiment.GetTipCatPenWidth(Cat)+ord(Sel or (Attr>0));
    Bitmap.Canvas.Pen.Width:=1;
    Bitmap.Canvas.Brush.Color:=Experiment.GetTipCatPenColor(Cat);
    if Sel or (Attr>0) then begin
      Bitmap.Canvas.Pen.Color:=0;
    end else begin
      Bitmap.Canvas.Pen.Color:=Bitmap.Canvas.Brush.Color;
    end;
    SetLength(p, 5);
    p[0].x:=x-w;
    p[0].y:=y;
    p[1].x:=x;
    p[1].y:=y+w;
    p[2].x:=x+w;
    p[2].y:=y;
    p[3].x:=x;
    p[3].y:=y-w;
    p[4].x:=x-w;
    p[4].y:=y;
    Bitmap.Canvas.Polygon(p);
    if Attr>0 then begin
      Bitmap.Canvas.MoveTo(p[0].x,p[0].y);
      Bitmap.Canvas.LineTo(p[2].x,p[2].y);
      Bitmap.Canvas.MoveTo(p[1].x,p[1].y);
      Bitmap.Canvas.LineTo(p[3].x,p[3].y);
    end;
  end;
  procedure DrawMarkPoint(X,Y: integer);
  var d: integer;
  begin
    d:=14;
    Bitmap.Canvas.Pen.Color:=Pref.CustomColors[Pref.FMarkColor];
    Bitmap.Canvas.Pen.Width:=1;
    Bitmap.Canvas.MoveTo(X, Y-d);
    Bitmap.Canvas.LineTo(X, Y+d);
    Bitmap.Canvas.MoveTo(X-d, Y);
    Bitmap.Canvas.LineTo(X+d, Y);
  end;
begin
  if areRootsVisible then begin
    for i:=0 to NRoots-1 do begin
      with Roots[i] do begin
        if (Attribute<2) or areGoneVisible then begin
          SelectCategory(Category);
          if NNodes>0 then begin
            Bitmap.Canvas.MoveTo(
              RealXToScr(Nodes[0].x,W,Bitmap.Width),
              RealYToScr(Nodes[0].y,H,Bitmap.Height));
            if Attribute>0 then begin
              Bitmap.Canvas.Pen.Width:=1;
              if Attribute>1 then begin
                Bitmap.Canvas.Pen.Style:=psDot;
              end else begin
                Bitmap.Canvas.Pen.Style:=psDash;
              end;
            end else begin
              Bitmap.Canvas.Pen.Style:=psSolid;
            end;
            for j:=1 to Roots[i].NNodes-1 do begin
              Bitmap.Canvas.LineTo(
                RealXToScr(Nodes[j].x,W,Bitmap.Width),
                RealYToScr(Nodes[j].y,H,Bitmap.Height));
            end;
            Bitmap.Canvas.Pen.Style:=psSolid;
          end;
          if areLabelsVisible then begin
            PutLabel(Name, Tag, i=SelRoot, Category, Attribute);
          end;
          if areWidthsVisible then begin
            PutMark(AveWidth, WM1,WM2, i=SelRoot);
          end;
        end;
      end;
    end;
  end;
  if areTipsVisible then begin
    for i:=0 to NTips-1 do begin
      with Tips[i] do begin
        if (Attribute<2) or areGoneVisible then begin
          if areLabelsVisible then begin
            PutLabel(Name, Tag, i=SelTip, Category, Attribute, true);
          end;
          DrawTip(
            RealXToScr(Point.x,W,Bitmap.Width),
            RealYToScr(Point.y,H,Bitmap.Height),
            Category, i=SelTip, Attribute);
        end;
      end;
    end;
  end;
  if (SelRoot>=0) and (SelRoot<NRoots) then begin
    with Roots[SelRoot] do begin
      for j:=0 to NNodes-1 do begin
        MarkSelNode(
          RealXToScr(Nodes[j].x,W,Bitmap.Width),
          RealYToScr(Nodes[j].y,H,Bitmap.Height),
          j=SelNode, j=NNodes-1, Category);
      end;
    end;
  end;
  if isMarkVisible then begin
    DrawMarkPoint(
        RealXToScr(Mark.x,W,Bitmap.Width),
        RealYToScr(Mark.y,H,Bitmap.Height));
  end;
end;

procedure TFrame.DelTip( Ind: integer );
var i: integer;
begin
  if Ind<NTips then begin
    for i:=Ind to NTips-2 do begin
      Tips[i].CopyFrom(Tips[i+1]);
    end;
    Dec(NTips);
    SetLength(Tips, NTips);
  end;
end;

procedure TFrame.NewTip;
begin
  Inc(NTips);
  SetLength(Tips, NTips);
  SelTip:=NTips-1;
  Tips[SelTip].Init;
end;

procedure TFrame.DelRoot( Ind: integer );
var i: integer;
begin
  if Ind<NRoots then begin
    for i:=Ind to NRoots-2 do begin
      Roots[i].CopyFrom(Roots[i+1]);
    end;
    Dec(NRoots);
    SetLength(Roots, NRoots);
  end;
end;

procedure TFrame.NewRoot;
begin
  Inc(NRoots);
  SetLength(Roots, NRoots);
  SelRoot:=NRoots-1;
  Roots[SelRoot].Init;
end;

procedure TFrame.NewNode(Bitmap: TBitmap; X,Y: integer);
begin
  if SelRoot>=0 then begin
    with Roots[SelRoot] do begin
      Inc(NNodes);
      SetLength(Nodes, NNodes);
      Nodes[NNodes-1].x:=ScrXToReal(x,Bitmap.Width,W);
      Nodes[NNodes-1].y:=ScrYToReal(y,Bitmap.Height,H);
      FixLabel;
    end;
  end;
end;

procedure TFrame.SelObject(Bitmap: TBitmap; X,Y: integer);
var d,err,xx,yy: float;
    sel,i: integer;
  function Distance( x,y: float; NNodes: integer;
    const Nodes: array of TFPoint): float;
  var d,c,l,a,b,u,v: float;
      i: integer;
  begin
    d:=maxint;
    if NNodes>0 then begin
      d:=Sqr(xx-Nodes[0].x)+Sqr(yy-Nodes[0].y);
    end;
    for i:=1 to NNodes-1 do begin
      a:=Nodes[i-1].x;
      b:=Nodes[i-1].y;
      u:=Nodes[i].x-a;
      v:=Nodes[i].y-b;
      l:=(u*(x-a)+v*(y-b))/(Sqr(u)+Sqr(v));
      if l<0 then l:=0 else
      if l>1 then l:=1;
      c:=Sqr(a+l*u-x)+Sqr(b+l*v-y);
      if c<d then d:=c;
    end;
    Result:=d;
  end;
begin
  SelRoot:=-1;
  SelTip:=-1;
  xx:=ScrXToReal(x,Bitmap.Width,W);
  yy:=ScrYToReal(y,Bitmap.Height,H);
  sel:=-1;
  err:=maxint;
  if areTipsVisible then begin
    for i:=0 to NTips-1 do begin
      if (Tips[i].Attribute<2) or areGoneVisible then begin
        d:=Sqr(xx-Tips[i].Point.x)+Sqr(yy-Tips[i].Point.y);
        if d<err then begin
          err:=d;
          sel:=i;
        end;
      end;
    end;
    if Sqrt(err)<GetClickError then begin
      SelTip:=sel;
    end;
    sel:=-1;
  end;
  if areRootsVisible then begin
    for i:=0 to NRoots-1 do begin
      if (Roots[i].Attribute<2) or areGoneVisible then begin
        d:=Distance(xx,yy,Roots[i].NNodes, Roots[i].Nodes);
        if d<err then begin
          err:=d;
          sel:=i;
        end;
      end;
    end;
    if sel>=0 then begin
      SelTip:=-1;
      if Sqrt(err)<GetClickError then begin
        SelRoot:=sel;
      end;
    end;
  end;
end;

function TFrame.SelWidthMark(Bitmap: TBitmap; X,Y: integer): boolean;
var d,err,xx,yy: float;
    sel,i: integer;
  function Distance( x,y: float; NNodes: integer;
    const Nodes: array of TFPoint): float;
  var d,c,l,a,b,u,v: float;
      i: integer;
  begin
    d:=maxint;
    if NNodes>0 then begin
      d:=Sqr(xx-Nodes[0].x)+Sqr(yy-Nodes[0].y);
    end;
    for i:=1 to NNodes-1 do begin
      a:=Nodes[i-1].x;
      b:=Nodes[i-1].y;
      u:=Nodes[i].x-a;
      v:=Nodes[i].y-b;
      l:=(u*(x-a)+v*(y-b))/(Sqr(u)+Sqr(v));
      if l<0 then l:=0 else
      if l>1 then l:=1;
      c:=Sqr(a+l*u-x)+Sqr(b+l*v-y);
      if c<d then d:=c;
    end;
    Result:=d;
  end;
begin
  Result:=false;
  xx:=ScrXToReal(x,Bitmap.Width,W);
  yy:=ScrYToReal(y,Bitmap.Height,H);
  sel:=-1;
  err:=maxint;
  for i:=0 to NRoots-1 do begin
    d:=Distance(xx,yy,Roots[i].NNodes, Roots[i].Nodes);
    if d<err then begin
      err:=d;
      sel:=i;
    end;
  end;
  if sel>=0 then begin
    if Sqrt(err)<GetClickError*2+(W+H)/64 then begin
      Result:=SelRoot=sel;
    end;
  end;
end;

function TFrame.GetRootLabelClick(Bitmap: TBitmap; X,Y: integer): boolean;
var i,xx,yy,ww,hh: integer;
begin
  Result:=false;
  if not (areRootsVisible and areLabelsVisible) then begin
    Exit;
  end;
  if NRoots>0 then begin
    Bitmap.Canvas.Font.Name:=Pref.FontName;
    Bitmap.Canvas.Font.Height:=GetFontHeight;
    Bitmap.Canvas.Font.Size:=GetFontSize;
    Bitmap.Canvas.Font.Pitch:=TFontPitch(Pref.FontPitch);
    Bitmap.Canvas.Font.Charset:=Pref.FontCharset;
    Bitmap.Canvas.Font.Style:=[];
    if Pref.FontBold then begin
      Bitmap.Canvas.Font.Style:=Bitmap.Canvas.Font.Style+[fsBold];
    end;
    if Pref.FontItalic then begin
      Bitmap.Canvas.Font.Style:=Bitmap.Canvas.Font.Style+[fsItalic];
    end;
  end;
  for i:=0 to NRoots-1 do begin
    xx:=RealXToScr(Roots[i].Tag.x,W,Bitmap.Width);
    yy:=RealXToScr(Roots[i].Tag.y,H,Bitmap.Height);
    ww:=Bitmap.Canvas.TextWidth(' '+Roots[i].Name+' ');
    hh:=Bitmap.Canvas.TextHeight(' '+Roots[i].Name+' ');
    if (x>xx) and (x<xx+ww) and
       (y>yy) and (y<yy+hh) then begin
      Result:=true;
      SelRoot:=i;
      LDX:=xx-x;
      LDY:=yy-y;
    end;
  end;
end;

function TFrame.GetTipLabelClick(Bitmap: TBitmap; X,Y: integer): boolean;
var i,xx,yy,ww,hh: integer;
begin
  Result:=false;
  if not (areTipsVisible and areLabelsVisible) then begin
    Exit;
  end;
  if NTips>0 then begin
    Bitmap.Canvas.Font.Name:=Pref.FontName;
    Bitmap.Canvas.Font.Height:=GetFontHeight;
    Bitmap.Canvas.Font.Size:=GetFontSize;
    Bitmap.Canvas.Font.Pitch:=TFontPitch(Pref.FontPitch);
    Bitmap.Canvas.Font.Charset:=Pref.FontCharset;
    Bitmap.Canvas.Font.Style:=[];
    if Pref.FontBold then begin
      Bitmap.Canvas.Font.Style:=Bitmap.Canvas.Font.Style+[fsBold];
    end;
    if Pref.FontItalic then begin
      Bitmap.Canvas.Font.Style:=Bitmap.Canvas.Font.Style+[fsItalic];
    end;
  end;
  for i:=0 to NTips-1 do begin
    xx:=RealXToScr(Tips[i].Tag.x,W,Bitmap.Width);
    yy:=RealXToScr(Tips[i].Tag.y,H,Bitmap.Height);
    ww:=Bitmap.Canvas.TextWidth(' '+Tips[i].Name+' ');
    hh:=Bitmap.Canvas.TextHeight(' '+Tips[i].Name+' ');
    if (x>xx) and (x<xx+ww) and
       (y>yy) and (y<yy+hh) then begin
      Result:=true;
      SelTip:=i;
      LDX:=xx-x;
      LDY:=yy-y;
    end;
  end;
end;

function TFrame.isMarkVisible: boolean;
begin
  Result:=RootViewer.RegButton.Down;
end;

function TFrame.areLabelsVisible: boolean;
begin
  Result:=RootViewer.LabelsBtn.Down;
end;

function TFrame.areRootsVisible: boolean;
begin
  Result:=RootViewer.RootsBtn.Down;
end;

function TFrame.areTipsVisible: boolean;
begin
  Result:=RootViewer.TipsBtn.Down;
end;

function TFrame.areWidthsVisible: boolean;
begin
  Result:=RootViewer.WidthsBtn.Down;
end;

function TFrame.areGoneVisible: boolean;
begin
  Result:=RootViewer.GhostBtn.Down;
end;

function TFrame.GetFontSize: integer;
begin
  with RootViewer do begin
    Result:=Round(Pref.FontSize*
      sqrt((Bmp1.Height+Bmp1.Width+1)/
           (JPeg1.Height+JPeg1.Width+1)));
  end;
end;

function TFrame.GetFontHeight: integer;
begin
  with RootViewer do begin
    Result:=Round(Pref.FontHeight*
      sqrt((Bmp1.Height+Bmp1.Width+1)/
           (JPeg1.Height+JPeg1.Width+1)));
  end;
end;

function TFrame.GetNextRootName: String;
var s: String;
    i: integer;
  function notFound(s: String): boolean;
  var i: integer;
  begin
    Result:=true;
    i:=0;
    while Result and (i<NRoots) do begin
      Result:=s<>Roots[i].Name;
      Inc(i);
    end;
  end;
begin
  i:=NRoots;
  repeat
    Inc(i);
    s:=Pref.RootPrefix+IntToStr(i);
  until notFound(s);
  Result:=s;
end;

function TFrame.GetNextTipName: String;
var s: String;
    i: integer;
  function notFound(s: String): boolean;
  var i: integer;
  begin
    Result:=true;
    i:=0;
    while Result and (i<NTips) do begin
      Result:=s<>Tips[i].Name;
      Inc(i);
    end;
  end;
begin
  i:=NTips;
  repeat
    Inc(i);
    s:=Pref.TipPrefix+IntToStr(i);
  until notFound(s);
  Result:=s;
end;

{ ------------------------------------------------------- }
{ *** Take ---------------------------------------------- }
{ ------------------------------------------------------- }

procedure TTake.Init;
begin
  Name:='';
end;

{ ------------------------------------------------------- }
{ *** Sites --------------------------------------------- }
{ ------------------------------------------------------- }

procedure TSite.Init;
begin
  NTubes:=0;
  SetLength(Tubes, NTubes);
end;

function TSite.ReadItems( var f: TextFile ): boolean;
var i,j,Err: integer;
    xs: String;
begin
  Err:=IOResult;
  if Err=0 then begin
    readln(f);
    readln(f, Name);
    readln(f, NTubes);
    SetLength(Tubes, NTubes);
    for i:=0 to NTubes-1 do begin
      with Tubes[i] do begin
        readln(f, xs);
        j:=1;
        while (j<=Length(xs)) and (xs[j]<=' ') do Inc(j);
        Name:='';
        while j<=Length(xs) do begin
          Name:=Name+xs[j];
          Inc(j);
        end;
        readln(f, Offset, Frames, Width, Height);
        Frames:=Frames-Offset+1;
      end;
    end;
  end;
  Err:=Err or IOResult;
  Result:=Err=0;
end;

{ ------------------------------------------------------- }
{ *** Experiment ---------------------------------------- }
{ ------------------------------------------------------- }

procedure TExperiment.Init;
var dir: String;
begin
  Name:='';
  NSites:=0;
  SetLength(Sites, NSites);
  NTakes:=0;
  SetLength(Takes, NTakes);
  TakeIndex:=-1;
  PlotIndex:=-1;
  TubeIndex:=-1;
  FrameIndex:=-1;
  NRootCat:=0;
  SetLength(RootCat, NRootCat);
  NTipCat:=0;
  SetLength(TipCat, NTipCat);
  dir:=ParamStr(0);
  dir:=GetLongFileName(dir);
  dir:=GetFilePath(dir);
  Experiment.CatFile:=dir+'\RootView.cat';
  Experiment.CfgFile:=dir+'\RootView.ini';
  if FileExists(Experiment.CfgFile) then begin
    Experiment.GetPref;
  end;
  if FileExists(Experiment.CatFile) then begin
    Experiment.GetCats;
  end;
end;

function TExperiment.CreateFolders: integer;
var i,j,k,c: integer;
    s,s1,s2: String;
begin
  c:=0;
  for i:=0 to NTakes-1 do begin
    s:=GetFilePath(Name);
    with Takes[i] do begin
      s:=s+'\'+Name;
      if not FolderExists(s) then begin
        MkDir(s);
        Inc(c);
      end;
      for j:=0 to NSites-1 do begin
        s1:=s+'\'+Sites[j].Name;
        if not FolderExists(s1) then begin
          MkDir(s1);
          Inc(c);
        end;
        for k:=0 to Sites[j].NTubes-1 do begin
          s2:=s1+'\'+Sites[j].Tubes[k].Name;
          if not FolderExists(s2) then begin
            MkDir(s2);
            Inc(c);
          end;
        end;
      end;
    end;
  end;
  Result:=c;
end;

function TExperiment.GetData: boolean;
var i,Err: integer;
    f: TextFile;
    s: String;
begin
  Result:=false;
  if not OpenRead(f, Name) then begin
    Exit;
  end;
  Err:=IOResult;
  if Err=0 then begin
    readln(f, NTakes);
    Err:=Err or IOResult;
    if (Err=0) and (NTakes>=0) then begin
      SetLength(Takes, NTakes);
      for i:=0 to NTakes-1 do begin
        Takes[i].Init;
        readln(f, Takes[i].Name);
      end;
    end;
    Err:=Err or IOResult;
    if Err<>0 then begin
      ErrorMsg('Error reading measurements from "'+Name+'".');
      NTakes:=0;
    end;
  end;
  Err:=Err or IOResult;
  if Err=0 then begin
    readln(f, NSites);
    Err:=Err or IOResult;
    if (Err=0) and (NSites>=0) then begin
      SetLength(Sites, NSites);
      for i:=0 to NSites-1 do begin
        Sites[i].Init;
        if (Err=0) and not Sites[i].ReadItems(f) then Err:=1;
      end;
    end;
    Err:=Err or IOResult;
    if Err<>0 then begin
      ErrorMsg('Error reading experiment layout from "'+Name+'".');
      NSites:=0;
    end;
  end;
  Err:=Err or IOResult;
  Close(f);
  Result:=Err=0;
  if Result then begin
    s:=GetFilePath(Name)+'\'+GetFileName(CfgFile)+GetFileType(CfgFile);
    if FileExists(s) then begin
      CfgFile:=s;
      GetPref;
    end;
    s:=GetFilePath(Name)+'\'+GetFileName(CatFile)+GetFileType(CatFile);
    if FileExists(s) then begin
      CatFile:=s;
      GetCats;
    end;
  end;
end;

function TExperiment.GetPref: boolean;
var f: TextFile;
begin
  Result:=false;
  if not OpenRead(f, CfgFile) then begin
    Exit;
  end;
  if GetPatch(f, CfgFile) then begin
    Result:=Pref.ReadItems(f);
  end;
  Close(f);
end;

function TExperiment.WrPref: boolean;
var f: TextFile;
begin
  Result:=false;
  if not OpenWrite(f, CfgFile) then begin
    Exit;
  end;
  if PutPatch(f, CfgFile) then begin
    Result:=Pref.WriteItems(f);
  end;
  Close(f);
end;

function TExperiment.GetCats: boolean;
var i,Err: integer;
    f: TextFile;
begin
  Result:=false;
  if not OpenRead(f, CatFile) then begin
    Exit;
  end;
  if not GetPatch(f, CatFile) then begin
    Exit;
  end;
  Err:=IOResult;
  if Err=0 then begin
    readln(f, NRootCat);
    Err:=Err or IOResult;
    if (Err<>0) or (NRootCat<0) then begin
      ErrorMsg('Category File "'+CatFile+'" is Corrupted (Roots)...');
      NRootCat:=0;
      Exit;
    end;
    SetLength(RootCat, NRootCat);
    for i:=0 to NRootCat-1 do begin
      readln(f);
      readln(f, RootCat[i].Name);
      readln(f, RootCat[i].Comment);
      readln(f, RootCat[i].Key);
      readln(f, RootCat[i].Color);
      readln(f, RootCat[i].Width);
    end;
  end;
  Err:=Err or IOResult;
  if Err=0 then begin
    readln(f, NTipCat);
    if (Err<>0) or (NTipCat<0) then begin
      ErrorMsg('Category File "'+CatFile+'" is Corrupted (Tips)...');
      NTipCat:=0;
      Exit;
    end;
    SetLength(TipCat, NTipCat);
    for i:=0 to NTipCat-1 do begin
      readln(f);
      readln(f, TipCat[i].Name);
      readln(f, TipCat[i].Comment);
      readln(f, TipCat[i].Key);
      readln(f, TipCat[i].Color);
      readln(f, TipCat[i].Width);
    end;
  end;
  Err:=Err or IOResult;
  Result:=Err=0;
  Close(f);
  RootViewer.FixCategories;
end;

function TExperiment.WrCats: boolean;
var i,Err: integer;
    f: TextFile;
begin
  Result:=false;
  if not OpenWrite(f, CatFile) then begin
    Exit;
  end;
  if not PutPatch(f, CatFile) then begin
    Exit;
  end;
  Err:=IOResult;
  if Err=0 then begin
    writeln(f, NRootCat, ' Root Categories');
    for i:=0 to NRootCat-1 do begin
      writeln(f);
      writeln(f, RootCat[i].Name);
      writeln(f, RootCat[i].Comment);
      writeln(f, RootCat[i].Key,' Shortcut');
      writeln(f, RootCat[i].Color, '  Color Index');
      writeln(f, RootCat[i].Width, '  Width in pxl');
    end;
  end;
  Err:=Err or IOResult;
  if Err=0 then begin
    writeln(f, NTipCat, ' Tip Categories');
    for i:=0 to NTipCat-1 do begin
      writeln(f);
      writeln(f, TipCat[i].Name);
      writeln(f, TipCat[i].Comment);
      writeln(f, TipCat[i].Key,' Shortcut');
      writeln(f, TipCat[i].Color, '  Color Index');
      writeln(f, TipCat[i].Width, '  Width in pxl');
    end;
  end;
  Err:=Err or IOResult;
  Result:=Err=0;
  Close(f);
end;

function TExperiment.GetCatPenColor(Cat: String): integer;
var i: integer;
begin
  i:=0;
  Result:=0;
  while (i<NRootCat) and (Cat<>RootCat[i].Name) do Inc(i);
  if i<NRootCat then begin
    Result:=Pref.CustomColors[RootCat[i].Color];
  end;
end;

function TExperiment.GetCatPenWidth(Cat: String): integer;
var i: integer;
begin
  i:=0;
  Result:=1;
  while (i<NRootCat) and (Cat<>RootCat[i].Name) do Inc(i);
  if i<NRootCat then begin
    Result:=RootCat[i].Width;
  end;
end;

function TExperiment.GetTipCatPenColor(Cat: String): integer;
var i: integer;
begin
  i:=0;
  Result:=0;
  while (i<NTipCat) and (Cat<>TipCat[i].Name) do Inc(i);
  if i<NTipCat then begin
    Result:=Pref.CustomColors[TipCat[i].Color];
  end;
end;

function TExperiment.GetTipCatPenWidth(Cat: String): integer;
var i: integer;
begin
  i:=0;
  Result:=3;
  while (i<NTipCat) and (Cat<>TipCat[i].Name) do Inc(i);
  if i<NTipCat then begin
    Result:=TipCat[i].Width;
  end;
end;

{ ------------------------------------------------------- }
{ *** Data Dialog --------------------------------------- }
{ ------------------------------------------------------- }

procedure TDataDlg.FormCreate(Sender: TObject);
var i: integer;
begin
  inherited;
  with Experiment do begin
    _NRootCat:=NRootCat;
    SetLength(_RootCat, NRootCat);
    for i:=0 to NRootCat-1 do begin
      RootCat[i].Ind:=i;
      _RootCat[i]:=RootCat[i];
    end;
    _NTipCat:=NTipCat;
    SetLength(_TipCat, NTipCat);
    for i:=0 to NTipCat-1 do begin
      TipCat[i].Ind:=i;
      _TipCat[i]:=TipCat[i];
    end;
    ColorGrid11.SetCustomColors(Pref.CustomColors);
    ColorGrid12.SetCustomColors(Pref.CustomColors);
    for i:=0 to NRootCat-1 do begin
      ListBox1.Items.Add(RootCat[i].Name);
    end;
    ListBox1.Items.Add('');
    for i:=0 to NTipCat-1 do begin
      ListBox2.Items.Add(TipCat[i].Name);
    end;
    ListBox2.Items.Add('');
  end;
  RItem:=0;
  TItem:=0;
  ListBox1.ItemIndex:=0;
  ListBox2.ItemIndex:=0;
  SetupItems;
  xnr:=0;
  xnt:=0;
  Setlength(xRA, xnr);
  Setlength(xRB, xnr);
  Setlength(xTA, xnt);
  Setlength(xTB, xnt);
  ComboBox1.Clear;
  ComboBox1.Enabled:=false;
  CheckListBox1.Clear;
  CheckListBox1.Enabled:=false;
  CCItem:=-1;
end;

procedure TDataDlg.SetupItems;
var i: integer;
begin
  if PageControl1.ActivePage.TabIndex=0 then begin
    i:=ListBox1.ItemIndex;
    RItem:=i;
    with Experiment do begin
      Button2.Enabled:=i<NRootCat;
      Button3.Enabled:=(i>0) and (i<NRootCat);
      Button4.Enabled:=i<NRootCat-1;
      if i>=NRootCat then begin
        Edit1.Enabled:=false;
        Edit1.Text:='';
        Edit2.Enabled:=false;
        Edit2.Text:='';
        Edit3.Enabled:=false;
        Edit3.Text:='';
        HotKey1.Enabled:=false;
        HotKey1.HotKey:=0;
        ColorGrid11.Enabled:=false;
        ColorGrid11.ForegroundIndex:=-1;
      end else begin
        with RootCat[i] do begin
          Edit1.Enabled:=true;
          Edit1.Text:=Name;
          Edit2.Enabled:=true;
          Edit2.Text:=IntToStr(Width);
          Edit3.Enabled:=true;
          Edit3.Text:=Comment;
          HotKey1.Enabled:=true;
          HotKey1.HotKey:=Key;
          ColorGrid11.Enabled:=true;
          ColorGrid11.ForegroundIndex:=Color;
        end;
      end;
    end;
  end else begin
    i:=ListBox2.ItemIndex;
    TItem:=i;
    with Experiment do begin
      Button6.Enabled:=i<NTipCat;
      Button7.Enabled:=(i>0) and (i<NTipCat);
      Button8.Enabled:=i<NTipCat-1;
      if i>=NTipCat then begin
        Edit4.Enabled:=false;
        Edit4.Text:='';
        Edit5.Enabled:=false;
        Edit5.Text:='';
        Edit6.Enabled:=false;
        Edit6.Text:='';
        HotKey2.Enabled:=false;
        HotKey2.HotKey:=0;
        ColorGrid12.Enabled:=false;
        ColorGrid12.ForegroundIndex:=-1;
      end else begin
        with TipCat[i] do begin
          Edit4.Enabled:=true;
          Edit4.Text:=Name;
          Edit5.Enabled:=true;
          Edit5.Text:=IntToStr(Width);
          Edit6.Enabled:=true;
          Edit6.Text:=Comment;
          HotKey2.Enabled:=true;
          HotKey2.HotKey:=Key;
          ColorGrid12.Enabled:=true;
          ColorGrid12.ForegroundIndex:=Color;
        end;
      end;
    end;
  end;
end;

function  TDataDlg.UpdateItems(index: integer): boolean;
begin
  if PageControl1.ActivePage.TabIndex=0 then begin
    with Experiment do begin
      if index<NRootCat then begin
        with RootCat[index] do begin
          Width:=StrToInt(Edit2.Text);
          Name:=Edit1.Text;
          Comment:=Edit3.Text;
          Key:=HotKey1.HotKey;
          Color:=ColorGrid11.ForegroundIndex;
        end;
      end;
    end;
  end else begin
    with Experiment do begin
      if index<NTipCat then begin
        with TipCat[index] do begin
          Width:=StrToInt(Edit5.Text);
          Name:=Edit4.Text;
          Comment:=Edit6.Text;
          Key:=HotKey2.HotKey;
          Color:=ColorGrid12.ForegroundIndex;
        end;
      end;
    end;
  end;
  Result:=true;
end;

procedure TDataDlg.ListBox1Click(Sender: TObject);
begin
  inherited;
  try
    UpdateItems(RItem);
  finally
    SetupItems;
  end;
end;

procedure TDataDlg.Button1Click(Sender: TObject);
var i: integer;
begin
  inherited;
  UpdateItems(RItem);
  with Experiment do begin
    Inc(NRootCat);
    SetLength(RootCat, NRootCat);
    for i:=NRootCat-1 downto RItem+1 do RootCat[i]:=RootCat[i-1];
    with RootCat[RItem] do begin
      Name:='';
      Color:=15;
      Width:=2;
      Comment:='';
      Ind:=$FFFF;
      Key:=0;
    end;
    ListBox1.Items.Insert(RItem, RootCat[RItem].Name);
  end;
  ListBox1.ItemIndex:=RItem;
  SetupItems;
  ActiveControl:=Edit1;
end;

procedure TDataDlg.Button2Click(Sender: TObject);
var i: integer;
begin
  inherited;
  UpdateItems(RItem);
  with Experiment do begin
    for i:=RItem to NRootCat-2 do RootCat[i]:=RootCat[i+1];
    Dec(NRootCat);
    SetLength(RootCat, NRootCat);
    ListBox1.Items.Delete(RItem);
  end;
  if RItem>ListBox1.Items.Count-1 then begin
    RItem:=ListBox1.Items.Count-1;
  end;
  ListBox1.ItemIndex:=RItem;
  SetupItems;
end;

procedure TDataDlg.Button3Click(Sender: TObject);
var c: TCategory;
begin
  inherited;
  UpdateItems(RItem);
  with Experiment do begin
    c:=RootCat[RItem];
    RootCat[RItem]:=RootCat[RItem-1];
    RootCat[RItem-1]:=c;
    ListBox1.Items[RItem-1]:=RootCat[RItem-1].Name;
    ListBox1.Items[RItem]:=RootCat[RItem].Name;
    Dec(RItem);
  end;
  ListBox1.ItemIndex:=RItem;
  SetupItems;
end;

procedure TDataDlg.Button4Click(Sender: TObject);
var c: TCategory;
begin
  inherited;
  UpdateItems(RItem);
  with Experiment do begin
    c:=RootCat[RItem];
    RootCat[RItem]:=RootCat[RItem+1];
    RootCat[RItem+1]:=c;
    ListBox1.Items[RItem]:=RootCat[RItem].Name;
    ListBox1.Items[RItem+1]:=RootCat[RItem+1].Name;
    Inc(RItem);
  end;
  ListBox1.ItemIndex:=RItem;
  SetupItems;
end;

procedure TDataDlg.Edit1Change(Sender: TObject);
begin
  inherited;
  if (ListBox1.ItemIndex>=0) and
     (ListBox1.Items[ListBox1.ItemIndex]<>Edit1.Text) then begin
    ListBox1.Items[ListBox1.ItemIndex]:=Edit1.Text;
  end;
end;

procedure TDataDlg.CancelBtnClick(Sender: TObject);
var i: integer;
begin
  dlgCancel:=true;
  inherited;
  with Experiment do begin
    NRootCat:=_NRootCat;
    SetLength(RootCat, NRootCat);
    for i:=0 to NRootCat-1 do begin
      RootCat[i]:=_RootCat[i];
    end;
    NTipCat:=_NTipCat;
    SetLength(TipCat, NTipCat);
    for i:=0 to NTipCat-1 do begin
      TipCat[i]:=_TipCat[i];
    end;
  end;
end;

procedure TDataDlg.OKBtnClick(Sender: TObject);
begin
  dlgCancel:=false;
  inherited;
  if PageControl1.ActivePage.TabIndex=0 then begin
    UpdateItems(RItem);
  end else begin
    UpdateItems(TItem);
  end;
end;

procedure TDataDlg.PageControl1Change(Sender: TObject);
begin
  inherited;
  SetupItems;
  if PageControl1.ActivePage.TabIndex=0 then begin
    ActiveControl:=ListBox1;
  end else
  if PageControl1.ActivePage.TabIndex=1 then begin
    ActiveControl:=ListBox2;
  end;
end;

procedure TDataDlg.PageControl1Changing(Sender: TObject;
  var AllowChange: Boolean);
begin
  inherited;
  if PageControl1.ActivePage.TabIndex=0 then begin
    AllowChange:=UpdateItems(RItem);
  end else
  if PageControl1.ActivePage.TabIndex=1 then begin
    AllowChange:=UpdateItems(TItem);
  end;
end;

procedure TDataDlg.Button5Click(Sender: TObject);
var i: integer;
begin
  inherited;
  UpdateItems(TItem);
  with Experiment do begin
    Inc(NTipCat);
    SetLength(TipCat, NTipCat);
    for i:=NTipCat-1 downto TItem+1 do TipCat[i]:=TipCat[i-1];
    with TipCat[TItem] do begin
      Name:='';
      Color:=1;
      Width:=4;
      Comment:='';
      Ind:=$FFFF;
      Key:=0;
    end;
    ListBox2.Items.Insert(TItem, TipCat[TItem].Name);
  end;
  ListBox2.ItemIndex:=TItem;
  SetupItems;
  ActiveControl:=Edit4;
end;

procedure TDataDlg.Button6Click(Sender: TObject);
var i: integer;
begin
  inherited;
  UpdateItems(TItem);
  with Experiment do begin
    for i:=TItem to NTipCat-2 do TipCat[i]:=TipCat[i+1];
    Dec(NTipCat);
    SetLength(TipCat, NTipCat);
    ListBox2.Items.Delete(TItem);
  end;
  if TItem>ListBox2.Items.Count-1 then begin
    TItem:=ListBox2.Items.Count-1;
  end;
  ListBox2.ItemIndex:=TItem;
  SetupItems;
end;

procedure TDataDlg.Button7Click(Sender: TObject);
var c: TCategory;
begin
  inherited;
  UpdateItems(TItem);
  with Experiment do begin
    c:=TipCat[TItem];
    TipCat[TItem]:=TipCat[TItem-1];
    TipCat[TItem-1]:=c;
    ListBox2.Items[TItem-1]:=TipCat[TItem-1].Name;
    ListBox2.Items[TItem]:=TipCat[TItem].Name;
    Dec(TItem);
  end;
  ListBox2.ItemIndex:=TItem;
  SetupItems;
end;

procedure TDataDlg.Button8Click(Sender: TObject);
var c: TCategory;
begin
  inherited;
  UpdateItems(TItem);
  with Experiment do begin
    c:=TipCat[TItem];
    TipCat[TItem]:=TipCat[TItem+1];
    TipCat[TItem+1]:=c;
    ListBox2.Items[TItem]:=TipCat[TItem].Name;
    ListBox2.Items[TItem+1]:=TipCat[TItem+1].Name;
    Inc(TItem);
  end;
  ListBox2.ItemIndex:=TItem;
  SetupItems;
end;

procedure TDataDlg.Edit4Change(Sender: TObject);
begin
  inherited;
  if (ListBox2.ItemIndex>=0) and
     (ListBox2.Items[ListBox2.ItemIndex]<>Edit4.Text) then begin
    ListBox2.Items[ListBox2.ItemIndex]:=Edit4.Text;
  end;
end;

procedure TDataDlg.FormCloseQuery(Sender: TObject; var CanClose: Boolean);
var i,j,nr,nt,dr,dt: integer;
    delR,delT: array of String;
    subRA,subRB: array of String;
    subTA,subTB: array of String;
  function FindTag(n: integer; const Cat: array of TCategory; ind: integer): integer;
  var i: integer;
  begin
    Result:=-1;
    for i:=0 to n-1 do begin
      if Cat[i].Ind=ind then begin
        Result:=i;
      end;
    end;
  end;
  function FindName(n: integer; const Cat: array of TCategory; name: String): integer;
  var i: integer;
  begin
    Result:=-1;
    for i:=0 to n-1 do begin
      if Cat[i].Name=Name then begin
        Result:=i;
      end;
    end;
  end;
  function CheckDeleted: boolean;
  begin
    Result:=CatDelDlg.Execute(dr, dt, delR, delT);
    UpdateWindow(Handle);
  end;
  function CheckSubstitutions: boolean;
  begin
    Result:=CatSubDlg.Execute(nr, nt, subRA,subRB, subTA,subTB);
    UpdateWindow(Handle);
  end;
begin
  inherited;
  canClose:=true;
  if dlgCancel or (Experiment.TakeIndex<0) then begin
    Exit;
  end;
  with Experiment do begin
    canClose:=true;
    nr:=0;
    dr:=0;
    for i:=0 to _NRootCat-1 do begin
      j:=FindTag(NRootCat, RootCat, _RootCat[i].Ind);
      if j>=0 then begin
        if RootCat[j].Name<>_RootCat[i].Name then begin
          Inc(nr);
          Setlength(SubRA, nr);
          Setlength(SubRB, nr);
          SubRA[nr-1]:=_RootCat[i].Name;
          SubRB[nr-1]:=RootCat[j].Name;
        end;
      end else begin
        j:=FindName(NRootCat, RootCat, _RootCat[i].Name);
        if j<0 then begin
          Inc(dr);
          Setlength(delR, dr);
          delR[dr-1]:=_RootCat[i].Name;
        end;
      end;
    end;
    nt:=0;
    dt:=0;
    for i:=0 to _NTipCat-1 do begin
      j:=FindTag(NTipCat, TipCat, _TipCat[i].Ind);
      if j>=0 then begin
        if TipCat[j].Name<>_TipCat[i].Name then begin
          Inc(nt);
          Setlength(SubTA, nt);
          Setlength(SubTB, nt);
          SubTA[nt-1]:=_TipCat[i].Name;
          SubTA[nt-1]:=TipCat[j].Name;
        end;
      end else begin
        j:=FindName(NTipCat, TipCat, _TipCat[i].Name);
        if j<0 then begin
          Inc(dt);
          Setlength(delT, dt);
          delT[dt-1]:=_TipCat[i].Name;
        end;
      end;
    end;
    if (dr>0) or (dt>0) then begin
      RootViewer.UpdateFrame;
      CanClose:=CheckDeleted;
    end;
    if not CanClose then Exit;
    if (nr>0) or (nt>0) then begin
      RootViewer.UpdateFrame;
      CanClose:=CheckSubstitutions;
      RootViewer.SetFrame(TakeIndex, PlotIndex, TubeIndex, FrameIndex);
    end;
  end;
end;

procedure TDataDlg.ListBox1DblClick(Sender: TObject);
begin
  inherited;
  if Edit1.Enabled then begin
    ActiveControl:=Edit1;
  end else begin
    Button1.Click;
  end;
end;

procedure TDataDlg.ListBox2Click(Sender: TObject);
begin
  inherited;
  try
    UpdateItems(TItem);
  finally
    SetupItems;
  end;
end;

procedure TDataDlg.ListBox2DblClick(Sender: TObject);
begin
  inherited;
  if Edit4.Enabled then begin
    ActiveControl:=Edit4;
  end else begin
    Button5.Click;
  end;
end;

function TDataDlg.GetFrameCount: integer;
var c,i,j: integer;
begin
  c:=0;
  with Experiment do begin
    for i:=0 to NSites-1 do begin
      with Sites[i] do begin
        for j:=0 to NTubes-1 do begin
          with Tubes[j] do begin
            Inc(c, Frames);
          end;
        end;
      end;
    end;
    c:=c*NTakes;
  end;
  Result:=c;
end;

function TDataDlg.ScanData: integer;
var x,c,t,i,j,k,l: integer;
  procedure IndicateProgress( c,t: integer );
  begin
    if t<=0 then t:=1;
    ProgressBar1.Position:=Round(100*c/t);
  end;
  function CheckOut( Take, Plot, Tube, Frame: integer ): boolean;
  var FN: String;
      xFrame: TFrame;
      i: integer;
    procedure CheckRootCat( var Cat: String );
    var i: integer;
    begin
      for i:=0 to xnr-1 do begin
        if Cat=xra[i] then begin
          Exit;
        end;
      end;
      Inc(xnr);
      SetLength(xra, xnr);
      xra[xnr-1]:=Cat;
    end;
    procedure CheckTipCat( var Cat: String );
    var i: integer;
    begin
      for i:=0 to xnt-1 do begin
        if Cat=xta[i] then begin
          Exit;
        end;
      end;
      Inc(xnt);
      SetLength(xta, xnt);
      xta[xnt-1]:=Cat;
    end;
  begin
    Result:=false;
    FN:=GetDataFileName(Take, Plot, Tube)+
      '\'+GetFrameName(Plot,Tube,Frame)+'.rvd';
    if FileExists(FN) then begin
      if xFrame.LoadFromFile(FN) then begin
        Result:=true;
        for i:=0 to xFrame.NRoots-1 do begin
          with xFrame.Roots[i] do begin
            CheckRootCat(Category);
          end;
        end;
        for i:=0 to xFrame.NTips-1 do begin
          with xFrame.Tips[i] do begin
            CheckTipCat(Category);
          end;
        end;
      end;
    end;
  end;
  function FindRoot( Cat: String ): boolean;
  var i: integer;
  begin
    with Experiment do begin
      for i:=0 to NRootCat-1 do begin
        if Cat=RootCat[i].Name then begin
          Result:=true;
          Exit;
        end;
      end;
    end;
    Result:=false;
  end;
  function FindTip( Cat: String ): boolean;
  var i: integer;
  begin
    with Experiment do begin
      for i:=0 to NTipCat-1 do begin
        if Cat=TipCat[i].Name then begin
          Result:=true;
          Exit;
        end;
      end;
    end;
    Result:=false;
  end;
begin
  StartWorking('Scanning Data...');
  xnr:=0;
  xnt:=0;
  SetLength(xra, xnr);
  SetLength(xrb, xnr);
  SetLength(xta, xnt);
  SetLength(xtb, xnt);
  ComboBox1.Clear;
  ComboBox1.Enabled:=false;
  CheckListBox1.Clear;
  CheckListBox1.Enabled:=false;
  CCItem:=-1;
  UpdateWindow(Handle);
  t:=GetFrameCount;
  c:=0;
  x:=0;
  with Experiment do begin
    for l:=0 to NTakes-1 do begin
      for i:=0 to NSites-1 do begin
        with Sites[i] do begin
          for j:=0 to NTubes-1 do begin
            with Tubes[j] do begin
              for k:=0 to Frames-1 do begin
                if not isCancelled and ((c mod 100<>0) or not isCancel) then begin
                  Inc(c);
                  if CheckOut(l, i,j,k) then begin
                    Inc(x);
                  end;
                  if (c mod 100=0) or (t<1000) then begin
                    IndicateProgress(c,t);
                  end;
                end;
              end;
            end;
          end;
        end;
      end;
    end;
  end;
  IndicateProgress(c,t);
  StopWorking;
  Result:=x;
  if isCancelled then begin
    Exit;
  end;
  CheckListBox1.Enabled:=(xnr>0) or (xnt>0);
  Setlength(xRB, xnr);
  Setlength(xTB, xnt);
  j:=0;
  for i:=0 to xnr-1 do begin
    xRB[i]:=xRA[i];
    CheckListBox1.Items.Add(xRA[i]);
    CheckListBox1.Checked[j]:=FindRoot(xRA[i]);
    Inc(j);
  end;
  CheckListBox1.Items.Add('----------------------------------------');
  CheckListBox1.Checked[j]:=false;
  CheckListBox1.State[j]:=cbGrayed;
  Inc(j);
  for i:=0 to xnt-1 do begin
    xTB[i]:=xTA[i];
    CheckListBox1.Items.Add(xTA[i]);
    CheckListBox1.Checked[j]:=FindTip(xTA[i]);
    Inc(j);
  end;
  Wait(100);
  IndicateProgress(0,1);
  EnableUpdate;
end;

function TDataDlg.UpdateData: integer;
var mf,sf,x,c,t,i,j,k,l: integer;
    rc,tc: array of integer;
    ok: boolean;
  procedure IndicateProgress( c,t: integer );
  begin
    if t<=0 then t:=1;
    ProgressBar1.Position:=Round(100*c/t);
  end;
  function CheckOut( Take, Plot, Tube, Frame: integer ): boolean;
  var FN: String;
      xFrame: TFrame;
      i: integer;
      isModified: boolean;
    procedure CheckRootCat( var Cat: String );
    var i: integer;
    begin
      for i:=0 to xnr-1 do begin
        if xra[i]<>xrb[i] then begin
          if Cat=xra[i] then begin
            Cat:=xrb[i];
            Inc(rc[i]);
            isModified:=true;
            Exit;
          end;
        end;
      end;
    end;
    procedure CheckTipCat( var Cat: String );
    var i: integer;
    begin
      for i:=0 to xnt-1 do begin
        if xta[i]<>xtb[i] then begin
          if Cat=xta[i] then begin
            Cat:=xtb[i];
            Inc(tc[i]);
            isModified:=true;
            Exit;
          end;
        end;
      end;
    end;
  begin
    Result:=false;
    if (xnr<=0) and (xnt<=0) then begin
      Exit;
    end;
    isModified:=false;
    FN:=GetDataFileName(Take, Plot, Tube)+
      '\'+GetFrameName(Plot,Tube,Frame)+'.rvd';
    if FileExists(FN) then begin
      if xFrame.LoadFromFile(FN) then begin
        Result:=true;
        if xnr>0 then begin
          for i:=0 to xFrame.NRoots-1 do begin
            with xFrame.Roots[i] do begin
              CheckRootCat(Category);
            end;
          end;
        end;
        if xnt>0 then begin
          for i:=0 to xFrame.NTips-1 do begin
            with xFrame.Tips[i] do begin
              CheckTipCat(Category);
            end;
          end;
        end;
        if isModified then begin
          Inc(mf);
          if xFrame.SaveToFile(FN) then begin
            Inc(sf);
          end;
        end;
      end;
    end;
  end;
begin
  StartWorking('Substituting...');
  mf:=0;
  sf:=0;
  t:=GetFrameCount;
  c:=0;
  x:=0;
  SetLength(rc, xnr);
  for i:=0 to xnr-1 do begin
    rc[i]:=0;
  end;
  SetLength(tc, xnt);
  for i:=0 to xnt-1 do begin
    tc[i]:=0;
  end;
  with Experiment do begin
    for l:=0 to NTakes-1 do begin
      for i:=0 to NSites-1 do begin
        with Sites[i] do begin
          for j:=0 to NTubes-1 do begin
            with Tubes[j] do begin
              for k:=0 to Frames-1 do begin
                if not isCancelled and ((c mod 100<>0) or not isCancel) then begin
                  Inc(c);
                  if CheckOut(l, i,j,k) then begin
                    Inc(x);
                  end;
                  if (c mod 100=0) or (t<1000) then begin
                    IndicateProgress(c,t);
                  end;
                end;
              end;
            end;
          end;
        end;
      end;
    end;
  end;
  IndicateProgress(c,t);
  StopWorking;
  Result:=x;
  j:=0;
  for i:=0 to xnr-1 do begin
    ok:=CheckListBox1.Checked[j];
    CheckListBox1.Items[j]:=xRA[i]+' --> '+xRB[i]+'   ('+IntToStr(rc[i])+')';
    CheckListBox1.Checked[j]:=ok;
    Inc(j);
  end;
  CheckListBox1.Items[j]:='';
  CheckListBox1.Checked[j]:=false;
  CheckListBox1.State[j]:=cbGrayed;
  Inc(j);
  for i:=0 to xnt-1 do begin
    ok:=CheckListBox1.Checked[j];
    CheckListBox1.Items[j]:=xTA[i]+' --> '+xTB[i]+'   ('+IntToStr(tc[i])+')';
    CheckListBox1.Checked[j]:=ok;
    Inc(j);
  end;
  Wait(100);
  IndicateProgress(0,1);
  if isCancelled then begin
    Exit;
  end;
  if (mf<>sf) then begin
    WarningMsg('Only '+IntToStr(mf-sf)+' out of '+IntToStr(mf)+
      ' modified frames have been succesfully updated!');
  end;
  UpdateWindow(Handle);
end;

procedure TDataDlg.EnableUpdate;
var i: integer;
    ok: boolean;
begin
  ok:=false;
  for i:=0 to xnr-1 do begin
    ok:=ok or (xra[i]<>xrb[i]);
  end;
  for i:=0 to xnt-1 do begin
    ok:=ok or (xta[i]<>xtb[i]);
  end;
  Button10.Enabled:=ok;
end;

procedure TDataDlg.Button9Click(Sender: TObject);
begin
  inherited;
  RootViewer.UpdateFrame;
  ScanData;
end;

procedure TDataDlg.Button10Click(Sender: TObject);
begin
  inherited;
  UpdateData;
  RootViewer.UpdateFrame;
  with Experiment do begin
    RootViewer.SetFrame(TakeIndex, PlotIndex, TubeIndex, FrameIndex);
  end;
end;

procedure TDataDlg.CheckListBox1Click(Sender: TObject);
var xCItem,i,ind: integer;
    ok: boolean;
begin
  if (CCItem>=0) and (CCItem<CheckListBox1.Items.Count) then begin
    if CCItem<xnr then begin
      ind:=CCItem;
      xRB[ind]:=ComboBox1.Text;
      ok:=CheckListBox1.Checked[CCItem];
      if xRA[ind]<>xRB[ind] then begin
        if CheckListBox1.Items[CCItem]<>
          xRA[ind]+' --> '+xRB[ind] then begin
          CheckListBox1.Items[CCItem]:=
            xRA[ind]+' --> '+xRB[ind];
          CheckListBox1.Checked[CCItem]:=ok;
        end;
      end else begin
        if CheckListBox1.Items[CCItem]<>xRA[ind] then begin
          CheckListBox1.Items[CCItem]:=xRA[ind];
          CheckListBox1.Checked[CCItem]:=ok;
        end;
      end;
    end else
    if CCItem>xnr then begin
      ok:=CheckListBox1.Checked[CCItem];
      ind:=CCItem-xnr-1;
      xTB[ind]:=ComboBox1.Text;
      if xTA[ind]<>xTB[ind] then begin
        if CheckListBox1.Items[CCItem]<>
          xTA[ind]+' --> '+xTB[ind] then begin
          CheckListBox1.Items[CCItem]:=
            xTA[ind]+' --> '+xTB[ind];
          CheckListBox1.Checked[CCItem]:=ok;
        end;
      end else begin
        if CheckListBox1.Items[CCItem]<>xTA[ind] then begin
          CheckListBox1.Items[CCItem]:=xTA[ind];
          CheckListBox1.Checked[CCItem]:=ok;
        end;
      end;
    end else begin
      CheckListBox1.State[CCItem]:=cbGrayed;
    end;
  end;
  inherited;
  if CCItem=CheckListBox1.ItemIndex then begin
    Exit;
  end;
  ComboBox1.Enabled:=false;
  ComboBox1.Text:='';
  ComboBox1.Clear;
  xCItem:=CheckListBox1.ItemIndex;
  if xCItem<xnr then begin
    ind:=xCItem;
    with Experiment do begin
      for i:=0 to NRootCat-1 do begin
        ComboBox1.Items.Add(RootCat[i].Name);
      end;
    end;
    ComboBox1.Text:=xRB[ind];
    ComboBox1.Enabled:=true;
  end else
  if xCItem>xnr then begin
    ind:=xCItem-xnr-1;
    with Experiment do begin
      for i:=0 to NTipCat-1 do begin
        ComboBox1.Items.Add(TipCat[i].Name);
      end;
    end;
    ComboBox1.Text:=xTB[ind];
    ComboBox1.Enabled:=true;
  end;
  EnableUpdate;
  CCItem:=CheckListBox1.ItemIndex;
end;

procedure TDataDlg.CheckListBox1DblClick(Sender: TObject);
begin
  inherited;
  if ComboBox1.Enabled then begin
    ActiveControl:=ComboBox1;
  end;
end;

procedure TDataDlg.ComboBox1Change(Sender: TObject);
begin
  inherited;
  CheckListBox1Click(Sender);
end;

end.

