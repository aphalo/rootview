unit Frame;

interface

uses Windows, SysUtils, Classes, Graphics, Forms, Controls, StdCtrls, 
  Buttons, ExtCtrls, OkCancl1;

type
  TFrameDlg = class(TOKBottomDlg)
    HelpBtn: TButton;
    Edit1: TEdit;
    Edit2: TEdit;
    Edit3: TEdit;
    Edit4: TEdit;
    Edit5: TEdit;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    Label8: TLabel;
    Label9: TLabel;
    Edit6: TEdit;
    Edit7: TEdit;
    Edit8: TEdit;
    Edit9: TEdit;
    Label10: TLabel;
    Edit10: TEdit;
    Label11: TLabel;
    Edit11: TEdit;
    Label12: TLabel;
    Edit12: TEdit;
    Label13: TLabel;
    Edit13: TEdit;
    Label14: TLabel;
    Edit14: TEdit;
    Label15: TLabel;
    Edit15: TEdit;
    procedure HelpBtnClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure OKBtnClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    function Execute: Boolean;
  end;

var
  FrameDlg: TFrameDlg;

implementation

uses Roots, Data;

{$R *.DFM}

procedure TFrameDlg.HelpBtnClick(Sender: TObject);
begin
  if Length(HlpFile)>0 then begin
    WinHelp(RootViewer.Handle, @HlpFile[1], Help_Key,
      Integer(PChar('Dialog: Image Properties')));
  end else begin
    Application.HelpContext(HelpContext);
  end;
end;

function TFrameDlg.Execute: Boolean;
begin
  { execute; set result based on how closed }
  Result:=FrameDlg.ShowModal=IDOK;
end;

procedure TFrameDlg.FormShow(Sender: TObject);
var a,n,i,d,g: integer;
begin
  inherited;
  with RootViewer.Frame do begin
    Edit1.Text:=GetCurrFrameName;
    Edit2.Text:=GetCurrTubeName;
    Edit3.Text:=GetCurrPlotName;
    Edit4.Text:=GetCurrTakeName;
    Edit5.Text:=Comment;
    Edit6.Text:=WrFloatStr(W, 4);
    Edit7.Text:=WrFloatStr(H, 4);
    Edit8.Text:=WrFloatStr(DX, 4);
    Edit9.Text:=WrFloatStr(DY, 4);
    d:=0;
    g:=0;
    n:=NRoots;
    for i:=0 to n-1 do begin
      a:=Roots[i].Attribute;
      if a=1 then Inc(d) else
      if a=2 then Inc(g);
    end;
    Edit10.Text:=IntToStr(n);
    Edit11.Text:=IntToStr(d);
    Edit12.Text:=IntToStr(g);
    d:=0;
    g:=0;
    n:=NTips;
    for i:=0 to n-1 do begin
      a:=Tips[i].Attribute;
      if a=1 then Inc(d) else
      if a=2 then Inc(g);
    end;
    Edit13.Text:=IntToStr(n);
    Edit14.Text:=IntToStr(d);
    Edit15.Text:=IntToStr(g);
  end;
end;

procedure TFrameDlg.OKBtnClick(Sender: TObject);
begin
  inherited;
  with RootViewer.Frame do begin
    Comment:=Edit5.Text;
  end;
end;

end.
 
