�
 TCATSUBDLG 0s  TPF0�
TCatSubDlg	CatSubDlgCaptionSubstitute Renamed CategoriesClientHeight� OnShowFormShowPixelsPerInch`
TextHeight �TBevelBevel1Left,Top� WidthHeightVisible  �TButtonOKBtnLeft� TopDefaultTabOrder  �TButton	CancelBtnLeft� Top,TabOrder  TButtonHelpBtnLeft� TopPWidthKHeightCaption&HelpTabOrderOnClickHelpBtnClick  	TGroupBox	GroupBox1LeftTopWidth� Height� CaptionCategory SubstitutionsTabOrder TProgressBarProgressBar1LeftTop� Width� HeightMin MaxdSmooth	StepTabOrder   TCheckListBoxCheckListBox1LeftTopWidth� HeightuOnClickCheckCheckListBox1ClickCheck
ItemHeightTabOrder   TMemoMemo1LeftTop� Width)Height=Lines.Strings4Categories listed above have been renamed. This may <cause invalid entries in the existing data. You are advised 8to substitute such entries and save the changes (OK) or .else return to the Categories Dialog (Cancel). ReadOnly	
ScrollBars
ssVerticalTabOrder  TButtonButton1Left� ToptWidthKHeightCaption&SubstituteDefault	TabOrder OnClickButton1Click   